# README #

### What is this repository for? ###

* This repository contains the iOS code for StrokeMaster
* More information can be found on the [Confluence page](https://strokemaster.atlassian.net/wiki/display/STROK/Strokemaster). **For access to Confluence reach out to** [Sean Baxter](sbaxter323@gmail.com), [TR Staake](trstaake@gmail.com) or [Greg Morano](gmorano90@gmail.com)
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Make sure you have XCode installed and set up to develop iOS applications
* Launch Xcode
* Select 'Check out an existing project' in the launch window
* Copy the Clone URL that you get when selecting 'Clone' button in BitBucket
* Paste the URL into the XCode window and clone the 'Integration' branch
* You can now add, commit, push, and pull from the 'Source Control' menu in XCode
* Dependencies 
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Push and Pull only to the 'Integration' branch merges to master will restricted.
* Commit early and often