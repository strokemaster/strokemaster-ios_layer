//
//  ScoreCardViewController.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 6/1/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class ScoreCardViewController: UIViewController, UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource {
    
    var userdefaults = NSUserDefaults.standardUserDefaults()
    var scoreCard: CourseScorecard!
    var scrollView: UIScrollView!
    var tableView: UITableView!
    let scrollWidth: CGFloat = 1115
    
    var cellsTitles : [String]!
    var smallTitlesViews: ScoreIndexView?
    
    //Scroll Limits
    let startPoint: CGFloat = 50.0
    let endPoint: CGFloat = 100.0
    
    //Small Index Size
    let smallIndexWidth: CGFloat = 50.0
    
    var holes = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
    var teeDistances = [376, 356, 345, 338, 326, 315, 307, 299, 287, 278, 270, 263, 254, 246, 233, 227, 216, 208]
    var handicap = Array<NSInteger>()
    var par = Array<NSInteger>()
    var user = Array<NSInteger>()
    var approach = Array<NSInteger>()
    var shorts = Array<NSInteger>()
    var putts = Array<NSInteger>()
    
    var arrays = Array<Array<NSInteger>>()
    
    let headerLabel = UILabel()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nil, bundle: nil)
        cellsTitles = ["Holes", "Blue Tees 67/126", "Handicap", "Par", userdefaults.objectForKey("Email") as! String, "Approach shots", "Short game", "Putts"]
        updateArray()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        handicap = self.scoreCard.mensHandicap
        par = self.scoreCard.mensPar
        
        updateArray()
        
        self.view.clipsToBounds = true
        
        //TO-DO Fix lateral scroll on different screen sizes
        scrollView = UIScrollView(frame: self.view.frame)
        scrollView.delegate = self
        scrollView.contentSize = CGSizeMake(scrollWidth, 600)
        scrollView.bounces = false
        scrollView.backgroundColor = UIColor.clearColor()
        self.view.addSubview(scrollView)
        
        let staticTableHeader = UIView()
        staticTableHeader.transparentViewWithAlpha(0.5)
        
        self.view.addSubview(staticTableHeader)
        
        staticTableHeader.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsMake(0, 0, 0, 0), excludingEdge: ALEdge.Bottom)
        staticTableHeader.autoSetDimension(ALDimension.Height, toSize: 30.0)
        
        headerLabel.font = UIFont(name: "\(kDefaultFont)-Medium", size: 15.0)
        headerLabel.textColor = UIColor.whiteColor()
        staticTableHeader.addSubview(headerLabel)
        
        headerLabel.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        headerLabel.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
     
        tableView = UITableView(frame: CGRectMake(0, 30.0, scrollWidth, SCREEN_HEIGHT), style: UITableViewStyle.Plain)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = UIColor.clearColor()
        tableView.separatorColor = UIColor.clearColor()
        tableView.bounces = false
        scrollView.addSubview(tableView)
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        //Small Titles View
        if smallTitlesViews == nil {
            createSmallerTitleViews()
        }
        
        let parent = self.parentViewController!.parentViewController!.parentViewController as! CategoriesContainerViewController
        parent.changeNavigationTitle("Scorecard")
    }
    
    //TableView Methods
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 5 : 3
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("puttCell") as? ScoreCardTableViewCell
        if cell == nil {
            cell = ScoreCardTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "puttCell")
        }
        
        if indexPath.section == 0 && indexPath.row == 1 {
            cell?.blackView.backgroundColor = kTransparentBlue
        }
        
        if indexPath.section == 0 {
            cell?.cellLabel!.text = cellsTitles[indexPath.row]
            
            if indexPath.row == 4 {
                cell?.addImage()
                cell?.fillUserScoreValues(user, parArray: par)
            } else if indexPath.row == 1{
                cell?.fillCellWithValuesAndBackgroundColor(arrays[indexPath.row], color: kTransparentBlue)  
            } else {
                cell?.fillCellWithValuesAndBackgroundColor(arrays[indexPath.row], color: nil)
            }
            
            if indexPath.row == 1 {
                cell?.cellLabel!.font = UIFont(name: "\(kDefaultFont)-Bold", size: 16.0)
            }
            
        } else {
            cell?.cellLabel!.text = cellsTitles[indexPath.row + 5]
            cell?.fillCellWithValuesAndBackgroundColor(arrays[indexPath.row + 5], color: nil)
        }
        return cell!
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 50
        }
        return 0
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.clearColor()
        
        let label = UILabel()
        label.textColor = UIColor.whiteColor()
        label.text = "SCORE DETAILS"
        label.font = UIFont(name: "\(kDefaultFont)-Light", size: 13.0)
        view.addSubview(label)
        
        label.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsMake(20, 15, 0, 0), excludingEdge: ALEdge.Right)
        return view
    }
    
    //Create ShortLabel View
    func createSmallerTitleViews() {
    
        smallTitlesViews = ScoreIndexView(frame: CGRectMake(-smallIndexWidth, 30, smallIndexWidth, smallIndexWidth * 9))
        
        self.view.addSubview(smallTitlesViews!)
    }
    
    //ScrollView
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        if scrollView.contentOffset.x > startPoint && scrollView.contentOffset.x < endPoint {
            smallTitlesViews!.center = CGPointMake(scrollView.contentOffset.x - (startPoint + smallIndexWidth/2) , smallTitlesViews!.center.y)
        } else if scrollView.contentOffset.x > endPoint {
            smallTitlesViews!.center = CGPointMake(smallIndexWidth/2, smallTitlesViews!.center.y)
        } else if scrollView.contentOffset.x < startPoint {
            smallTitlesViews!.center = CGPointMake(-smallIndexWidth/2, smallTitlesViews!.center.y)
        }
    }
    
    //Update main array
    func updateArray() {
        arrays = [holes, teeDistances, handicap, par, user, approach, shorts, putts]
    }
    
    
    func initTeeDetails(course: CourseListCourse){
        cellsTitles = ["Holes", course.teeDetails!.teeColorName , "Handicap", "Par", userdefaults.objectForKey("Email") as! String, "Approach shots", "Short game", "Putts"]
        if(course.scoreCardInfo?.mensParTotal == 9){
            holes = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        }
        else{
            holes = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
        }
        teeDistances = course.teeDetails!.ydsHole
        par = course.scoreCardInfo!.mensPar
        handicap = course.scoreCardInfo!.mensHandicap
    }
}
