//
//  AppModel.swift
//  StrokeMaster-iOS
//
//  Created by Greg Morano on 3/12/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import Foundation

class AppModel: NSObject {
    let name: String
    let appStoreURL: String
    
    override var description: String {
        return "Name: \(name), URL: \(appStoreURL)\n"
    }
    
    init(name: String?, appStoreURL: String?) {
        self.name = name ?? ""
        self.appStoreURL = appStoreURL ?? ""
    }
}