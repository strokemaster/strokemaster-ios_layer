//
//  CourseDetailTableViewCell.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/8/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class CourseDetailTableViewCell: GenericIconLabelTableViewCell {
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        iconImageView.contentMode = UIViewContentMode.ScaleAspectFit
        
        iconImageView.autoPinEdgeToSuperviewEdge(ALEdge.Left, withInset: 35.0)
        iconImageView.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        
        
        label.textColor = UIColor.whiteColor()
        
        label.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        label.autoPinEdge(ALEdge.Left, toEdge: ALEdge.Right, ofView: iconImageView, withOffset: 35.0)

    }
    
    override func addChevron() {
        
        let chevron = UIImageView(image: UIImage(named: "chevron")!)
        self.contentView.addSubview(chevron)
        
        chevron.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        chevron.autoPinEdgeToSuperviewEdge(ALEdge.Right, withInset: 15.0)
    }
}
