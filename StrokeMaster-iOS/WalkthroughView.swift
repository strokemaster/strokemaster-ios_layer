//
//  WalkthroughView.swift
//  Hangify
//
//  Created by Marcos Griselli on 6/20/15.
//  Copyright (c) 2015 Hangify. All rights reserved.
//

import UIKit

protocol WalkthroughDelegate {
        func donePressed()
}

class WalkthroughView: UIView, UIScrollViewDelegate {
    
    private var scrollView: UIScrollView!
    private var circleArray = [UIImageView]()
    let button = UIButton()
    
    var delegate: WalkthroughDelegate!
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let imageView = UIImageView(image: UIImage(named: "backgroundblack"))
        self.addSubview(imageView)
        imageView.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsZero)
        
        scrollView = UIScrollView()
        scrollView.delegate = self
        scrollView.contentSize = CGSizeMake(SCREEN_WIDTH * 11, 0)
        scrollView.bounces = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.pagingEnabled = true
        scrollView.backgroundColor = UIColor.clearColor()
        self.addSubview(scrollView)
        scrollView.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsZero)
        
        for value in 0...10 {
            //Add images
            let floatValue = CGFloat(value)
            let imageView = UIImageView(image: UIImage(named: "walk\(value)"))
            imageView.frame = CGRectMake(SCREEN_WIDTH * floatValue, 0.0, SCREEN_WIDTH, SCREEN_HEIGHT)
            scrollView.addSubview(imageView)
        }
        
        for value in 0...10 {
            //Add indicator points
            let circle = UIImageView(image: UIImage(named: value == 0 ? "filledCircle" : "circle"))
            circleArray.append(circle)
            self.addSubview(circle)
            circle.autoAlignAxis(ALAxis.Vertical, toSameAxisOfView: self, withOffset: -SCREEN_WIDTH/5 + 15.0 * CGFloat(value))
            circle.autoPinEdgeToSuperviewEdge(ALEdge.Bottom, withInset: 15.0)
            circle.autoSetDimensionsToSize(CGSizeMake(10, 10))
        }
        
        //Skip/Done button
        button.setTitle("Skip", forState: UIControlState.Normal)
        button.addTarget(self, action: "buttonPressed", forControlEvents: UIControlEvents.TouchUpInside)
        self.addSubview(button)
        button.autoPinEdgeToSuperviewEdge(ALEdge.Bottom, withInset: 5.0)
        button.autoPinEdgeToSuperviewEdge(ALEdge.Right, withInset: 15.0)
    }
    
    //MARK:- ScrollView Delegate
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        
        for circle in circleArray {
            circle.image = UIImage(named: "circle")
        }
        let newValue = Int(scrollView.contentOffset.x/SCREEN_WIDTH)
        circleArray[newValue].image = UIImage(named: "filledCircle")
        
        if newValue == 10 {
            button.setTitle("Done", forState: UIControlState.Normal)
        } else {
            button.setTitle("Skip", forState: UIControlState.Normal)
        }
    }
    
    //Button Action
    func buttonPressed() {
        self.delegate.donePressed()
    }
}
