//
//  HorizontalButtonsScroll.h
//  Vorterix
//

#import <UIKit/UIKit.h>

@class HorizontalButtonsScroll;
@protocol HorizontalButtonsScrollDataSource <NSObject>

- (NSInteger)numberOfButtonsInHorizontalButtonsScroll:(HorizontalButtonsScroll *)horizontalButtonsScroll;
- (NSString *)horizontalButtonsScroll:(HorizontalButtonsScroll *)horizontalButtonsScroll titleForButtonAtIndex:(NSInteger)index;
- (NSString *)horizontalButtonsScroll:(HorizontalButtonsScroll *)horizontalButtonsScroll subtitleForButtonAtIndex:(NSInteger)index;
- (UIColor *)horizontalButtonsScroll:(HorizontalButtonsScroll *)horizontalButtonsScroll colorForButtonAtIndex:(NSInteger)index;

@end

@protocol HorizontalButtonsScrollDelegate <NSObject>
/**
 *  Called when an item is selected in an horizontal buttons scroll.
 *
 *  @param horizontalButtonsScroll Sender horizontal buttons scroll.
 *  @param item                    Selected item.
 */
- (void)horizontalButtonsScroll:(HorizontalButtonsScroll *)horizontalButtonsScroll didSelectButtonAtIndex:(NSInteger)index;

@end

@interface HorizontalButtonsScroll : UIView

@property (nonatomic, assign) id<HorizontalButtonsScrollDataSource> dataSource;
@property (nonatomic, assign) id<HorizontalButtonsScrollDelegate> delegate;

@property (nonatomic, assign) CGSize fixedCellSize;
@property (nonatomic, retain) NSMutableArray *iconArray;
@property (nonatomic, retain) UIColor *colorRGB;

/**
 *  Initializes an HorizontalButtonsScrollItem with a fixed cell size.
 *
 *  @param frame View frame.
 *  @param size  Fixed cell size.
 *
 *  @return Returns an HorizontalButtonsScrollItem.
 */
- (id)initWithFrame:(CGRect)frame fixedCellSize:(CGSize)size;
/**
 *  Forces selection of an item.
 *
 *  @param buttonIndex To-be-selected item index.
 */
- (void)selectButtonWithIndex:(NSInteger)buttonIndex;
- (void)reloadData;
- (void)scrollToIndex:(NSInteger) index;
@end
