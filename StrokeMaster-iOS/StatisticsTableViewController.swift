//
//  StatisticsTableViewController.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/27/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class StatisticsTableViewController: UITableViewController {

    var json: JSON = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Strokemaster"
        self.view.backgroundColor = UIColor.clearColor()
        
        //Table UI
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.clearColor()
        
    }
    
    init(json:JSON){
        super.init(style: .Plain)
        self.json = json
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return indexPath.section == 0 ? 420 : 85
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            var cell = tableView.dequeueReusableCellWithIdentifier("graphCell") as? StatisticsGraphCell
            if cell == nil {
                cell = StatisticsGraphCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell", json: self.json)
            }
            
            return cell!
        } else {
            var cell = tableView.dequeueReusableCellWithIdentifier("bottomCell") as? StatisticsBottomCell
            if cell == nil {
                cell = StatisticsBottomCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell",json: self.json)
            }
            
            return cell!
        }
    }
}
