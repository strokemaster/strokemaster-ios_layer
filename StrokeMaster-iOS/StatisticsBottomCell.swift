//
//  StatisticsBottomCell.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/27/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class StatisticsBottomCell: UITableViewCell {
    
    //Static
    let drivingLabel = UILabel()
    let approachLabel = UILabel()
    let shortGameLabel = UILabel()
    let puttingLabel = UILabel()
    var json:JSON = nil
    let xOffset = SCREEN_WIDTH/25
    
    //Scores
    var timer: NSTimer!
    
    let puttingScore = UILabel()
    
    var score = 0.0
    let final = 9.4


    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(style: UITableViewCellStyle, reuseIdentifier: String?, json:JSON) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.json = json
        self.selectionStyle = .None
        self.backgroundColor = UIColor.clearColor()
        
        self.contentView.addSubview(drivingLabel)
        drivingLabel.statisticsScoresTitles()
        drivingLabel.text = "Driving"
        drivingLabel.autoPinEdgeToSuperviewEdge(ALEdge.Leading, withInset: xOffset)
        
        self.contentView.addSubview(approachLabel)
        approachLabel.statisticsScoresTitles()
        approachLabel.text = "Approach"
        approachLabel.autoAlignAxis(ALAxis.Vertical, toSameAxisOfView: self.contentView, withOffset: -(SCREEN_WIDTH/7))

        self.contentView.addSubview(shortGameLabel)
        shortGameLabel.statisticsScoresTitles()
        shortGameLabel.text = "Short Game"
        shortGameLabel.autoAlignAxis(ALAxis.Vertical, toSameAxisOfView: self.contentView, withOffset: SCREEN_WIDTH/7)
        
        self.contentView.addSubview(puttingLabel)
        puttingLabel.statisticsScoresTitles()
        puttingLabel.text = "Putting"
        puttingLabel.autoPinEdgeToSuperviewEdge(ALEdge.Right, withInset: xOffset)
        var myScores = [Int]()
        var driving = self.json["Driving"].int
        driving = (driving == nil ? 0 : driving)
        myScores.append(driving!)
        var approach = self.json["Approach"].int
        approach = (approach == nil ? 0 : approach)
        myScores.append(approach!)
        var shortGame = self.json["Short Game"].int
        shortGame = (shortGame == nil ? 0 : shortGame)
        myScores.append(shortGame!)
        var putting = self.json["Putting"].int
        putting = (putting == nil ? 0 : putting)
        myScores.append(putting!)
        self.addScores(myScores)
    }
    
    //Add scores for each Title
    func addScores(scoresArray: Array<Int>) {
        
        let drivingScore = UILabel()
        drivingScore.statisticsScores()
        drivingScore.text = ("\(scoresArray[0])%")
        drivingScore.textColor = self.setColorForScore(scoresArray[0])
        self.contentView.addSubview(drivingScore)
        
        drivingScore.autoAlignAxis(ALAxis.Vertical, toSameAxisOfView: drivingLabel, withOffset: 5.0)
        drivingScore.autoPinEdge(ALEdge.Top, toEdge: ALEdge.Bottom, ofView: drivingLabel, withOffset: 15.0)
        
        let approachScore = UILabel()
        approachScore.statisticsScores()
        approachScore.text = ("\(scoresArray[1])%")
        approachScore.textColor = self.setColorForScore(scoresArray[1])
        self.contentView.addSubview(approachScore)
        
        approachScore.autoAlignAxis(ALAxis.Vertical, toSameAxisOfView: approachLabel, withOffset: 5.0)
        approachScore.autoPinEdge(ALEdge.Top, toEdge: ALEdge.Bottom, ofView: approachLabel, withOffset: 15.0)
        
        let shortGameScore = UILabel()
        shortGameScore.statisticsScores()
        shortGameScore.text = ("\(scoresArray[2])%")
        shortGameScore.textColor = self.setColorForScore(scoresArray[2])
        self.contentView.addSubview(shortGameScore)
        
        shortGameScore.autoAlignAxis(ALAxis.Vertical, toSameAxisOfView: shortGameLabel, withOffset: 5.0)
        shortGameScore.autoPinEdge(ALEdge.Top, toEdge: ALEdge.Bottom, ofView: shortGameLabel, withOffset: 15.0)
        
        puttingScore.statisticsScores()
        puttingScore.text = ("\(scoresArray[3])%")
        puttingScore.textColor = self.setColorForScore(scoresArray[3])
        self.contentView.addSubview(puttingScore)
        
        puttingScore.autoAlignAxis(ALAxis.Vertical, toSameAxisOfView: puttingLabel, withOffset: 5.0)
        puttingScore.autoPinEdge(ALEdge.Top, toEdge: ALEdge.Bottom, ofView: puttingLabel, withOffset: 15.0)
        
        
//        timer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: self, selector:  Selector("loadScore"), userInfo: nil, repeats: true)
    }
    
    //Load each score as a counter from 0 to its value in 1 or 2 seconds as graph loads
    func loadScore() {
        
        //Possible in the future
        if score < final {
            puttingScore.text = String(format: "%.01f", arguments: [score])
            score = score + 0.1
        } else {
            timer.invalidate()
        }
    }
    
    //Color check
    func setColorForScore(score: Int) -> UIColor {
        
        if score < 60 {
            return UIColor.redColor()
        } else if score < 70 {
            return UIColor.orangeColor()
        } else if score < 80 {
            return UIColor.yellowColor()
        }
        return score < 90 ? kscoreGreenColor : kscoreBlueColor
    }
}
