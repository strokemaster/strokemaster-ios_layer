//
//  SideMenuTableHeaderView.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/5/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class SideMenuTableHeaderView: UIView {
    
    let profileImageView = UIImageView()
    let nameLabel = UILabel()
    let handicapLabel = UILabel()
    let levelLabel = UILabel()

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.blackColor()
        
        profileImageView.circularImageViewWithWidth(75.0)
        profileImageView.drawBordersWithColorAndWidth(UIColor.whiteColor(), width: 3.0)
        profileImageView.image = UIImage(named: "golfer")
        self.addSubview(profileImageView)
        
        profileImageView.autoPinEdgeToSuperviewEdge(ALEdge.Left, withInset: 15.0)
        profileImageView.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        
        handicapLabel.textColor = UIColor.whiteColor()
        handicapLabel.text = "Handicap 28"
        handicapLabel.font = UIFont(name: "\(kDefaultFont)-Medium", size: 12.0)
        self.addSubview(handicapLabel)
        
        handicapLabel.autoAlignAxis(ALAxis.Horizontal, toSameAxisOfView: profileImageView)
        handicapLabel.autoPinEdge(ALEdge.Left, toEdge: ALEdge.Right, ofView: profileImageView, withOffset: 10.0)
        
        nameLabel.textColor = UIColor.whiteColor()
        //nameLabel.text = "Robert Downey Jr."
        let userdefaults = NSUserDefaults.standardUserDefaults()
        nameLabel.text = userdefaults.objectForKey("Email") as? String
        nameLabel.font = UIFont(name: "\(kDefaultFont)-Light", size: 14.0)
        self.addSubview(nameLabel)
        
        nameLabel.autoPinEdge(ALEdge.Bottom, toEdge: ALEdge.Top, ofView: handicapLabel, withOffset: -10.0)
        nameLabel.autoPinEdge(ALEdge.Left, toEdge: ALEdge.Left, ofView: handicapLabel)
        
        levelLabel.textColor = UIColor.whiteColor()
        levelLabel.text = "LEVEL: Pro"
        levelLabel.font = UIFont(name: "\(kDefaultFont)-Medium", size: 11.0)
        self.addSubview(levelLabel)
        
        levelLabel.autoPinEdge(ALEdge.Top, toEdge: ALEdge.Bottom, ofView: handicapLabel, withOffset: 10.0)
        levelLabel.autoPinEdge(ALEdge.Left, toEdge: ALEdge.Left, ofView: nameLabel)

    }
}
