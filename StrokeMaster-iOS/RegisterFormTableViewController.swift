//
//  RegisterFormTableViewController.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/4/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//


//NOT IN USE ANYMORE
import UIKit

class RegisterFormTableViewController: GenericFormTableViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerView.title.text = "Register to Strokemaster"
        //headerView.addStep("STEP 1/2")
        
        footerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 288)
        footerView.button.setTitle("NEXT STEP", forState: UIControlState.Normal)
        footerView.centeredGoBackButton()
        footerView.addMailLabel()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func pushViewController(email: NSString) {
        let control = BagSetupViewController()
        //control.email = email
        self.navigationController?.customPush(control)
    }
    
    override func loginPressed() {
        print("First step of registering complete")
        self.pushViewController("testemail")
    }
    
}
