//
//  DiagnosticTableViewController.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 7/3/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class DiagnosticTableViewController: UITableViewController {
    
    let shotTypes = ["Overall", "Driving", "Approach", "Short Game", "Putting"]
    //let indexes = [145, 88, 23, 8]
    //let values = [135, 90,  20, 10]
    //let excess = [10, 2, 5, 7]
    var json: JSON = nil
    
    init() {
        super.init(style: .Plain)
        let userdefaults = NSUserDefaults.standardUserDefaults()
        
        let semaphore = dispatch_semaphore_create(0)
        
        let email = userdefaults.objectForKey("Email") as! String
        
        DataManager.getDiagnostics(email, success:{(results) -> Void in
            let result = results
            self.json = JSON(data: result)
            
            dispatch_semaphore_signal(semaphore)
        })
        
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Diagnostic Stats"
        
        self.tableView.backgroundColor = UIColor.clearColor()
        self.tableView.tableFooterView = UIView()
        self.tableView.tableHeaderView = createTableHeader()
        
        
        let backButton = UIBarButtonItem(image: UIImage(named: "backarrow"), style: UIBarButtonItemStyle.Plain, target: self, action: "backPressed")
        self.navigationItem.leftBarButtonItem = backButton
        
    }

    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return json.count - 1
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("diagCell") as? DiagnosticTableViewCell
        if cell == nil {
            cell = DiagnosticTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "diagCell")
        }
        
        //initialize stat vars
        var type : JSON = nil
        if(indexPath.row == 0){
            type = json["Overall"]
        }
        else if(indexPath.row == 1){
            type = json["Driving"]
        }
        else if(indexPath.row == 2){
            type = json["Approach"]
        }
        else if(indexPath.row == 3){
            type = json["Short Game"]
        }
        else{
            type = json["Putting"]
        }
        
        cell?.shotTypeLabel.text = shotTypes[indexPath.row]
        cell?.indexLabel.text = "\(type["Index"])"
        cell?.valueLabel.text = "\(type["Value"])"
        
        var scoreColor = type["Excess"]
        
        cell?.excessLabel.textColor = setColorForScore( scoreColor.int! )
        cell?.excessLabel.text = "\(type["Excess"])"
        
        if type["Value"] > type["Index"] {
            cell?.valueLabel.font = UIFont.boldSystemFontOfSize(18.0)
        }

        return cell!
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRectMake(0, 0, SCREEN_WIDTH, 50.0))
        
        let clubLabel = UILabel()
        clubLabel.textColor = .whiteColor()
        clubLabel.text = "Shot Type"
        headerView.addSubview(clubLabel)
        clubLabel.autoPinEdgeToSuperviewEdge(ALEdge.Left, withInset: 15.0)
        clubLabel.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        
        let rangeLabel = UILabel()
        rangeLabel.textColor = .whiteColor()
        rangeLabel.text = "Index"
        headerView.addSubview(rangeLabel)
        rangeLabel.autoAlignAxis(ALAxis.Vertical, toSameAxisOfView: headerView, withOffset: -SCREEN_WIDTH/10)
        rangeLabel.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        
        let effectivityLabel = UILabel()
        effectivityLabel.textColor = .whiteColor()
        effectivityLabel.text = "Your Value"
        headerView.addSubview(effectivityLabel)
        effectivityLabel.autoAlignAxis(ALAxis.Vertical, toSameAxisOfView: headerView, withOffset: SCREEN_WIDTH/7)
        effectivityLabel.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        
        let excessLabel = UILabel()
        excessLabel.textColor = .whiteColor()
        excessLabel.text = "Excess"
        headerView.addSubview(excessLabel)
        excessLabel.autoPinEdgeToSuperviewEdge(ALEdge.Right, withInset: 15.0)
        excessLabel.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        
        let separatorView = UIView()
        separatorView.backgroundColor = UIColor.lightGrayColor()
        headerView.addSubview(separatorView)
        separatorView.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsZero, excludingEdge: ALEdge.Top)
        separatorView.autoSetDimension(ALDimension.Height, toSize: 0.5)
        
        return headerView
    }
    
    func createTableHeader() -> UIView {
        
        let headerView = UIView(frame: CGRectMake(0, 0, SCREEN_WIDTH, 60.0))
        
        let label = UILabel()
        label.font = UIFont.boldSystemFontOfSize(18.0)
        label.textColor = .whiteColor()
        label.text = "Average Round - Excess"
        headerView.addSubview(label)
        label.autoPinEdgeToSuperviewEdge(ALEdge.Left, withInset: 15.0)
        label.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        
        let value = UILabel()
        let type = json["Overall"]
        var scoreColor = type["Excess"]
        
        value.text = String(type["Excess"].int!)
        value.textColor = setColorForScore( scoreColor.int! )
        value.font = UIFont.boldSystemFontOfSize(22.0)
        headerView.addSubview(value)
        value.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        value.autoPinEdge(ALEdge.Left, toEdge: ALEdge.Right, ofView: label, withOffset: 5.0)
        
        return headerView
    }
    
    func backPressed() {
        self.navigationController?.customPop()
    }
    
    //Color check
    func setColorForScore(score: Int) -> UIColor {
        
        if score < 4 {
            return UIColor.redColor()
        } else if score < 6 {
            return UIColor.orangeColor()
        } else if score < 8 {
            return UIColor.yellowColor()
        }
        return score < 9 ? kscoreGreenColor : kscoreBlueColor
    }
}
