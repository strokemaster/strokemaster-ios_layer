//
//  LoadingView.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/14/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class LoadingView: UIView {
    
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
    let loadingLabel = UILabel()
    let blackView = UIView()
    let tipLabel = UILabel()
    let tipName = UILabel()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.clearColor()
        
        activityIndicator.startAnimating()
        self.addSubview(activityIndicator)
        activityIndicator.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        activityIndicator.autoAlignAxis(ALAxis.Horizontal, toSameAxisOfView: self, withOffset: -60.0)
        
        loadingLabel.text = "LOADING"
        loadingLabel.textColor = UIColor.whiteColor()
        self.addSubview(loadingLabel)
        
        loadingLabel.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        loadingLabel.autoPinEdge(ALEdge.Top, toEdge: ALEdge.Bottom, ofView: activityIndicator, withOffset: 15.0)
        
        blackView.transparentBlackView()
        blackView.drawBordersWithColorAndWidth(UIColor.blackColor(), width: 1.5)
        self.addSubview(blackView)
        
        blackView.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsMake(0, -1, 60.0, -1), excludingEdge: ALEdge.Top)
        
        tipLabel.textColor = .whiteColor()
        tipLabel.text = "My attitude is that if you push me towards something that you think is a weakness, then I will turn that perceived weakness into a strength."
        tipLabel.numberOfLines = 0
        
        blackView.addSubview(tipLabel)
        
        tipLabel.autoPinEdgeToSuperviewEdge(ALEdge.Left, withInset: 15.0)
        tipLabel.autoPinEdgeToSuperviewEdge(ALEdge.Top, withInset: 15.0)
        tipLabel.autoPinEdgeToSuperviewEdge(ALEdge.Right, withInset: 15.0)
        
        tipName.textColor = .whiteColor()
        tipName.text = "-- Michael Jordan"
        blackView.addSubview(tipName)
        
        tipName.autoPinEdgeToSuperviewEdge(ALEdge.Right, withInset: 15.0)
        tipName.autoPinEdgeToSuperviewEdge(ALEdge.Bottom, withInset: 15.0)
        tipName.autoPinEdge(ALEdge.Top, toEdge: ALEdge.Bottom, ofView: tipLabel, withOffset: 10.0)
    
    }
}
