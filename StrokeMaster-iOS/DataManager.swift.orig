//
//  DataManager.swift
//  StrokeMaster-iOS
//
//  Created by Greg Morano on 3/12/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

let serverURL = "http://ec2-52-24-220-119.us-west-2.compute.amazonaws.com:9000/"
//let TopAppURL = "https://itunes.apple.com/us/rss/topgrossingipadapplications/limit=25/json"
//let iGolfUrl = "https://localhost:9443/vector-data"


class DataManager {
    
    //webservice call to get vector data for specific course
    class func getTopAppsDataFromIGolfWithSuccess(success: ((iGolfData: NSData!) -> Void)) {
        //var iGolfUrl: String = "http://localhost:9000/vector-data"
        var iGolfUrl: String  = "http://ec2-52-24-220-119.us-west-2.compute.amazonaws.com:9000/vector-data"
        loadDataFromURL(NSURL(string: iGolfUrl)!, completion:{(data, error) -> Void in
            //If data was retrieved successfully
            if let urlData = data {
                success(iGolfData: urlData)
            }
        })
    }
    
    class func getVectorDataForCourseWithID(courseId: String, success: ((iGolfData: NSData!) -> Void)) {
        //var iGolfUrl: String = "http://localhost:9000/vector-data"
        
        //Replace courseID
        var iGolfUrl = "\(serverURL)vector-data?id=\(courseId)"
        loadDataFromURL(NSURL(string: iGolfUrl)!, completion:{(data, error) -> Void in
            //If data was retrieved successfully
            if let urlData = data {
                success(iGolfData: urlData)
            }
        })
    }
    
    //tries to login with user name and password
    class func tryLogin(username: String, password: String, success: ((loginResult: NSData!) -> Void)) {
        
        //var loginUrl: String = "https://localhost:9443/login?"
        //var loginUrl: String = "http://localhost:9000/login?"
        var loginUrl: String = "http://ec2-52-24-220-119.us-west-2.compute.amazonaws.com:9000/login?"
        loginUrl += "email=" + username + "&pass=" + password
            
        loadDataFromURL(NSURL(string: loginUrl)!, completion:{(data, error) -> Void in
            //If data was retrieved successfully
            if let urlData = data {
                success(loginResult: urlData)
            }
        })
        
    }
    
    //tries to create user with username and password
    class func createUser(username: String, password: String, success: ((loginResult: NSData!) -> Void)) {
        
        //var loginUrl: String = "https://localhost:9443/create-user?"
        //var loginUrl: String = "http://localhost:9000/create-user?"
        var loginUrl: String = "http://ec2-52-24-220-119.us-west-2.compute.amazonaws.com:9000/create-user?"
        loginUrl += "email=" + username + "&pass=" + password
        
        loadDataFromURL(NSURL(string: loginUrl)!, completion:{(data, error) -> Void in
            //If data was retrieved successfully
            if let urlData = data {
                success(loginResult: urlData)
            }
        })
    }
    
    //gets the closest courses
    class func getCourseList(lon: Double, lat: Double, success: ((loginResult: NSData!) -> Void)) {
        
        //var loginUrl: String = "https://localhost:9443/course-list?"
        //var loginUrl: String = "http://localhost:9000/course-list?"
        var loginUrl: String = "http://ec2-52-24-220-119.us-west-2.compute.amazonaws.com:9000/course-list?"
        loginUrl += "lon=" + String(format:"%f", lon) + "&lat=" + String(format:"%f", lat)
        
        loadDataFromURL(NSURL(string: loginUrl)!, completion:{(data, error) -> Void in
            //If data was retrieved successfully
            if let urlData = data {
                success(loginResult: urlData)
            }
        })
    }
    
    //gets the closest courses scorecards
    class func getCourseScorecardList(courseIds: [String], success: ((loginResult: NSData!) -> Void)) {
        
        //var loginUrl: String = "https://localhost:9443/course-scorecard-list?"
        var loginUrl: String = "http://localhost:9000/course-scorecard-list?"
        //var loginUrl: String = "http://ec2-52-24-220-119.us-west-2.compute.amazonaws.com:9000/course-scorecard-list?"
        loginUrl += "courses=["
        for str in courseIds{
            loginUrl += str + ","
        }
        loginUrl.substringToIndex(loginUrl.endIndex.predecessor())
        loginUrl += "]"
        loadDataFromURL(NSURL(string: loginUrl)!, completion:{(data, error) -> Void in
            //If data was retrieved successfully
            if let urlData = data {
                success(loginResult: urlData)
            }
        })
    }
    
    //test function for getting json data from a file.  Not used
    class func getTopAppsDataFromFileWithSuccess(success: ((data: NSData) -> Void)) {
        //1
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            //2
            let filePath = NSBundle.mainBundle().pathForResource("TopApps",ofType:"json")
            
            var readError:NSError?
            if let data = NSData(contentsOfFile:filePath!,
                options: NSDataReadingOptions.DataReadingUncached,
                error:&readError) {
                    success(data: data)
            }
        })
    }
    
    //create an http request and call the given url web service
    class func loadDataFromURL(url: NSURL, completion:(data: NSData?, error: NSError?) -> Void) {
        var session = NSURLSession.sharedSession()
        
        // Use NSURLSession to get data from an NSURL
        let loadDataTask = session.dataTaskWithURL(url, completionHandler: { (data: NSData!, response: NSURLResponse!, error: NSError!) -> Void in
            if let responseError = error {
                completion(data: nil, error: responseError)
            } else if let httpResponse = response as? NSHTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    var statusError = NSError(domain:"com.strokemaster", code:httpResponse.statusCode, userInfo:[NSLocalizedDescriptionKey : "HTTP status code has unexpected value."])
                    completion(data: nil, error: statusError)
                } else {
                    completion(data: data, error: nil)
                }
            }
        })
        
        loadDataTask.resume()
    }
    
    //converts json of point data to a vector of lines to draw
    class func pointsToVectorLines(courseObject: String, json: JSON) -> [Line] {
        var count = 0
        var points = [CGPoint]()
        var lines  = [Line]()
        
        //parse the json into an array of CGPOints
        while let xval = json[courseObject][count]["x"].int {
            //println("Point " + String(count))
            //println(json[courseObject][count])
            let point: CGPoint = CGPoint(x: abs(json[courseObject][count]["x"].doubleValue) , y: abs(json[courseObject][count]["y"].doubleValue))
            points.append(point)
            count++
        }
        
        var size  = points.count
        for( var i = 0; i < (size-1); i++){
            let line: Line = Line(start: points[i], end: points[i+1])
            lines.append(line)
        }
        lines.append(Line(start: points[size-1], end: points[0]))
    
        return lines
    }
    
    /*
    *Iterates through multiple shapes for a specific course object.  Then adds them to a dictionary to be later drawn.
    */
    class func pointsToMultiVectorLines(courseObject: String, json: JSON) -> Dictionary<Int, [Line]> {
        var objects = Dictionary<Int, [Line]>()
        
        //iterate through each shape
        for(var i = 0; i < json[courseObject].count; i++){
            var points = [CGPoint]()
            var lines  = [Line]()
            var count = 0
            
            //iterate through each point for this shape then creates lines from them
            while let xval = json[courseObject][i.description][count]["x"].int {

                let point: CGPoint = CGPoint(x: abs(json[courseObject][i.description][count]["x"].doubleValue) , y: abs(json[courseObject][i.description][count]["y"].doubleValue))
                points.append(point)
                count++
            }
            
            var size  = points.count
            for( var i = 0; i < (size-1); i++){
                let line: Line = Line(start: points[i], end: points[i+1])
                lines.append(line)
            }
            if(courseObject != "cartpath"){
                lines.append(Line(start: points[size-1], end: points[0]))
            }
            objects[i] = lines
        }
        
        return objects
    }
    
    
    /*
    *Iterates through multiple shapes for a specific course object.  Then adds them to a dictionary to be later drawn.
    */
    class func pointsToMultiPoints(courseObject: String, json: JSON) -> [CGPoint] {
        var points = [CGPoint]()
        
        //iterate through each shape
        for(var i = 0; i < json[courseObject].count; i++){
            let point: CGPoint = CGPoint(x: abs(json[courseObject][i.description]["x"].doubleValue) , y: abs(json[courseObject][i.description]["y"].doubleValue))
            points.append(point)
        }
        
        return points
    }
    
    /*
<<<<<<< HEAD
    Sending data to backend
    */
    
    func sendData(nametextField: UITextField, passwordTextField: UITextField) {
    
    //declare parameter as a dictionary which contains string as key and value combination.
    var parameters = ["name": nametextField.text, "password": passwordTextField.text] as Dictionary<String, String>
    
    //create the url with NSURL
    let url = NSURL(string: "http://myServerName.com/api") //change the url
    
    //create the session object
    var session = NSURLSession.sharedSession()
    
    //now create the NSMutableRequest object using the url object
    let request = NSMutableURLRequest(URL: url!)
    request.HTTPMethod = "POST" //set http method as POST
    
    var err: NSError?
    request.HTTPBody = NSJSONSerialization.dataWithJSONObject(parameters, options: nil, error: &err) // pass dictionary to nsdata object and set it as request body
    
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    request.addValue("application/json", forHTTPHeaderField: "Accept")
    
    //create dataTask using the session object to send data to the server
    var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
        println("Response: \(response)")
        var strData = NSString(data: data, encoding: NSUTF8StringEncoding)
        println("Body: \(strData)")
        var err: NSError?
        var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves, error: &err) as? NSDictionary
        
        // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
        if(err != nil) {
            println(err!.localizedDescription)
            let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
            println("Error could not parse JSON: '\(jsonStr)'")
        }
        else {
            // The JSONObjectWithData constructor didn't return an error. But, we should still
            // check and make sure that json has a value using optional binding.
            if let parseJSON = json {
                // Okay, the parsedJSON is here, let's get the value for 'success' out of it
                var success = parseJSON["success"] as? Int
                println("Succes: \(success)")
            }
            else {
                // Woa, okay the json object was nil, something went worng. Maybe the server isn't running?
                let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                println("Error could not parse JSON: \(jsonStr)")
            }
        }
    })
    
    task.resume()
=======
    *Converts an array of json to an array of int
    */
    class func toIntArray(arr: JSON, str: String) -> [Int] {
        
        var res = [Int]()
        for(var i = 0; i < arr[str].count; i++){
            res.append(arr[str][i].int!)
        }
        return res
>>>>>>> development_greg
    }
    
}