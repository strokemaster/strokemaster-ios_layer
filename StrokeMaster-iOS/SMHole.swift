//
//  SMHole.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/22/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class SMHole: NSObject {
    
    let number: NSInteger!
    let yards: NSInteger!
    let par: NSInteger!
    let handicap: NSInteger!
    
    
    init(number: NSInteger!, yards: NSInteger!, par: NSInteger!, handicap: NSInteger!) {
        self.yards = yards
        self.par = par
        self.handicap = handicap
        self.number = number
        super.init()
    }
}
