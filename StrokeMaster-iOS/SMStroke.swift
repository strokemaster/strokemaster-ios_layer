//
//  SMStroke.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/25/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class SMStroke: NSObject {
    
    let type: String! //Type refers to stroke-putt-penalty
    let club: SMClub?
    let yards: NSInteger? //Optional as penalty doesn't have yards
    let startPoint: String?
    let endPoint: String?
    let startDist: Int?
    let endDist: Int?
    let index: NSInteger!
    let clubNumber: Int?
    
    
    init(type: String!, club: SMClub?, yards: NSInteger?, startPoint: String?, endPoint: String?, startDist: Int?, endDist: Int?, index: NSInteger!) {
        self.type = type
        self.club = club
        self.yards = yards
        self.startPoint = startPoint
        self.endPoint = endPoint
        self.startDist = startDist
        self.endDist = endDist
        self.index = index
        self.clubNumber = nil
        super.init()
    }
    
    init(type: String!, club: SMClub?, yards: NSInteger?, startPoint: String?, endPoint: String?, startDist: Int?, endDist: Int?, index: NSInteger!, clubNumber: Int?) {
        self.type = type
        self.club = club
        self.yards = yards
        self.startPoint = startPoint
        self.endPoint = endPoint
        self.startDist = startDist
        self.endDist = endDist
        self.index = index
        self.clubNumber = clubNumber
        super.init()
    }
}
