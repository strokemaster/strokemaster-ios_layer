//
//  TrackingView.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/14/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

protocol TrackingViewDelegate {
    func gpsButtonPressed()
    func penaltyButtonPressed()
    func puttButtonPressed()
    func updateStartLocation()
}

class TrackingView: GenericGameView {
    
    var delegate: TrackingViewDelegate!
    let buttonWidth = SCREEN_WIDTH - SCREEN_WIDTH/5
    
    //Buttons
    let teeOffButton = UIButton()
    let gpsButton = UIButton()
    let penaltyButton = UIButton()
    let puttButton = UIButton()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        gpsButton.addTarget(self, action: "gpsPressed", forControlEvents: UIControlEvents.TouchUpInside)
        gpsButton.transparentButton("SAVE GPS POINT")
        gpsButton.alpha = 0
        self.addSubview(gpsButton)
        
        gpsButton.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        gpsButton.autoPinEdge(ALEdge.Top, toEdge: ALEdge.Bottom, ofView: handicapView, withOffset: 20.0)
        gpsButton.autoSetDimensionsToSize(CGSizeMake(buttonWidth, 50.0))
        
        penaltyButton.addTarget(self, action: "penaltyPressed", forControlEvents: UIControlEvents.TouchUpInside)
        penaltyButton.transparentButton("ADD PENALTY")
        penaltyButton.backgroundColor = UIColor(red: 210.0/255.0, green: 40.0/255.0, blue: 50.0/255.0, alpha: 0.2)
        penaltyButton.alpha = 0
        self.addSubview(penaltyButton)
        
        penaltyButton.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        penaltyButton.autoPinEdge(ALEdge.Top, toEdge: ALEdge.Bottom, ofView: gpsButton, withOffset: 10.0)
        penaltyButton.autoMatchDimension(ALDimension.Width, toDimension: ALDimension.Width, ofView: gpsButton)
        penaltyButton.autoSetDimension(ALDimension.Height, toSize: 30.0)
        
        puttButton.addTarget(self, action: "puttPressed", forControlEvents: UIControlEvents.TouchUpInside)
        puttButton.transparentButton("SAVE PUTT")
        puttButton.backgroundColor = kLightGreenColor
        puttButton.alpha = 0
        self.addSubview(puttButton)
        
        puttButton.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        puttButton.autoPinEdge(ALEdge.Top, toEdge: ALEdge.Bottom, ofView: penaltyButton, withOffset: 10.0)
        puttButton.autoMatchDimension(ALDimension.Width, toDimension: ALDimension.Width, ofView: gpsButton)
        puttButton.autoSetDimension(ALDimension.Height, toSize: 30.0)
    
        setTeeOff()
    }
    
    //Create TeeOff for first hole
    func setTeeOff() {
        
        teeOffButton.addTarget(self, action: "teeOffPressed", forControlEvents: UIControlEvents.TouchUpInside)
        teeOffButton.transparentButton("Tee Off")
        teeOffButton.backgroundColor = kRoyalBlue
        
        self.addSubview(teeOffButton)
        
        teeOffButton.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        teeOffButton.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        teeOffButton.autoSetDimensionsToSize(CGSizeMake(buttonWidth, 60.0))
    }
    
    //Replace TeeOff button with others
    func teeOffPressed() {
        
        self.teeOffButton.removeConstraints(self.teeOffButton.constraints)
        
        //Create new constrains to animate change
        teeOffButton.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        teeOffButton.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        teeOffButton.autoSetDimensionsToSize(CGSizeMake(buttonWidth, 0.0))
        
        teeOffButton.setTitle("", forState: UIControlState.Normal)
        
        self.delegate.updateStartLocation()
        
        UIView.animateWithDuration(0.33, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
            
            self.teeOffButton.layoutIfNeeded()
            
            self.gpsButton.alpha = 1.0
            self.penaltyButton.alpha = 1.0
            self.puttButton.alpha = 1.0
            
            }) { (finished) -> Void in
            self.teeOffButton.removeFromSuperview()
        }
        
    }
    
    //MARK: - TrackingView Delegate
    func gpsPressed() {
        self.delegate.gpsButtonPressed()
    }
    
    func penaltyPressed() {
        self.delegate.penaltyButtonPressed()
    }
    
    func puttPressed() {
        self.delegate.puttButtonPressed()
    }
    
    func refreshTrackingView(){

        self.gpsButton.alpha = 0
        self.penaltyButton.alpha = 0
        self.puttButton.alpha = 0

        self.teeOffButton.removeConstraints(self.teeOffButton.constraints)
        
        teeOffButton.addTarget(self, action: "teeOffPressed", forControlEvents: UIControlEvents.TouchUpInside)
        teeOffButton.transparentButton("Tee Off")
        teeOffButton.backgroundColor = kRoyalBlue
        
        self.addSubview(teeOffButton)
        
        teeOffButton.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        teeOffButton.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        
        teeOffButton.autoSetDimensionsToSize(CGSizeMake(buttonWidth, 60.0))
        
    }
}
