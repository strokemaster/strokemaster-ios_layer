//
//  GameSetupCourseCell.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/8/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class GameSetupCourseCell: UITableViewCell {
    
    let blackView = UIView()
    let nameLabel = UILabel()
    let detailLabel = UILabel()

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(style: UITableViewCellStyle, reuseIdentifier: String?, course: CourseListCourse) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    
        self.backgroundColor = UIColor.clearColor()
        self.selectionStyle = .None
        
        blackView.transparentBlackView()
        self.contentView.addSubview(blackView)
        
        blackView.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsMake(2, 0, 0, 0))
        
        
        nameLabel.textColor = UIColor.whiteColor()
        nameLabel.text = course.name
        nameLabel.font = UIFont(name: "\(kDefaultFont)-Light", size: 20.0)
        blackView.addSubview(nameLabel)
        
        nameLabel.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsMake(5.0, 15.0, 0, 15.0), excludingEdge: ALEdge.Bottom)
    
        detailLabel.textColor = UIColor.whiteColor()
        detailLabel.font = UIFont(name: "\(kDefaultFont)-Light", size: 16.0)
        //var holes: String!

        //detailLabel.text = "18 Holes, Par 72, Slope 118"
        blackView.addSubview(detailLabel)
        
        detailLabel.autoPinEdgeToSuperviewEdge(ALEdge.Left, withInset: 15.0)
        detailLabel.autoPinEdgeToSuperviewEdge(ALEdge.Bottom, withInset: 5.0)
    }

}
