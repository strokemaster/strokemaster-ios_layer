//
//  StatsRangesView.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 6/26/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class StatsRangesView: UIView {
    
    let firstValue = UILabel()
    let midValue = UILabel()
    let finalValue = UILabel()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        firstValue.textColor = kscoreOrangeColor
        self.addSubview(firstValue)
        firstValue.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsMake(0, 0, 0, 0), excludingEdge: ALEdge.Right)
        
        midValue.textColor = kscoreYellowColor
        self.addSubview(midValue)
        midValue.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        midValue.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        
        finalValue.textColor = kscoreGreenColor
        self.addSubview(finalValue)
        finalValue.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsMake(0, 0, 0, 0), excludingEdge: ALEdge.Left)

        //Separator lines
        
//        let firstSeparator = UIView()
//        firstSeparator.backgroundColor = UIColor.whiteColor()
//        self.addSubview(firstSeparator)
//        
//        firstSeparator.autoPinEdge(ALEdge.Left, toEdge: ALEdge.Right, ofView: firstValue, withOffset: 2.0)
//        firstSeparator.autoPinEdge(ALEdge.Right, toEdge: ALEdge.Left, ofView: midValue, withOffset: -2.0)
//        firstSeparator.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
//        firstSeparator.autoSetDimension(ALDimension.Height, toSize: 1.0)
//        
//        let secondSeparator = UIView()
//        secondSeparator.backgroundColor = UIColor.whiteColor()
//        self.addSubview(secondSeparator)
//        
//        secondSeparator.autoPinEdge(ALEdge.Left, toEdge: ALEdge.Right, ofView: midValue, withOffset: 2.0)
//        secondSeparator.autoPinEdge(ALEdge.Right, toEdge: ALEdge.Left, ofView: finalValue, withOffset: -2.0)
//        secondSeparator.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
//        secondSeparator.autoSetDimension(ALDimension.Height, toSize: 1.0)
    }

}
