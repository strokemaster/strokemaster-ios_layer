//
//  SMGolfBag.swift
//  StrokeMaster-iOS
//
//  Created by TR Staake on 12/7/14.
//  Copyright (c) 2014 StrokeMaster. All rights reserved.
//

import Foundation
import UIKit

class SMGolfBag: NSObject {
    
    var array: Array<SMClub>!
    
    override init() {
        self.array = Array()
        super.init()
    }
    
    //Print clubs in our bag
    func printBag() {
        for club in array {
            print(club.description)
        }
    }
    
    func hasClub(newClub: SMClub) -> Bool {
        
        var result = Bool()
        
        for club in array {
            if club.type == newClub.type && club.number == newClub.number {
                result = true
            }
        }
        return result
    }
    
    //Add Club
    func addClub(newClub: SMClub) {
        if !self.hasClub(newClub) {
            self.array.append(newClub)
        }
    }
    
    //Delete
    func deleteClub(deleteClub: SMClub) {
        
        var index = 0
        
        for club in array {
            if club.type == deleteClub.type && club.number == deleteClub.number {
                self.array.removeAtIndex(index)
            }
            index++
        }
    }
    
    //Get count of a club type 
    func countForType(newClub: SMClub) -> NSInteger{
        var count = 0
        for club in array {
            if club.type == newClub.type {
                count++
            }
        }
        return count
    }
}
