//
//  SMUser.swift
//  StrokeMaster-iOS
//
//  Created by TR Staake on 12/7/14.
//  Copyright (c) 2014 StrokeMaster. All rights reserved.
//

import Foundation
import UIKit

class SMUser: NSObject {
    var firstName: String?
    var lastName: String?
    // to Goler var dateOfBirth: String?
    var eMail: String?
    var userName: String?
    var password: String?
    // To Golfer var gender: String?
    
    class var sharedUserInstance: SMUser{
        
        struct Singleton {
            static let userInstance = SMUser()
        }
        return Singleton.userInstance
    }
    
    
    //Constructor
    
    override init()
    {
        super.init()
    }
    
    //init(firstName fName: String, lastName lName:String, dateOfBirth dob:String, eMail mail :String, userName uName:String, gender isFemale:Bool)
    init(firstName fName: String, lastName lName:String, eMail mail :String, userName uName:String, userPassword localPassword: String)
    {
        self.firstName = fName
        self.lastName = lName
        self.eMail = mail
        self.userName = uName
        super.init()
        password = localPassword
    }
    //End Constructor
    
    
    //Getters and setters
//    func setFirstName(fName:String)
//    {
//        firstName = fName
//    }
    
    func getFirstName()->String
    {
        return self.firstName!
    }
    
//    func setLastName(lName:String)
//    {
//        lastName = lName
//    }
    
    func getLastName()->String
    {
        return self.lastName!
    }
    
    func setEmail(eMail:String)
    {
        self.eMail = eMail
    }
    
    func getEmail()->String
    {
        return self.eMail!
    }
    
//    func setUserName(uName:String)
//    {
//        userName = uName
//    }
    
    func getUserName()->String
    {
        return self.userName!
    }
    //END Getters and Setters
    
    //prints object for debugging
    func toString() -> String!
    {
        return "\tFirst Name: \(firstName!) \n\tLast Name: \(lastName!) \n\temail: \(eMail!) \n\tuser name: \(userName!)"
    }
    
    
    
    
}
