//
//  SMGolfer.swift
//  StrokeMaster-iOS
//
//  Created by TR Staake on 12/7/14.
//  Copyright (c) 2014 StrokeMaster. All rights reserved.
//

import Foundation
import UIKit
class SMStatsGolfer: SMUser
{
    //Start Variables
    var dateOfBirth: String?
    var gender: String?
    var homeCourse: String?
    var bag = SMGolfBag()
    
    //This will depend on what the iGolf API calls for when searching for a course.
    //var homeCourse: GolfCourse
    
    //End Variables
    
    //Start Contstructors
    override init()
    {
        super.init()
    }
    
    init(hasFirstName fName: String, andLastName lName:String, hasGender isFemale: Bool, hasEmail eMail:String, andUserName uName:String, hasPassword password: String, bornOn dateBorn:String, hasHomeCourse hCourse: String, golfBag: SMGolfBag)
    {
        //bag = golfBag
        dateOfBirth = dateBorn
        homeCourse = hCourse
        super.init(firstName:fName, lastName: lName, eMail: eMail, userName: uName, userPassword:password)
        gender = selectGender(isFemale)!
        
    }
    //End Constructors
    
    //Start Getters and Setters
//    func setDateOfBirth(dob:String)
//    {
//        dateOfBirth = dob
//    }
    
    func getDateOfBirth()->String
    {
        return self.dateOfBirth!
    }
    
//    func setGender(gender:String)
//    {
//        self.gender = gender
//    }
    
    func getGender()->String
    {
        return self.gender!
    }
    
    //Sets gender from bool switch
    func selectGender(isMale:Bool) -> String!
    {
        return (isMale ? "Male":"Female")
    }
    //End Getters and Setters
    
    //Debuggs to console
    override func toString() -> String!
    {
        return "\(super.toString()) \n\tGender: \(gender!)\n\tdob: \(dateOfBirth!)\n\thome course: \(homeCourse!)"
    }
    
    
    
}
