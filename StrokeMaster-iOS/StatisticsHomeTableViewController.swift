//
//  StatisticsHomeTableViewController.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 6/17/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class StatisticsHomeTableViewController: UITableViewController {
    
    let cellIdentifier = "StatisticsHomeCell"
    var json: JSON = nil
    let titles = ["Game Summary Page", "Leaderboard", "Clubs Stats Page", "Diagnostics"]
    let color = [kTransparentBlue, kTransparentRed, kLightGreenColor, kOrangeColor]
    var statsViewControllers = [UITableViewController]()

    init() {
        super.init(style: .Plain)
        let userdefaults = NSUserDefaults.standardUserDefaults()
        
        let semaphore = dispatch_semaphore_create(0)
        
        let email = userdefaults.objectForKey("Email") as! String
        
        DataManager.getGameSummary(email, success:{(results) -> Void in
            let result = results
            self.json = JSON(data: result)
            
            dispatch_semaphore_signal(semaphore)
        })
        
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)
        
        statsViewControllers = [StatisticsTableViewController(json: json), LeaderboardTableViewController(), ClubStatsTableViewController(), DiagnosticTableViewController()]
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Statistics"
        
        //TableView UI
        self.view.backgroundColor = UIColor.clearColor()
        self.tableView.separatorColor = UIColor.clearColor()
        self.tableView.rowHeight = 100.0
        
        //Create MenuButton
        let leftDrawerButton = UIBarButtonItem(image: UIImage(named: "menuButton"), style: UIBarButtonItemStyle.Plain, target: self, action: "menuPressed")
        self.navigationItem.setLeftBarButtonItem(leftDrawerButton, animated: false)
        
        //BackButton
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: self, action: "backPressed:")
        self.navigationItem.backBarButtonItem = backButton
    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as UITableViewCell?
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
        }
        
        cell?.selectionStyle = .None
        cell?.backgroundColor = color[indexPath.row] as UIColor

        let title = UILabel()
        title.text = titles[indexPath.row] as String
        title.textColor = UIColor.whiteColor()
        title.font = UIFont(name: "\(kDefaultFont)-Bold", size: 22.0)
        title.textAlignment = NSTextAlignment.Center
        cell?.addSubview(title)
        
        title.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsZero)
        
        return cell!
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 100.0
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return createTopLabels()
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.navigationController?.customPush(statsViewControllers[indexPath.row] as UIViewController)
    }
    
    //UI For Labels at top
    
    func createTopLabels() -> UIView {
        
        let headerView = UIView()
        
        //Static Labels
        let averageLabel = UILabel()
        averageLabel.textColor = kBrightGreenColor
        averageLabel.text = "Average"
        averageLabel.font = UIFont(name: "\(kDefaultFont)-Medium", size: 16.0)
        headerView.addSubview(averageLabel)
        
        averageLabel.autoPinEdgeToSuperviewEdge(ALEdge.Left, withInset: 15.0)
        averageLabel.autoPinEdgeToSuperviewEdge(ALEdge.Top, withInset: 15.0)
        
        let statsLabel = UILabel()
        statsLabel.textColor = UIColor.whiteColor()
        statsLabel.text = "Stats"
        statsLabel.font = UIFont(name: "\(kDefaultFont)-Medium", size: 24.0)
        headerView.addSubview(statsLabel)
        
        statsLabel.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        statsLabel.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        
        let roundsLabel = UILabel()
        roundsLabel.textColor = kBrightGreenColor
        roundsLabel.text = "Rounds"
        roundsLabel.font = UIFont(name: "\(kDefaultFont)-Medium", size: 16.0)
        headerView.addSubview(roundsLabel)
        
        roundsLabel.autoPinEdgeToSuperviewEdge(ALEdge.Right, withInset: 15.0)
        roundsLabel.autoPinEdgeToSuperviewEdge(ALEdge.Top, withInset: 15.0)
        
        //Changing Labels
        let averageDataLabel = UILabel()
        averageDataLabel.textColor = .whiteColor()
        let avg = self.json["Overall"].int
        averageDataLabel.text = (avg == nil ? "0" : String(avg!))
        averageDataLabel.font = UIFont(name: "\(kDefaultFont)-Bold", size: 20.0)
        headerView.addSubview(averageDataLabel)
        
        averageDataLabel.autoAlignAxis(ALAxis.Vertical, toSameAxisOfView: averageLabel)
        averageDataLabel.autoPinEdge(ALEdge.Top, toEdge: ALEdge.Bottom, ofView: averageLabel, withOffset: 5.0)
        
        let roundsDataLabel = UILabel()
        roundsDataLabel.textColor = .whiteColor()
        let scoreValues = DataManager.stringToDoubleArray(self.json["Scores"].string!)
        roundsDataLabel.text = String(scoreValues.count)
        roundsDataLabel.font = UIFont(name: "\(kDefaultFont)-Bold", size: 20.0)
        headerView.addSubview(roundsDataLabel)
        
        roundsDataLabel.autoAlignAxis(ALAxis.Vertical, toSameAxisOfView: roundsLabel)
        roundsDataLabel.autoPinEdge(ALEdge.Top, toEdge: ALEdge.Bottom, ofView: roundsLabel, withOffset: 5.0)
        
        return headerView
    }
    
    //MARK: - Open left side menu
    func menuPressed() {
        self.evo_drawerController?.toggleDrawerSide(.Left, animated: true, completion: nil)
    }

}
