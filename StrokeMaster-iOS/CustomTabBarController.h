//
//  CustomTabBarController.h
//  AA2000
//
//

#import <UIKit/UIKit.h>
#import "HorizontalButtonsScroll.h"

@interface CustomTabBarController : UIViewController 

@property (nonatomic, strong) NSArray *viewControllers;

@property (nonatomic, strong, readonly) HorizontalButtonsScroll *horizontalButtonsScroll;
@property (nonatomic, strong, readonly) UIPageViewController *pageViewController;
@property (nonatomic, retain) UIColor *colorRGB;
@property (nonatomic, assign) BOOL showsCalendar;

@property (nonatomic, assign) NSInteger selectedIndex;

// If buttonsSize equals CGSizeZero, then each button size is automatically calculated
@property (nonatomic, assign) CGFloat buttonsWidth;

- (void)setSelectedIndex:(NSInteger)selectedIndex animated:(BOOL)animated;

@end
