//
//  DiagnosticTableViewCell.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 7/3/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class DiagnosticTableViewCell: UITableViewCell {
    
    let shotTypeLabel = UILabel()
    let indexLabel = UILabel()
    let valueLabel = UILabel()
    let excessLabel = UILabel()

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.clearColor()
        self.selectionStyle = .None
        
        shotTypeLabel.textColor = UIColor.whiteColor()
        self.contentView.addSubview(shotTypeLabel)
        shotTypeLabel.autoPinEdgeToSuperviewEdge(ALEdge.Left, withInset: 15.0)
        shotTypeLabel.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        
        indexLabel.textColor = UIColor.whiteColor()
        self.contentView.addSubview(indexLabel)
        indexLabel.autoAlignAxis(ALAxis.Vertical, toSameAxisOfView: self.contentView, withOffset: -SCREEN_WIDTH/10)
        indexLabel.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        
        valueLabel.textColor = UIColor.whiteColor()
        self.contentView.addSubview(valueLabel)
        valueLabel.autoAlignAxis(ALAxis.Vertical, toSameAxisOfView: self.contentView, withOffset: SCREEN_WIDTH/7)
        valueLabel.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        
        excessLabel.textColor = UIColor.whiteColor()
        excessLabel.font = UIFont.boldSystemFontOfSize(17.0)
        self.contentView.addSubview(excessLabel)
        excessLabel.autoPinEdgeToSuperviewEdge(ALEdge.Right, withInset: 15.0)
        excessLabel.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        
    }
}
