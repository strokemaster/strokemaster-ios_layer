//
//  HorizontalButtonsScrollCell.m
//  Vorterix
//

#import "HorizontalButtonsScrollCell.h"
#import "PureLayout.h"

#define APPLICATION_COLOR [UIColor colorWithRed:29.0/255.0 green:83.0/255.0 blue:29.0/255.0 alpha:1.0]
#define SCREEN_RECT [[UIScreen mainScreen] bounds]
#define kDefaultFont "HelveticaNeue"

@interface HorizontalButtonsScrollCell ()
{
    NSArray *_constraints;
    NSString *name;
}
@end

@implementation HorizontalButtonsScrollCell

+ (CGSize)cellSizeForTitle:(NSString *)title subtitle:(NSString *)subtitle
{
    CGRect rect = [title boundingRectWithSize:CGSizeMake(0, 0)
                                      options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:@{NSFontAttributeName:@"Helvetica"}
                                      context:nil];
    CGSize size = CGSizeMake(rect.size.width + 31.5, 43);
    
    return size;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor blackColor];
        
        //No separators Agenda Cultural
        UIView *leftSeparator = [[UIView alloc] init];
        leftSeparator.backgroundColor = [UIColor clearColor];
        [leftSeparator setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self addSubview:leftSeparator];
        
        UIView *rightSeparator = [[UIView alloc] init];
        rightSeparator.backgroundColor = [UIColor clearColor];
        [rightSeparator setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self addSubview:rightSeparator];
        
        NSDictionary *views = NSDictionaryOfVariableBindings(leftSeparator, rightSeparator);
        NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(-1)-[leftSeparator(==1)]"
                                                                       options: 0
                                                                       metrics:nil
                                                                         views:views];
        [self addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[rightSeparator(==leftSeparator)]|"
                                                                       options: 0
                                                                       metrics:nil
                                                                         views:views];
        [self addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-6-[leftSeparator(==28)]"
                                                                       options: 0
                                                                       metrics:nil
                                                                         views:views];
        [self addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-6-[rightSeparator(==leftSeparator)]"
                                                              options: 0
                                                              metrics:nil
                                                                views:views];
        [self addConstraints:constraints];
        
        _title = [[UILabel alloc] init];
        _title.textAlignment = NSTextAlignmentCenter;
        [_title setTextColor: [UIColor whiteColor]];
        [_title setNumberOfLines:1];
        [_title setFont:[UIFont fontWithName:[NSString stringWithFormat:@"%s-Light", kDefaultFont] size:14.0]];
        [_title setTranslatesAutoresizingMaskIntoConstraints:NO];

        [self addSubview:_title];
        
        _subtitle = [[UILabel alloc] init];
        [_subtitle setTextColor:[UIColor redColor]];
        [_subtitle setFont:[UIFont fontWithName:@"Helvetica" size:12.0]];
        [_subtitle setNumberOfLines:1];
        [_subtitle setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self addSubview:_subtitle];
        
        _fakeColorView = [[UIView alloc] init];
        _fakeColorView.backgroundColor = [UIColor clearColor];
        [self addSubview:_fakeColorView];
        _colorView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        _colorView.backgroundColor = [UIColor clearColor];
        [_colorView setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self addSubview:_colorView];
        
//        views = NSDictionaryOfVariableBindings(_colorView, _fakeColorView);
//        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_colorView]|"
//                                                                       options: 0
//                                                                       metrics:nil
//                                                                         views:views];
//        [self addConstraints:constraints];
//        
//        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[_colorView(==_fakeColorView)]|"
//                                                              options: 0
//                                                              metrics:nil
//                                                                views:views];
//        [self addConstraints:constraints];
        
    }
    return self;
}

- (void)setTitle:(NSString *)title subtitle:(NSString *)subtitle
{
    
    _title.text = title;
    
    if (subtitle) {
        [_subtitle setText:subtitle];
    }
    
    if (!_constraints) {
        NSDictionary *views;
        if (subtitle) {
            
            views = NSDictionaryOfVariableBindings(_title, _subtitle);
            _constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15.5-[_title]-15.5-|"
                                                                           options: 0
                                                                           metrics:nil
                                                                             views:views];
            [self addConstraints:_constraints];
            
            _constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-2-[_title]-(-5)-[_subtitle]"
                                                                  options: 0
                                                                  metrics:nil
                                                                    views:views];
            [self addConstraints:_constraints];
            
            NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:_subtitle
                                                                          attribute:NSLayoutAttributeCenterX
                                                                          relatedBy:NSLayoutRelationEqual
                                                                             toItem:_title
                                                                          attribute:NSLayoutAttributeCenterX
                                                                         multiplier:1.0
                                                                           constant:0.0];
            [self addConstraint:constraint];
        } else {
            
            views = NSDictionaryOfVariableBindings(_title);
            _constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[_title]-0-|"
                                                                           options: 0
                                                                           metrics:nil
                                                                             views:views];
            [self addConstraints:_constraints];
            
            [_title autoAlignAxisToSuperviewAxis:ALAxisVertical];
            
            [_fakeColorView autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:_title];
            [_fakeColorView autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:_title];
            [_fakeColorView autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:3.0];
            [_fakeColorView autoSetDimension:ALDimensionHeight toSize:1.0];
        }
    }
}

- (void)deselect
{
//    _fakeColorView.frame = CGRectMake(0, 0, 0, 0);
    _fakeColorView.backgroundColor = [UIColor clearColor];
}

- (void)select
{
    _fakeColorView.backgroundColor = APPLICATION_COLOR;
//        _fakeColorView.backgroundColor = [];
}


- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    
    if (selected) {
        [self select];
        _title.textColor = [UIColor whiteColor];
    }else{
        [self deselect];
        _title.textColor = [UIColor whiteColor];
    }
}

@end
