//
//  CourseScorecard.swift
//  StrokeMaster-iOS
//
//  Created by Greg Morano on 10/27/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class CourseTeeDetails: NSObject {
    
    let courseId: String!
    let ratingMen: Double!
    let ratingWomen: Double!
    let slopeMen: Int!
    let slopeWomen: Int!
    let teeName: String!
    let teeColorName: String!
    let teeColorValue: String!
    let ydsTotal: Int!
    let yds1to9: String!
    let yds10to18: String!
    let yds1to18: String!
    let ydsHole: [Int]!
    
    
    init(json :JSON) {
        self.courseId = json["courseId"].string
        self.ratingMen = json["ratingMen"].double
        self.ratingWomen = json["ratingWomen"].double
        self.slopeMen = json["slopeMen"].int
        self.slopeWomen = json["slopeWomen"].int
        self.teeName = json["teeName"].string
        self.teeColorName = json["teeColorName"].string
        self.teeColorValue = json["teeColorValue"].string
        self.ydsTotal = json["ydsTotal"].int
        self.yds1to9 = json["yds1to9"].string
        self.yds10to18 = json["yds10to18"].string
        self.yds1to18 = json["yds1to18"].string
        self.ydsHole = DataManager.stringToIntArray( json["ydsHole"].string!)
        
        
        super.init()
    }
    
    
    
    
}