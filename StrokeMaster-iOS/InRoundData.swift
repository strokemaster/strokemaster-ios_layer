//
//  InRoundData.swift
//  StrokeMaster-iOS
//
//  Created by Greg Morano on 8/20/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class InRoundData: NSObject {
    
    var userId: String!
    var courseName: String!
    var par: String!
    var score: String!
    var totalScore: Int!
    var totalPar: Int!
    var startHole: Int!
    var holes: [HoleData]!

    
    init(userId:String, name:String){
        self.userId = userId
        self.courseName = name
        self.par =  ""
        self.score = ""
        self.totalScore = -1
        self.totalPar = -1
        self.startHole = 1
        self.holes = [HoleData]()
    }
    
    
    
    func toJSONString() -> String{
        
        //here for my reference not needed once tested - Greg
        //var dataString: String = "{\n \"UserId\" : \"123456\",\n \"CourseId\" : \"543asd\",\n \"Score\": \"80\",\n \"Holes\" : [\n {\"Shots\" : [ 300, 75, 10, 3],\n \"Clubs\" : [ 1, 12, 20, 20],\n \"Terrain\": [ 1, 3, 5, 5],\n \"Par\" : 4},\n {\"Shots\" : [ 300, 75, 10, 3],\n \"Clubs\" : [ 1, 12, 20, 20],\n \"Terrain\": [ 1, 3, 5, 5],\n \"Par\" : 4\n}\n]\n}\n"
        
        var result : String = ""
        
        result += "{\n \"UserId\" : \""
        result += String(self.userId)
        result += "\",\n \"CourseId\" : \""
        result += self.courseName
        result += "\",\n \"Par\" : \""
        result += self.par
        result += "\",\n \"Score\" : \""
        result += self.score
        result += "\",\n \"TotalScore\" : \""
        result += String(self.totalScore)
        result += "\",\n \"TotalPar\" : \""
        result += String(self.totalPar)
        result += "\",\n \"StartHole\" : \""
        result += String(self.startHole)
        result += "\",\n \"Holes\" : [ "
        
        for hole in holes {
            
            result += "\n{\"Clubs\" : "
            result += hole.clubs.description
            result += ",\n \"Terrain\" : "
            result += hole.terrain.description
            result += ",\n \"Yardages\" : "
            result += hole.yardage.description
            result += ",\n \"StartDist\" : "
            result += hole.startDist.description
            result += ",\n \"EndDist\" : "
            result += hole.endDist.description
            result += ",\n \"StartPoint\" : "
            result += hole.startPoint.description
            result += ",\n \"EndPoint\" : "
            result += hole.endPoint.description
            result += ",\n \"Par\" : \""
            result += String(hole.par)
            result += "\"\n},"
            
        }
        result.removeAtIndex(result.endIndex.predecessor())
        result += "\n]\n}\n"
        
        return result
    }
    
}

