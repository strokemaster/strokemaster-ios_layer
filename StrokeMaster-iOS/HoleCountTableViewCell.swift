//
//  HoleCountTableViewCell.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/22/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class HoleCountTableViewCell: UITableViewCell {
    
    let label = UILabel()
    let segmentedControl = UISegmentedControl(items: ["9", "18"])
    let nineHolesButton = UIButton()
    let allHolesButton = UIButton()

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .None
        self.backgroundColor = UIColor.clearColor()
        
        label.textColor = UIColor.whiteColor()
        label.font = UIFont(name: "\(kDefaultFont)-Roman", size: 14.0)
        label.text = "Number of holes"
        self.contentView.addSubview(label)
        
        label.autoPinEdgeToSuperviewEdge(ALEdge.Left, withInset: 25.0)
        label.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
    
//        segmentedControl.selectedSegmentIndex = 0
//        segmentedControl.tintColor = UIColor.blackColor()
//        segmentedControl.backgroundColor = UIColor.whiteColor()
//        segmentedControl.layer.cornerRadius = 5.0
//        self.contentView.addSubview(segmentedControl)
//        
//        segmentedControl.autoPinEdgeToSuperviewEdge(ALEdge.Right, withInset: 15.0)
//        segmentedControl.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
//        segmentedControl.autoSetDimensionsToSize(CGSizeMake(85, 30))
        
        allHolesButton.setTitle("18", forState: UIControlState.Normal)
        allHolesButton.circularImageViewWithWidth(40.0)
        allHolesButton.addTarget(self, action: "buttonPressed:", forControlEvents: UIControlEvents.TouchUpInside)
        allHolesButton.titleLabel?.textAlignment = NSTextAlignment.Center
        allHolesButton.layer.backgroundColor = kSettingsGreenColor.CGColor
        allHolesButton.tag = 0
        allHolesButton.selected = true
        self.contentView.addSubview(allHolesButton)
        allHolesButton.autoPinEdgeToSuperviewEdge(ALEdge.Right, withInset: 25.0)
        allHolesButton.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        allHolesButton.autoSetDimensionsToSize(CGSizeMake(40.0, 40.0))
        
        let separatorView = UIView()
        separatorView.backgroundColor = UIColor.lightGrayColor()
        self.contentView.addSubview(separatorView)
        separatorView.autoPinEdge(ALEdge.Right, toEdge: ALEdge.Left, ofView: allHolesButton, withOffset: -10.0)
        separatorView.autoSetDimensionsToSize(CGSizeMake(1.0, 25.0))
        separatorView.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        
        nineHolesButton.setTitle("9", forState: UIControlState.Normal)
        nineHolesButton.circularImageViewWithWidth(40.0)
        nineHolesButton.addTarget(self, action: "buttonPressed:", forControlEvents: UIControlEvents.TouchUpInside)
        nineHolesButton.titleLabel?.textAlignment = NSTextAlignment.Center
        nineHolesButton.tag = 1
        self.contentView.addSubview(nineHolesButton)
        nineHolesButton.autoPinEdge(ALEdge.Right, toEdge: ALEdge.Left, ofView: separatorView, withOffset: -10.0)
        nineHolesButton.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        nineHolesButton.autoSetDimensionsToSize(CGSizeMake(40.0, 40.0))
    }
    
        //Buttons pressed
    func buttonPressed(sender: UIButton) {
        
        sender.selected = !sender.selected
        
        if sender.tag == 0 {
            UIView.animateWithDuration(0.15, animations: {
                sender.layer.backgroundColor = kSettingsGreenColor.CGColor
                self.nineHolesButton.layer.backgroundColor = UIColor.clearColor().CGColor
                self.nineHolesButton.selected = false
            })
        } else {
            UIView.animateWithDuration(0.15, animations: {
                sender.layer.backgroundColor = kSettingsGreenColor.CGColor
                self.allHolesButton.layer.backgroundColor = UIColor.clearColor().CGColor
                self.allHolesButton.selected = false
            })
        }
    }
}
