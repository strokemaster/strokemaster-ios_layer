//
//  BagTableViewController.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/16/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit
import CoreData

class BagTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var bag: SMGolfBag!
    let headersArray = ["Woods", "Hybrids", "Irons", "Wedges"]
    var clubList = [SMClub]()
    var clubsLabel: UILabel!
    var userdefaults = NSUserDefaults.standardUserDefaults()
    var myArray : NSArray!
    var userEmail: String!
    //Bag for backend
    var golfBag = [Int]()

    //Clubs for type count
    var woodsCount: Int!
    var hybridsCount: Int!
    var ironsCount: Int!
    var wedgesCount: Int!
    
    //TableView
    var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.myArray = userdefaults.objectForKey("Clubs") as! NSArray
        self.userEmail = userdefaults.objectForKey("Email") as! String
        
        let semaphore = dispatch_semaphore_create(0)
        var datastring: String = ""
        
        DataManager.getClubsList("all", success:{(clubsResult) -> Void in
            let result = clubsResult
            let datansstring: NSString = NSString(data: result, encoding: NSUTF8StringEncoding)!
            datastring = datansstring as String
            dispatch_semaphore_signal(semaphore)
        })
        
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)
        
        
        woodsCount = 0
        hybridsCount = 0
        ironsCount = 0
        wedgesCount = 0
        parseClubList(datastring)
        
        addCurrentClubsToBag()
        
        //Title
        self.title = "My Bag"
        
        //Add background Image
        let imageView = UIImageView(image: UIImage(named: "background"))
        self.view.addSubview(imageView)
        imageView.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsZero)
        
        //TableView UI
        tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = UIColor.clearColor()
        self.tableView.tableHeaderView = UIView()
        //Save
        self.saveButtonView()
        self.view.addSubview(tableView)
        tableView.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsZero)
        
        //BAG
        bag = SMGolfBag()
        
        //Navigation Label
        clubsLabel = UILabel()
        clubsLabel.text = "\(golfBag.count)/13"
        clubsLabel.textColor = UIColor.whiteColor()
        clubsLabel.font = UIFont(name: "\(kDefaultFont)-Light", size: 22.0)
        
        if self.navigationController != nil {
            self.navigationController?.view.addSubview(clubsLabel)
            clubsLabel.autoPinEdgeToSuperviewEdge(ALEdge.Right, withInset: 15.0)
            clubsLabel.autoAlignAxis(ALAxis.Horizontal, toSameAxisOfView: (self.navigationController?.navigationBar)!,
                withOffset: -2.0)
        }
    }
    
    
    override func viewWillDisappear(animated: Bool) {
        clubsLabel.removeFromSuperview()
    }

    // MARK: - Table view data source

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        var numSecs = 0
        if(woodsCount > 0){
            numSecs = numSecs + 1
        }
        if(hybridsCount > 0){
            numSecs = numSecs + 1
        }
        if(ironsCount > 0){
            numSecs = numSecs + 1
        }
        if(wedgesCount > 0){
            numSecs = numSecs + 1
        }
        
        return numSecs
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0 && woodsCount > 0){
            return woodsCount
        }
        else if(section == 0 && hybridsCount > 0){
            return hybridsCount
        }
        else if(section == 0){
            return ironsCount
        }
        
        if(section == 1 && woodsCount > 0 && hybridsCount > 0){
            return hybridsCount
        }
        else if(section == 1 && woodsCount > 0 && hybridsCount == 0 && ironsCount > 0){
            return ironsCount
        }
        else if(section == 1 && woodsCount == 0 && hybridsCount > 0 && ironsCount > 0){
            return ironsCount
        }
        else if(section == 1){
            return wedgesCount
        }
        
        
        if(section == 2 && woodsCount > 0 && hybridsCount > 0 && ironsCount > 0){
            return ironsCount
        }
        else if(section == 2){
            return wedgesCount
        }
        
        if(section == 3){
            return wedgesCount
        }
        
        return 0
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("sectionCell") as? BagTableViewCell
        if cell == nil {
            cell = BagTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "sectionCell")
        }
        
        cell?.removeTick()
        
        let club = getClubForIndexPath(indexPath)
        let clubNumber = getClubNumberForIndexPath(indexPath)
        
        if golfBag.contains(clubNumber) {
            cell?.addTick()
        }
        
        
        cell?.label.text = club.name
        
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! BagTableViewCell
        
        let value = getClubNumberForIndexPath(indexPath)
        
        if golfBag.contains(value) {
            for (index, number) in golfBag.enumerate() {
                if number == value {
                    golfBag.removeAtIndex(index)
                    cell.removeTick()
                    break
                }
            }
        } else {
            if golfBag.count < 13 {
                golfBag.append(value)
                cell.addTick()
            }
        }
        
        clubsLabel.text = "\(golfBag.count)/13"
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
       
        let view = UIView(frame: CGRectMake(0, 0, SCREEN_WIDTH, 40))
        view.backgroundColor = kBlackTransparentColor
        
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue-Roman", size: 18.0)
        label.textColor = UIColor.whiteColor()
        label.text = headersArray[section]
        view.addSubview(label)
        label.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsMake(0, 15.0, 0, 0))
        
        return view
    }

    func saveButtonView() {
        
        let saveButton = UIButton(frame: CGRectMake(0, 0, SCREEN_WIDTH, 60.0))
        saveButton.transparentButton("SAVE BAG")
        saveButton.backgroundColor = kLightGreenColor
        saveButton.addTarget(self, action: "saveBag", forControlEvents: UIControlEvents.TouchUpInside)
        self.tableView.tableFooterView = saveButton
    }
    
    //Send to backend
    func saveBag() {
        
        let semaphore = dispatch_semaphore_create(0)
        
        //turn list of club id to list of club names
        var clubNames = [String]()
        golfBag.sortInPlace(<)
        for id in golfBag {
            clubNames.append(clubList[id-1].name!)
        }
        let json = JSON([["Email" : userEmail, "Clubs": clubNames]])
        var res: Int = -1
        
        
        DataManager.sendGolfBag(json, success:{(loginResult) -> Void in
            res = loginResult
            dispatch_semaphore_signal(semaphore)
        })
        
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)
        
        //successfully saved the bag display success
        if(res == 1){
            
            saveClubData(clubNames)
            print("Bag Save Success", terminator: "")
        }
            //save failed display message need to do
        else{
            print("Bag save failed", terminator: "")
        }
        
        
    }
    
    //Get club for indexPath
    func getClubForIndexPath(indexPath: NSIndexPath) -> SMClub {
        
        switch indexPath.section {
        case 0:
            return clubList[indexPath.row]
        case 1:
            return clubList[indexPath.row + woodsCount]
        case 2:
            return clubList[indexPath.row + woodsCount + hybridsCount]
        default:
            return clubList[indexPath.row + woodsCount + hybridsCount + ironsCount]
        }
    }
    
    //Get club for indexPath
    func getClubNumberForIndexPath(indexPath: NSIndexPath) -> Int {
        
        switch indexPath.section {
        case 0:
            return indexPath.row + 1
        case 1:
            return indexPath.row + woodsCount + 1
        case 2:
            return indexPath.row + woodsCount + hybridsCount + 1
        default:
            return indexPath.row + woodsCount + hybridsCount + ironsCount + 1
        }
    }
    
    
    func parseClubList(clubLists: String) {
        let clubArr = clubLists.componentsSeparatedByString(", ")
        var count = 1
        var num = 0
        for club in clubArr {
            
            if(  Int(String(Array(arrayLiteral: club)[0])) != nil){
                num = Int(String(Array(arrayLiteral: club)[0]))!
            }
            else{
                num = 1
            }
            
            if club.lowercaseString.rangeOfString("wood") != nil || club.lowercaseString.rangeOfString("driver") != nil {
                count = 0
                woodsCount = woodsCount + 1
            }
            else if club.lowercaseString.rangeOfString("hybrid") != nil{
                count = 1
                hybridsCount = hybridsCount + 1
            }
            else if club.lowercaseString.rangeOfString("iron") != nil{
                count = 2
                ironsCount = ironsCount + 1
            }
            else if club.lowercaseString.rangeOfString("wedge") != nil{
                count = 3
                wedgesCount = wedgesCount + 1
            }
            
            let newclub = SMClub(type: Club(rawValue: count), number: num, name: club)
            clubList.append(newclub)
            
        }
    }
    
    func saveClubData(names: [String]) {
        
        let userdefaults = NSUserDefaults.standardUserDefaults()
        userdefaults.setObject(names, forKey: "Clubs")
        userdefaults.synchronize()
        
    }
    
    
    func addCurrentClubsToBag(){
        
        for myclub in myArray {
            
            for var i = 0; i < clubList.count; i++ {
                let club = clubList[i]
                if(club.name! == myclub as! String){
                    self.golfBag.append(i+1)
                }
                
            }
            
        }
        
    }
}
