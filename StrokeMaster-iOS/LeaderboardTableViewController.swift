//
//  LeaderboardTableViewController.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 6/17/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class LeaderboardTableViewController: UITableViewController {
    
    let cellIdentifier = "LeaderboardCell"
    var json: JSON = nil
    
    init() {
        super.init(style: .Plain)

        let semaphore = dispatch_semaphore_create(0)
        
        
        DataManager.getLeaderboard({(results) -> Void in
            let result = results
            self.json = JSON(data: result)
            
            dispatch_semaphore_signal(semaphore)
        })
        
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Leaderboard"

        //Table UI
        self.tableView.backgroundColor = UIColor.clearColor()
        self.tableView.separatorColor = UIColor.clearColor()
        
        //Table Header
        let headerLabel = UILabel(frame: CGRectMake(0, 0, SCREEN_WIDTH, 50))
        headerLabel.backgroundColor = kBlackTransparentColor
        headerLabel.textColor = UIColor.whiteColor()
        headerLabel.text = "Top 10"
        headerLabel.font = UIFont(name: "\(kDefaultFont)-Medium", size: 22.0)
        headerLabel.textAlignment = NSTextAlignment.Center
        
        self.tableView.tableHeaderView = headerLabel
        self.tableView.tableFooterView = UIView()
    }

    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 1 : json.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? LeaderboardTableViewCell
        if cell == nil {
            cell = LeaderboardTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
        }
        
        if indexPath.section == 0 {
            cell?.setUpAsHeader()
        } else {
        
            cell?.rankLabel.text = "\(indexPath.row + 1)"
            cell?.playerLabel.text = (json[indexPath.row]["userName"]).string
            cell?.playerLabel.font = UIFont(name: "\(kDefaultFont)-Medium", size: 12.0)
            cell?.scoreLabel.text = String((json[indexPath.row]["avgScore"]).double!)
            cell?.roundsLabel.text = String((json[indexPath.row]["numRounds"]).int!)
        }
        
        return cell!
    }
}
