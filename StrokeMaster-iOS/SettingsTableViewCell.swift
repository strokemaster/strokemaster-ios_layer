//
//  SettingsTableViewCell.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/13/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

protocol SettingsDelegate {
    func switchBackground(status: Bool)
}

class SettingsTableViewCell: UITableViewCell {
    
    let label = UILabel()
    let icon = UIImageView()
    var delegate: SettingsDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.clearColor()
        self.selectionStyle = .None
    
//        self.contentView.addSubview(icon)
//        icon.autoPinEdgeToSuperviewEdge(ALEdge.Left, withInset: 15.0)
//        icon.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        
        label.font = UIFont(name: "\(kDefaultFont)-Bold", size: 16.0)
        label.numberOfLines = 2
        label.textColor = UIColor.whiteColor()
        self.contentView.addSubview(label)
        
        label.autoPinEdgeToSuperviewEdge(ALEdge.Left, withInset: 15.0)
        label.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        
        //No switch for background change for now
        
//        let switcher = UISwitch()
//        switcher.onTintColor = kSettingsGreenColor
//        switcher.addTarget(self, action: "switchChanged:", forControlEvents: UIControlEvents.ValueChanged)
//        switcher.setOn(true, animated: false)
//        self.contentView.addSubview(switcher)
//        
//        switcher.autoPinEdgeToSuperviewEdge(ALEdge.Right, withInset: 15.0)
//        switcher.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
    }
    
    func switchChanged(sender: UISwitch) {
        self.delegate?.switchBackground(sender.on)
    }
}