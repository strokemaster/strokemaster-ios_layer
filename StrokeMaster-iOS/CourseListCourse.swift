//
//  CourseListCourse.swift
//  StrokeMaster-iOS
//
//  Created by Greg Morano on 5/27/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit


class CourseListCourse: NSObject {

    let id:String!
    let name: String!
    let address: String!
    let city: String!
    let state: String!
    let courseId: String!
    let numHole: NSInteger!
    
    //Added score card to Course Object so we don't have to manage Scorecard data separetly from Course.
    var scoreCardInfo: CourseScorecard?
    
    //added tee details
    var teeDetails: CourseTeeDetails?
    
    init(id: String!, name: String!, address: String!, city: String!, state: String!, courseId: String!, numHole: NSInteger!) {
        self.id = id
        self.name = name
        self.address = address
        self.city = city
        self.state = state
        self.courseId = courseId
        self.numHole = numHole
        super.init()
    }
    
}