//
//  Extensions.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/4/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController {
    
    
    
    convenience init(viewController: UIViewController) {
        self.init(rootViewController: viewController)
        self.navigationBar.barTintColor = UIColor.blackColor()
        self.navigationBar.translucent = false
        self.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "\(kDefaultFont)-Light", size: 26.0)!]
    }

    func customPush(viewController: UIViewController) {
        
        let transition = CATransition()
        transition.duration = 0.33
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        
        transition.timingFunction = CAMediaTimingFunction(name: "easeInEaseOut")
        
        self.view.layer.addAnimation(transition, forKey: "kCATransition")
        self.pushViewController(viewController, animated: false)
    }
    
    func customFade(viewController: UIViewController) {
        
        let transition = CATransition()
        transition.duration = 0.33
        transition.type = kCATransitionFade
        
        transition.timingFunction = CAMediaTimingFunction(name: "easeInEaseOut")
        
        self.view.layer.addAnimation(transition, forKey: "kCATransition")
        self.pushViewController(viewController, animated: false)
    }
    
    func customPop() {
        
        let transition = CATransition()
        transition.duration = 0.33
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        
        transition.timingFunction = CAMediaTimingFunction(name: "easeInEaseOut")
        
        self.view.layer.addAnimation(transition, forKey: "kCATransition")
        self.popViewControllerAnimated(false)
    }
}

extension UIButton {
    
    func transparentButton(title: String) {
    
        self.setTitle(title, forState: UIControlState.Normal)
        self.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        self.titleLabel?.font = UIFont(name: "\(kDefaultFont)-Medium", size: 15.0)
        self.titleLabel?.numberOfLines = 2
        self.titleLabel?.textAlignment = NSTextAlignment.Center
        self.transparentBlackView()
    }

    func transparentButtonWithTitle(title: String) {
        
        self.setTitle(title, forState: UIControlState.Normal)
        self.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        self.titleLabel?.font = UIFont(name: "\(kDefaultFont)-Light", size: 15.0)
        self.transparentBlackView()
        
        self.autoSetDimension(ALDimension.Height, toSize: 40.0)
    }
}

extension UIView {

    func transparentBlackView() {
        self.backgroundColor = kBlackTransparentColor
    }
    
    func transparentViewWithAlpha(alpha: CGFloat) {
        self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: alpha)

    }
    
    func drawBordersWithColorAndWidth(color: UIColor, width: CGFloat) {
        
        self.layer.borderWidth = width
        self.layer.borderColor = color.CGColor
    }
    
    func circularImageViewWithWidth(width: CGFloat) {
        
//        self.backgroundColor = UIColor.whiteColor()
        self.layer.cornerRadius = width/2
        self.layer.masksToBounds = false
        self.contentMode = UIViewContentMode.ScaleAspectFill    
        self.clipsToBounds = true
        
        self.autoSetDimensionsToSize(CGSizeMake(width, width))
    }
}
extension UITextField {

    func transparentTextField() {
        
        self.autocorrectionType = UITextAutocorrectionType.No
        self.backgroundColor = UIColor.clearColor()
        self.textColor = UIColor.whiteColor()
    }
    
    func mailTypeTextField() {
        
        self.keyboardType = UIKeyboardType.EmailAddress
        self.autocapitalizationType = UITextAutocapitalizationType.None
    }
    
    func passwordTypeTextField() {

        self.clearButtonMode = UITextFieldViewMode.Never;
        self.secureTextEntry = true
    }
    
    func setLightGrayPlaceholder(placeholder: String) {

        self.attributedPlaceholder = NSAttributedString(string:placeholder,
            attributes:[NSForegroundColorAttributeName: UIColor.darkGrayColor()])
    }
}

extension UILabel {
    
    func statisticsScoresTitles() {
        self.textColor = UIColor.whiteColor()
        self.font = UIFont(name: "\(kDefaultFont)-Roman", size: 16.0)
        self.autoPinEdgeToSuperviewEdge(ALEdge.Top, withInset: 5.0)
    }
    
    func statisticsScores() {
        self.font = UIFont(name: "\(kDefaultFont)-Bold", size: 30.0)
    }
    
    func scoreTag() {
        self.textColor = UIColor.whiteColor()
        self.backgroundColor = UIColor.blackColor()
        self.textAlignment = NSTextAlignment.Center
    }
}

extension Character {
    var integerValue:Int {
        return Int(String(self)) ?? 0
    }
}

extension String {
    
    func suffixForNumber(number: Int) -> String {
    
        switch number {
        case 1:
            return "\(number)st"
        case 2:
            return "\(number)nd"
        case 3:
            return "\(number)rd"
        default:
            return "\(number)th"
        }
    }
}