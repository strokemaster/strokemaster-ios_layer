//
//  StatisticsGraphCell.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/27/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit
import Charts

class StatisticsGraphCell: UITableViewCell, ChartViewDelegate, UITableViewDelegate, UITableViewDataSource {

    var chartView: LineChartView!
    
    var json : JSON = nil
    var scoreValues = Array<Double>()
    let averageLabel = UILabel()
    let roundsLabel = UILabel()
    let averageDataLabel = UILabel()
    let roundsDataLabel = UILabel()
    let filterButton = UIButton()
    
    let valuesSwitch = UISwitch()
    
    let headerView = UIView()
    
    var tableView: UITableView?
    var filterOptions = Array<String>()
    
    let userdefaults = NSUserDefaults.standardUserDefaults()
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(style: UITableViewCellStyle, reuseIdentifier: String?, json:JSON) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        self.json = json
        self.scoreValues = DataManager.stringToDoubleArray(self.json["Scores"].string!)
        
        self.selectionStyle = .None
        self.backgroundColor = UIColor.clearColor()
        
        self.createTopLabels()
    
        chartView = LineChartView()
        
        chartView.delegate = self;
        
        chartView.descriptionText = "";
        chartView.noDataTextDescription = "You need to provide data for the chart.";
        

        chartView.highlightEnabled = false
        chartView.dragEnabled = true
        chartView.setScaleEnabled(true)
        chartView.pinchZoomEnabled = true
        chartView.drawGridBackgroundEnabled = false
        
        
        chartView.xAxis.enabled = true
        chartView.leftAxis.enabled = true
        chartView.rightAxis.enabled = false
        chartView.legend.enabled = true
        
        //Axis Labels
        let xAxis = chartView.xAxis;
        xAxis.labelPosition = ChartXAxis.XAxisLabelPosition.Bottom
        xAxis.labelFont = UIFont.systemFontOfSize(10.0)
        xAxis.drawGridLinesEnabled = false
        xAxis.labelTextColor = UIColor.whiteColor()
        xAxis.setLabelsToSkip(0)
        
        let leftAxis = chartView.leftAxis
        leftAxis.labelFont = UIFont.systemFontOfSize(10.0)
        leftAxis.labelTextColor = UIColor.whiteColor()
        leftAxis.startAtZeroEnabled = false
        leftAxis.customAxisMin = 40
//        leftAxis.labelCount = 8
        leftAxis.drawGridLinesEnabled = false
        leftAxis.valueFormatter = NSNumberFormatter()
        leftAxis.valueFormatter!.maximumFractionDigits = 0
//        leftAxis.valueFormatter!.negativeSuffix = " $"
//        leftAxis.valueFormatter!.positiveSuffix = " strokes"
        leftAxis.labelPosition = ChartYAxis.YAxisLabelPosition.OutsideChart;
        leftAxis.spaceTop = 0.15
        
        chartView.legend.position = ChartLegend.ChartLegendPosition.BelowChartRight
        chartView.legend.form = ChartLegend.ChartLegendForm.Circle
        chartView.legend.formSize = 10
        chartView.legend.font = UIFont(name:"\(kDefaultFont)-Light", size: 13.0)!
        chartView.legend.textColor = UIColor.whiteColor()
        chartView.legend.xEntrySpace = 10
        
        //add Graph
        self.contentView.addSubview(chartView)
        
        //Toogle values
        let valuesLabel = UILabel()
        valuesLabel.text = "Show values"
        valuesLabel.textColor = .whiteColor()
        valuesLabel.font = UIFont(name: "\(kDefaultFont)-Light", size: 13.0)
        self.contentView.addSubview(valuesLabel)
        
        valuesLabel.autoPinEdgeToSuperviewEdge(ALEdge.Left, withInset: 15.0)
        valuesLabel.autoPinEdgeToSuperviewEdge(ALEdge.Bottom, withInset: 25.0)
        
        
        valuesSwitch.onTintColor = kTabGreenColor
        valuesSwitch.addTarget(self, action: "switchChanged:", forControlEvents: UIControlEvents.ValueChanged)
        self.contentView.addSubview(valuesSwitch)
        
        valuesSwitch.autoPinEdge(ALEdge.Left, toEdge: ALEdge.Right, ofView: valuesLabel, withOffset: 10.0)
        valuesSwitch.autoAlignAxis(ALAxis.Horizontal, toSameAxisOfView: valuesLabel)

        //Set graph points
        self.setDataCount(count:12, range: 80)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        chartView.autoPinEdge(ALEdge.Top, toEdge: ALEdge.Bottom, ofView: headerView, withOffset: 5.0)
        chartView.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsMake(0, 0, 20, 0), excludingEdge: ALEdge.Top)
        self.animateChart()
    }
    
    //UI For Labels at top
    
    func createTopLabels() {
        
        //Static Labels
        averageLabel.textColor = kBrightGreenColor
        averageLabel.text = "Average"
        averageLabel.font = UIFont(name: "\(kDefaultFont)-Medium", size: 16.0)
        self.contentView.addSubview(averageLabel)
        
        averageLabel.autoPinEdgeToSuperviewEdge(ALEdge.Left, withInset: 15.0)
        averageLabel.autoPinEdgeToSuperviewEdge(ALEdge.Top, withInset: 15.0)
        
        roundsLabel.textColor = kBrightGreenColor
        roundsLabel.text = "Rounds"
        roundsLabel.font = UIFont(name: "\(kDefaultFont)-Medium", size: 16.0)
        self.contentView.addSubview(roundsLabel)
        
        roundsLabel.autoPinEdgeToSuperviewEdge(ALEdge.Right, withInset: 15.0)
        roundsLabel.autoPinEdgeToSuperviewEdge(ALEdge.Top, withInset: 15.0)
        
        //Changing Labels
        averageDataLabel.textColor = .whiteColor()
        let avg = self.json["Overall"].int
        averageDataLabel.text = (avg == nil ? "0" : String(avg!))
        
        averageDataLabel.font = UIFont(name: "\(kDefaultFont)-Bold", size: 20.0)
        self.contentView.addSubview(averageDataLabel)
        
        averageDataLabel.autoAlignAxis(ALAxis.Vertical, toSameAxisOfView: averageLabel)
        averageDataLabel.autoPinEdge(ALEdge.Top, toEdge: ALEdge.Bottom, ofView: averageLabel, withOffset: 5.0)
        
        roundsDataLabel.textColor = .whiteColor()
        roundsDataLabel.text = String(self.scoreValues.count)
        roundsDataLabel.font = UIFont(name: "\(kDefaultFont)-Bold", size: 20.0)
        self.contentView.addSubview(roundsDataLabel)
        
        roundsDataLabel.autoAlignAxis(ALAxis.Vertical, toSameAxisOfView: roundsLabel)
        roundsDataLabel.autoPinEdge(ALEdge.Top, toEdge: ALEdge.Bottom, ofView: roundsLabel, withOffset: 5.0)
        
        //Filter
        filterButton.setTitle("Last 12 \nRounds", forState: UIControlState.Normal)
        filterButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        filterButton.titleLabel?.numberOfLines = 2
        filterButton.titleLabel?.font = UIFont(name: "\(kDefaultFont)-Roman", size: 16.0)
        filterButton.titleLabel?.textAlignment = NSTextAlignment.Center
        filterButton.addTarget(self, action: "showTable", forControlEvents: UIControlEvents.TouchUpInside)
        self.contentView.addSubview(filterButton)
        
        filterButton.autoPinEdge(ALEdge.Top, toEdge: ALEdge.Top, ofView: roundsLabel)
        filterButton.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)

        
        let arrowIcon = UIImageView(image: UIImage(named: "detailarrow"))
        self.contentView.addSubview(arrowIcon)
        
        arrowIcon.autoPinEdge(ALEdge.Left, toEdge: ALEdge.Right, ofView: filterButton, withOffset: 10.0)
        arrowIcon.autoAlignAxis(ALAxis.Horizontal, toSameAxisOfView: filterButton)
        arrowIcon.autoSetDimensionsToSize(CGSizeMake(12.0, 10.0))
        
        filterOptions = ["3", "6", "12"]
        
        self.graphHeaderView()
    }
    
    //Create Graph header information (player name - Stats)
    func graphHeaderView() {
        
        headerView.transparentViewWithAlpha(0.5)
        self.contentView.addSubview(headerView)
        
        headerView.autoPinEdgeToSuperviewEdge(ALEdge.Right)
        headerView.autoPinEdgeToSuperviewEdge(ALEdge.Left)
        headerView.autoPinEdge(ALEdge.Top, toEdge: ALEdge.Bottom, ofView: averageDataLabel, withOffset: 15.0)
        headerView.autoSetDimension(ALDimension.Height, toSize: 30)
    
        let headerLabel = UILabel()
        let email = userdefaults.objectForKey("Email") as! String
        headerLabel.text = email + "'s Scores"
        headerLabel.textColor = .whiteColor()
        headerLabel.font = UIFont(name: "\(kDefaultFont)-Medium", size: 14.0)
        
        headerView.addSubview(headerLabel)
        
        headerLabel.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        headerLabel.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
    }


    //Populates the Graph Data Array (Demo values)
    func setDataCount(count count: Int, range: Float) {
    
        var xVals = Array<String>()
        
        //TO-DO Get actual month and calcuate previous 3-6-12
        
        var months = ["1", "2", "3", "4", "5", "6", "7", "8", "9",
        "10", "11", "12"]
    
        for (var i = 0; i < count; i++) {
            xVals.append(months[i])
        }
        
        //Scores values
        var yVals1 = Array<ChartDataEntry>()
        
        //Custom values for the decreacing
        
        //fix add custom data

        
        
        //scoreValues = [134, 123, 120, 115, 107, 110, 97, 90, 84, 83, 81, 78]
    
        for (var i = 0; i < scoreValues.count; i++) {
//            var mult = (range + 1)
//            var val: Float = Float((arc4random_uniform(UInt32(mult))) + 60 - UInt32(i))
            
            yVals1.append(ChartDataEntry(value: scoreValues[i] , xIndex: i))
        }
        
        let set1 = LineChartDataSet(yVals: yVals1, label: "Round Score")
        set1.drawCubicEnabled = true
        set1.drawFilledEnabled = true
        set1.cubicIntensity = 0.2
        set1.drawCirclesEnabled = true
        set1.drawValuesEnabled = valuesSwitch.on
        set1.lineWidth = 2
        set1.circleRadius = 4
        set1.circleHoleColor = kTabGreenColor
        set1.circleColors = [kTabGreenColor, kTabGreenColor]
        set1.highlightColor = UIColor.redColor()
        set1.setColor(kTabGreenColor)
        set1.fillColor = kGreenColor
        
        //Average Value
        var yVals2 = Array<ChartDataEntry>()
        
        var average: Double = 0.0
        
        for (var i = 0; i < scoreValues.count; i++) {

            average += yVals1[i].value
            let val: Double = average/Double(i + 1)
            yVals2.append(ChartDataEntry(value: val, xIndex: i))
        }
        
        let set2 = LineChartDataSet(yVals: yVals2, label: "Average Score")
        set2.drawCubicEnabled = true
        set2.drawFilledEnabled = true
        set2.cubicIntensity = 0.2
        set2.drawCirclesEnabled = false
        set2.drawValuesEnabled = false
        set2.lineWidth = 2
        set2.circleRadius = 5
        set2.highlightColor = UIColor.redColor()
        set2.setColor(UIColor.yellowColor())
        set2.fillColor = UIColor.clearColor()
        
    
        
        var dataSets = Array<LineChartDataSet>()
        dataSets.append(set2)
        dataSets.append(set1)
        
        
        let data = LineChartData(xVals: xVals, dataSets: dataSets)
        data.setValueFont(UIFont(name: "\(kDefaultFont)-Light", size: 10.0))
        data.setValueTextColor(UIColor.whiteColor())
        let format = NSNumberFormatter()
        format.generatesDecimalNumbers = false
        data.setValueFormatter(format)
        
        chartView.data = data;
    }

    func animateChart() {
        chartView.animate(yAxisDuration: 1.0)
    }
    
    //MARK: - ChartViewDelegate
    
    func chartValueSelected(chartView: ChartViewBase, entry: ChartDataEntry, dataSetIndex: Int, highlight: ChartHighlight) {
        print("chartValueSelected", terminator: "")
    }
    
    func chartValueNothingSelected(chartView: ChartViewBase) {
        print("Nothing Selected", terminator: "")
    }
    
    //MARK: - FilterButton
    func showTable() {
        
        if tableView == nil {
            tableView = UITableView(frame: CGRectMake(filterButton.frame.origin.x + filterButton.frame.size.width + 20, 0, 60, 0))
            tableView?.dataSource = self
            tableView?.delegate = self
            tableView?.backgroundColor = UIColor.clearColor()
            tableView?.bounces = false
            tableView?.rowHeight = 25
            tableView?.separatorColor = .clearColor()
            self.contentView.addSubview(tableView!)
            
            UIView.animateWithDuration(0.33, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
                    self.tableView?.frame.size = CGSizeMake(60, 75)
                }, completion: nil)
        } else {
            
            UIView.animateWithDuration(0.33, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
                self.tableView?.frame.size = CGSizeMake(60, 0)
                }, completion: { (finished) -> Void in
                    
                    self.tableView?.removeFromSuperview()
                    self.tableView = nil
            })
        }
    }
    
    //MARK: - TableView 
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterOptions.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("cell") as UITableViewCell?
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        }
        
        cell?.backgroundColor = UIColor.clearColor()
        cell?.selectionStyle = .None
        cell?.textLabel?.textColor = UIColor.whiteColor()
        cell?.textLabel?.text = filterOptions[indexPath.row] as String
        cell?.textLabel?.textAlignment = NSTextAlignment.Center
        cell?.textLabel?.font = UIFont(name: "\(kDefaultFont)-Bold", size: 18)
        
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
     
        //FIX FILTER CODE
        
        if indexPath.row == 0 {
            self.setDataCount(count: 3, range: 80)
            self.filterButton.setTitle("Last 3\nRounds", forState: UIControlState.Normal)
        } else if indexPath.row == 1 {
            self.setDataCount(count: 6, range: 100)
            self.filterButton.setTitle("Last 6\nRounds", forState: UIControlState.Normal)
        } else {
            self.setDataCount(count: 12, range: 120)
            self.filterButton.setTitle("Last 12\nRounds", forState: UIControlState.Normal)
        }
        
        self.showTable()
        self.animateChart()
    }
    
    
    //MARK: - Switch Functions
    //Show/Hide values on graphh
    func switchChanged(sender: UISwitch) {
        
        for set in chartView.data!.dataSets {
            if set.label != "Average Score" {
                set.drawValuesEnabled = !set.isDrawValuesEnabled
            }
        }
    
        chartView.setNeedsDisplay()
        
    }
}
