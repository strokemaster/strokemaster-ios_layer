//
//  Constants.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/4/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import Foundation
import UIKit

//Sizes
let SCREEN_HEIGHT: CGFloat = UIScreen.mainScreen().bounds.size.height
let SCREEN_WIDTH: CGFloat = UIScreen.mainScreen().bounds.size.width

//Font
let kDefaultFont: String = "HelveticaNeue"

//Colors
let kBlackTransparentColor: UIColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6)
let kGreenColor: UIColor = UIColor(red: 42/255, green: 59/255, blue: 37/255, alpha: 0.9)
let kTabGreenColor: UIColor = UIColor(red: 29/255, green: 83/255, blue: 9/255, alpha: 1.0)
let kSettingsGreenColor: UIColor = UIColor(red: 0/255, green: 91/255, blue: 45/255, alpha: 1.0)
let kLightGreenColor: UIColor = UIColor(red: 24.0/255.0, green: 210.0/255.0, blue: 27.0/255.0, alpha: 0.2)
let kscoreGreenColor: UIColor = UIColor(red: 0.0/255.0, green: 234.0/255.0, blue: 0.0/255.0, alpha: 1.0)
let kscoreBlueColor: UIColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 255.0/255.0, alpha: 1.0)
let kscoreYellowColor: UIColor = UIColor(red: 241.0/255.0, green: 196.0/255.0, blue: 15.0/255.0, alpha: 1.0)
let kscoreOrangeColor: UIColor = UIColor(red: 230.0/255.0, green: 126.0/255.0, blue: 34.0/255.0, alpha: 1.0)
let kscoreRedColor: UIColor = UIColor(red: 231.0/255.0, green: 76.0/255.0, blue: 60.0/255.0, alpha: 1.0)
let kBrightGreenColor: UIColor = UIColor(red: 157.0/255.0, green: 253.0/255.0, blue: 50.0/255.0, alpha: 1.0)
let kRoyalBlue: UIColor = UIColor(red: 65.0/255.0, green: 105.0/255.0, blue: 225.0/255.0, alpha: 0.7)
let kTransparentBlue: UIColor = UIColor(red: 9.0/255.0, green: 25.0/255.0, blue: 43.0/255.0, alpha: 0.6)
let kTransparentRed: UIColor = UIColor(red: 70.0/255.0, green: 15.0/255.0, blue: 9.0/255.0, alpha: 0.6)
let kSolidBlue: UIColor = UIColor(red: 9.0/255.0, green: 25.0/255.0, blue: 43.0/255.0, alpha: 1.0)
let kOrangeColor: UIColor = UIColor(red: 250.0/255.0, green: 140.0/255.0, blue: 50.0/255.0, alpha: 0.4)


