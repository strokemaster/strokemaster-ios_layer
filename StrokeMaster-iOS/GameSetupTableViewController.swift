//
//  GameSetupTableViewController.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/8/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class GameSetupTableViewController: UITableViewController {
    
    var itemArray: NSArray!
    var selectedSection: Int?
    var course: CourseListCourse!
    var userchoices: HoleCountTableViewCell!
    var userchoices2: ClubTableViewCell!
    var userchoices3: ClubTableViewCell!
    var userchoices4: ClubTableViewCell!
    var numHoles: Int!
    var startHole: Int!
    var teeColor: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Game Setup"
        self.view.backgroundColor = UIColor.clearColor()
        self.tableView.separatorColor = UIColor.clearColor()
        self.tableView.tableFooterView = UIView()
        self.tableView.rowHeight = 60.0
        
        let backButton = UIBarButtonItem(image: UIImage(named: "backarrow"), style: UIBarButtonItemStyle.Plain, target: self, action: "backPressed")
        self.navigationItem.leftBarButtonItem = backButton
        
        if let path = NSBundle.mainBundle().pathForResource("GameSetUp", ofType: "plist") {
            itemArray = NSArray(contentsOfFile: path)!
        }
        
        self.createStartButton()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.evo_drawerController?.openDrawerGestureModeMask = []
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 4
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let index = selectedSection {
            if index == section {
                return index == 3 ? 5 : 3
            }
        }
        return 1
    }

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return indexPath.section >= 2 ? 44.0 : 60.0
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            
            var cell = tableView.dequeueReusableCellWithIdentifier("cell") as? GameSetupCourseCell
            if cell == nil {
                cell = GameSetupCourseCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell", course: self.course!)
            }
            
            if let scoreCard = self.course.scoreCardInfo {
                var holes = ""
                if(scoreCard.mensParTotal < 60){
                holes = "9"
                } else {
                    holes = "18"
                }
                cell?.detailLabel.text = holes + " Holes, Par " + String(scoreCard.mensParTotal)
            }
            return cell!
            
        } else if indexPath.section == 1 {
            userchoices = tableView.dequeueReusableCellWithIdentifier("holesCountCell") as? HoleCountTableViewCell
            if userchoices == nil {
                userchoices = HoleCountTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
            }
            
            return userchoices!
        
        } else if indexPath.row == 0 {
            
            if (indexPath.section == 2){
                if userchoices2 == nil {
                    userchoices2 = ClubTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
                    userchoices2?.gameStyle(itemArray[indexPath.section - 2][1] as! String)
                }
                
                userchoices2?.label.text = itemArray[indexPath.section - 2][0] as? String
                
                if indexPath.section == 3 {
                    userchoices2?.optionLabel.textColor = UIColor.whiteColor()
                }
                
                return userchoices2!
            }
            else{
                userchoices3 = tableView.dequeueReusableCellWithIdentifier("cell") as? ClubTableViewCell
                if userchoices3 == nil {
                    userchoices3 = ClubTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
                    userchoices3?.gameStyle(itemArray[indexPath.section - 2][1] as! String)
                }
            
                userchoices3?.label.text = itemArray[indexPath.section - 2][0] as? String
                
                if indexPath.section == 3 {
                    userchoices3?.optionLabel.textColor = UIColor.whiteColor()
                }
            
                return userchoices3!
            }
        } else {
            
            userchoices4 = tableView.dequeueReusableCellWithIdentifier("cell") as? ClubTableViewCell
            if userchoices4 == nil {
                userchoices4 = ClubTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
                userchoices4?.blackView.backgroundColor = UIColor(red: 0.05, green: 0.05, blue: 0.05, alpha: 0.6)
                userchoices4?.gameOption()
            }
            
            userchoices4?.label.text = itemArray[indexPath.section - 2][indexPath.row] as? String
           
            if indexPath.section == 3 {
                userchoices4?.label.textColor = colorForLabel(indexPath.row)
            } else {
                userchoices4?.label.textColor = .whiteColor()
            }
            
            return userchoices4!
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.section > 1 {
        
            let cell = tableView.cellForRowAtIndexPath(indexPath) as! ClubTableViewCell
            
            if indexPath.row == 0 {
            
                if selectedSection == indexPath.section {
                
                    cell.restoreArrow()
                    selectedSection = nil
                    self.tableView.beginUpdates()
                    self.compressExpandedSection(indexPath.section)
                    self.tableView.endUpdates()
                    return
                }
            
                self.tableView.beginUpdates()
                if let previousSelected = selectedSection {
                    let previousCell = tableView.cellForRowAtIndexPath(indexPath) as! ClubTableViewCell
                    previousCell.restoreArrow()
                    self.compressExpandedSection(previousSelected)
                }
            
                selectedSection = indexPath.section
                self.showExpandedSection()
                self.tableView.endUpdates()
                cell.rotateArrow()
            //else may not be needed at all
            } else {
                let sectionCell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: indexPath.section)) as! ClubTableViewCell
                sectionCell.optionLabel.text = cell.label.text
                if indexPath.section == 3 {
                    sectionCell.optionLabel.textColor = colorForLabel(indexPath.row)
                }
                cell.restoreArrow()
                selectedSection = nil
                self.tableView.beginUpdates()
                self.compressExpandedSection(indexPath.section)
                self.tableView.endUpdates()
            }
        }
    }
    
    func showExpandedSection() {
        
        var indexPathArray = [NSIndexPath]()
        
        let cellCount = selectedSection == 3 ? 4 : 2
        
        for index in 1...cellCount {
            let indexPath = NSIndexPath(forRow: index, inSection: selectedSection!)
            indexPathArray.append(indexPath)
        }
        
        self.tableView.insertRowsAtIndexPaths(indexPathArray, withRowAnimation: UITableViewRowAnimation.Bottom)
    }
    
    func compressExpandedSection(sectionToCompress: Int) {
        
        var indexPathArray = [NSIndexPath]()
        
        let cellCount = sectionToCompress == 3 ? 4 : 2
        
        for index in 1...cellCount {
            let indexPath = NSIndexPath(forRow: index, inSection: sectionToCompress)
            indexPathArray.append(indexPath)
        }
        self.tableView.deleteRowsAtIndexPaths(indexPathArray, withRowAnimation: UITableViewRowAnimation.Top)
    }
    
    //Color for tee label
    func colorForLabel(index: NSInteger) -> UIColor {
        if index == 1{
            return .whiteColor()
        }
        else if index == 2 {
            return .blueColor()
        } else {
            return index == 3 ? .redColor() : .lightGrayColor()
        }
    }
    
    //Start Button
    func createStartButton() {
        
        let startButton = UIButton(frame: CGRectMake(0, self.view.bounds.size.height - 153.0, SCREEN_WIDTH, 60.0))
        startButton.setTitle("START COURSE", forState: UIControlState.Normal)
        startButton.backgroundColor = kLightGreenColor
        startButton.addTarget(self, action: "startPressed", forControlEvents: UIControlEvents.TouchUpInside)
        
        if self.course.scoreCardInfo == nil {
            startButton.userInteractionEnabled = false
            startButton.backgroundColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.3)
        }
        
        self.tableView.addSubview(startButton)
    }

    func startPressed() {
        
        if(self.userchoices.nineHolesButton.selected){
            numHoles = 9
        }
        else if(self.userchoices.allHolesButton.selected){
            numHoles = 18
        }
        
        if(userchoices2.optionLabel.text! == "1st Hole"){
            startHole = 1
        }
        else{
            startHole = 10
        }

        //fix this shit
        teeColor = userchoices3.optionLabel.text!
        
        self.tabBarController?.tabBar.hidden = true
        self.hidesBottomBarWhenPushed = true
        let loadingViewController = LoadingViewController()
        loadingViewController.course = self.course
        loadingViewController.startHole = startHole
        loadingViewController.numHoles = numHoles
        loadingViewController.teeColor = teeColor
        self.navigationController?.customPush(loadingViewController)
    }
    
    //Back button custom pop
    func backPressed() {
        self.navigationController?.customPop()
    }
}
