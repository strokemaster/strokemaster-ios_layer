//
//  ClubsTableViewController.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/16/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

protocol ClubsTableViewControllerDelegate {
    func clubSelected(club: SMClub)
}

class ClubsTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var userdefaults = NSUserDefaults.standardUserDefaults()
    var tableView: UITableView!
    //var clubArray: NSArray!
    var clubArray: NSArray!
    var headersArray: NSArray!
    var clubsDelegate: ClubsTableViewControllerDelegate!
    var headerText = "SELECT CLUB TYPE"
    
    var clubList = [SMClub]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.clubArray = userdefaults.objectForKey("Clubs") as! NSArray
        //Load everyClub
        /*if let path = NSBundle.mainBundle().pathForResource("ClubsList", ofType: "plist") {
            clubArray = NSArray(contentsOfFile: path)!
        }*/
        
        
        for var i = 0; i <  clubArray.count; i++ {
            
            let myclub = SMClub(type: Club(rawValue: clubType(clubArray[i] as! String)), number: nil, name: clubArray[i] as? String, clubNumber: i)
            clubList.append(myclub)
        }

        
        /*for (index, club) in enumerate(clubList) {
            club.clubNumber = index + 1
        }*/
        
        
        headersArray = ["Drivers", "Woods", "Hybrids", "Irons", "Wedges", "Putters"]
        
        let headerView = self.createHeaderView()
        self.view.addSubview(headerView)
        headerView.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsMake(0, 0, 0, 0), excludingEdge: ALEdge.Bottom)
        headerView.autoSetDimension(ALDimension.Height, toSize: 64.0)
        
        self.view.backgroundColor = UIColor.clearColor()
        
        tableView = UITableView(frame: CGRectMake(0, 64, self.view.bounds.size.width, self.view.bounds.size.height - 64), style: UITableViewStyle.Plain)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = UIColor.clearColor()
        tableView.tableFooterView = UIView()
        self.view.addSubview(tableView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return clubArray[section].count
        return clubList.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("clubCell") as UITableViewCell?
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "clubCell")
            cell?.selectionStyle = UITableViewCellSelectionStyle.None
        }
        
//        let string = clubArray[indexPath.section][indexPath.row] as! String
        
        let club = clubList[indexPath.row]

        let string = club.name
        
        /*if let number = club.number {
            string = "\(number) \(string)"
        } else if let name = club.name {
            string = "\(name) \(string)"
        }
        
        if string == "Driver Wood" {
            string = "Driver"
        }*/
        
        cell?.textLabel?.text = string!.uppercaseString
        
        customizeCell(cell!)
        return cell!
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
//        var str: String = clubArray[indexPath.section][indexPath.row] as! String
//        var unichar = str[str.startIndex]
//        var unicharString = "\(unichar)"
//        
//        let clubEnum = Club(rawValue: indexPath.section)
//        let club = SMClub(type: clubEnum, number: unicharString.toInt(), name: nil)
        
        self.clubsDelegate.clubSelected(clubList[indexPath.row])
    }
    
    //No header
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }

    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView(frame: CGRectMake(0, 0, self.view.bounds.size.width, 30))
        view.transparentViewWithAlpha(0.9)
        let label = UILabel()
        label.textColor = UIColor.whiteColor()
        label.text = headersArray[section] as? String
        label.font = UIFont(name: "\(kDefaultFont)-Roman", size: 14.0)
        view.addSubview(label)
        
        label.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsMake(0, 15, 0, 0), excludingEdge: ALEdge.Right)
//        return view
        return nil
    }
    
    func createHeaderView() -> UIView {
    
        let headerView = UIView(frame: CGRectMake(0, 0, SCREEN_WIDTH, 64))
        headerView.backgroundColor = UIColor.blackColor()
        
        let headerLabel = UILabel()
        headerLabel.textColor = UIColor.whiteColor()
        headerLabel.font = UIFont(name: "\(kDefaultFont)-Light", size: 22.0)
        headerLabel.text = headerText
        headerView.addSubview(headerLabel)
        
        headerLabel.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        headerLabel.autoPinEdgeToSuperviewEdge(ALEdge.Bottom, withInset: 10.0)

        return headerView
    }
    
    func customizeCell(cell: UITableViewCell) {
        
        cell.backgroundColor = UIColor.clearColor()
        cell.textLabel?.font = UIFont(name: "\(kDefaultFont)-Light", size: 14.0)
        cell.textLabel?.textColor = UIColor.whiteColor()
    }
    
    func clubType(name:String) -> Int{
        if name.rangeOfString("Wood") != nil  || name.rangeOfString("Driver") != nil {
            return 0
        }
        else if name.rangeOfString("Hybrid") != nil {
            return 1
        }
        else if name.rangeOfString("Iron") != nil {
            return 2
        }
        else {
            return 3
        }
    }
}
