//
//  ProfileTableViewController.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/12/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class ProfileTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Profile"

        self.tableView.rowHeight = 115.0
        self.view.backgroundColor = UIColor.clearColor()
        
        self.tableView.tableFooterView = UIView(frame: CGRectMake(0, 0, SCREEN_WIDTH, 100))
        
        let mybagButton = UIButton()
        mybagButton.transparentButton("My Bag")
        mybagButton.backgroundColor = kLightGreenColor
        mybagButton.addTarget(self, action: "myBagPressed", forControlEvents: UIControlEvents.TouchUpInside)
        self.tableView.tableFooterView?.addSubview(mybagButton)
        mybagButton.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        mybagButton.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        mybagButton.autoSetDimensionsToSize(CGSizeMake(220.0, 40.0))

        //Create MenuButton
        let leftDrawerButton = UIBarButtonItem(image: UIImage(named: "menuButton"), style: UIBarButtonItemStyle.Plain, target: self, action: "menuPressed")
        self.navigationItem.setLeftBarButtonItem(leftDrawerButton, animated: false)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("profileCell") as? ProfileTableViewCell
        if cell == nil {
            cell = ProfileTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "profileCell")
        }

        return cell!
    }

    func menuPressed() {
        self.evo_drawerController?.toggleDrawerSide(.Left, animated: true, completion: nil)
    }

    //My Bag Pressed
    func myBagPressed() {
        self.navigationController?.customPush(BagTableViewController())
    }
}
