//
//  GpsViewController.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/16/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit
import CoreLocation

class GpsViewController: UIViewController {
    
    var hole: SMHole!
    let gpsView = GpsGameView()
    var greenCenterDistance: Int!
    var courseInfo: JSON!
    //Green Distances
    var greenCenter: CLLocation!
    var greenStart: CLLocation!
    var greenEnd: CLLocation!
    
    //Parent
    var containerViewController: CategoriesContainerViewController!
    
    override func loadView() {
        super.loadView()
        gpsView.frame = self.view.bounds
        self.view = gpsView
        
        //Distance
//        gpsView.greenCenter.informationLabel.text = "\(greenCenterDistance) yards"
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Define containerViewController
        containerViewController = self.parentViewController!.parentViewController!.parentViewController as! CategoriesContainerViewController
        
        //SetUp Top boxes info with hole
        gpsView.fillInformationWithHole(hole)
        
        //Set up bottom information 
//        gpsView.setUpBottomInformation(hole)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.gpsView.courseImage.drawUserLocation(containerViewController.getLocation())
        
        //FIX ME
        let string = ""
        let newString = string.suffixForNumber(hole.number)
        containerViewController.changeNavigationTitle("\(newString) Hole")
    }
    
    //Update new hole
    func updateHole(hole: SMHole, json: JSON) {
        gpsView.fillInformationWithHole(hole)
        
        //Send new JSON for hole and redraw
        gpsView.courseImage.json = json
        gpsView.courseImage.setNeedsDisplay()
        self.hole = hole
    }
    
    //Update golfer position and distances (bottom middle and hole)
    func updateLocation(actualLocation: CLLocation) {
        
        gpsView.calculateGreenDistances(actualLocation)
    }
}
