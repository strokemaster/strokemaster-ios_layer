//
//  RegisterFormViewController.swift
//  StrokeMaster-iOS
//
//  Created by Greg Morano on 5/18/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class RegisterFormViewController: GenericFormTableViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerView.title.text = "Register for Strokemaster"
        
        footerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 200)
        footerView.button.setTitle("FINISH", forState: UIControlState.Normal)
        footerView.centeredGoBackButton()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func fillDataArray() {
        if let path = NSBundle.mainBundle().pathForResource("RegisterFormList", ofType: "plist") {
            formDataArray = NSArray(contentsOfFile: path)!
        }
    }
    
    override func setTextFieldType(index: NSInteger, cell: FormTableViewCell) {
        if(index == 0){
            cell.textField.mailTypeTextField()
        }
        else{
            cell.textField.passwordTypeTextField()
        }
    }
    
    //Login button pressed.  Validate email and password entered.  If validated create user.
    override func loginPressed() {
        
        print("Creating new user")
        var email = ""
        var pass = ""
        var pass2 = ""
        //var datastring: NSString = ""
        
        //check that passwords match
        var cell =  tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0)) as! FormTableViewCell
        email = cell.textField.text!
        cell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 1, inSection: 0)) as! FormTableViewCell
        pass = cell.textField.text!
        cell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 2, inSection: 0)) as! FormTableViewCell
        pass2 = cell.textField.text!
        

        
        //validate that email form is correct
        if(self.isValidEmail(email)){
            //validate passwords are valid and match if validate create a new user call
            if(self.isValidPasswords(pass, pass2: pass2)){
                let semaphore = dispatch_semaphore_create(0)
                
                var dictionary = Dictionary<String, AnyObject>()
                var dictionary2 = Dictionary<String, AnyObject>()
                dictionary2.updateValue(email as String, forKey: "Email")
                dictionary2.updateValue(pass, forKey: "Password")
                dictionary.updateValue(dictionary2, forKey: "Data")
                let json: JSON =  [["Email": email as String, "Password": pass]]
                var json2: [JSON] = []
                json2.append(JSON(["Email": email as String, "Password": pass]))
                json2.append(JSON(["Email": pass as String, "Password": email as String]))
                    
               
                var res: Int = -1
                

                
                DataManager.sendCreateUser(json, success:{(loginResult) -> Void in
                    res = loginResult
                    dispatch_semaphore_signal(semaphore)
                })
                /*DataManager.createUser(email as String, password: pass, success:{(loginResult) -> Void in
                    let result = loginResult
                    datastring = NSString(data: result, encoding: NSUTF8StringEncoding)!
                    dispatch_semaphore_signal(semaphore)
                })*/
                
                dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)
                //println(datastring)
                //new user created
                //if(datastring == "1"){
                if(res == 1){
                    
                    let userdefaults = NSUserDefaults.standardUserDefaults()
                    userdefaults.setObject(email, forKey: "Email")
                    userdefaults.synchronize()
                    
                    self.pushViewController()
                }
                //user already exists
                else{
                    if(!self.updateErrorLabel("User Already Exists")){
                        headerView.addError("User Already Exists")
                    }
                }
            }
        }
        else{
            if(!self.updateErrorLabel("Invalid Email Address")){
                headerView.addError("Invalid Email Address")
            }
        }
    }
    
    override func pushViewController() {
        print("New user created")
        self.navigationController?.customPush(RegisterFirstStepViewController())
        //super.pushViewController()
    }
    
    
    func isValidPasswords(pass : String, pass2 : String) -> Bool {
        
        if(pass == pass2){
            if(pass.characters.count >= 8){
                return true
            }
            else{
                if(!self.updateErrorLabel("Passwords Must Be At Least 8 Characters")){
                    headerView.addError("Passwords Must At Least 8 Characters")
                }
                return false
            }
        }
        else{
            if(!self.updateErrorLabel("Passwords Must Match")){
                headerView.addError("Passwords Must Match")
            }
            return false
        }
    }
    
    func updateErrorLabel(text :String) -> Bool {
        if(self.headerView.subviews.count  > 1){
            let label: UILabel = self.headerView.subviews[1] as! UILabel
            label.text = text
            return true
        }
        return false
    }
    

}
