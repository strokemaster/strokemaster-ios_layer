//
//  SettingsTableViewController.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/13/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController, SettingsDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.clearColor()
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.clearColor()
        self.tableView.rowHeight = 60.0
        
        self.navigationItem.title = "Settings"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("settingsCell") as? SettingsTableViewCell
        if cell == nil {
            cell = SettingsTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "settingsCell")
        }
        
        cell?.icon.image = UIImage(named: "backgroundicon")
        cell?.label.text = "Not available in Beta"
        cell?.delegate = self
        
        return cell!
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRectMake(0, 0, SCREEN_WIDTH, 35.0))
        headerView.backgroundColor = UIColor.clearColor()
        
        let label = UILabel()
        label.textColor = UIColor.whiteColor()
        label.font = UIFont(name: "\(kDefaultFont)-Roman", size: 14.0)
        label.text = "SETTINGS"
        headerView.addSubview(label)
        
        label.autoPinEdgeToSuperviewEdge(ALEdge.Left, withInset: 15.0)
        label.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        
        let greenLine = UIView()
        greenLine.backgroundColor = kSettingsGreenColor
        headerView.addSubview(greenLine)
        
        greenLine.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsMake(0, 15, 0, 0), excludingEdge: ALEdge.Top)
        greenLine.autoSetDimension(ALDimension.Height, toSize: 1.0)
        
        return headerView
    }
    
    func switchBackground(status: Bool) {
        self.tabBarController?.view.backgroundColor = UIColor(patternImage: UIImage(named: status == true ? "backgroundblack" : "background")!)
    }
}
