//
//  ClubTableViewCell.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/16/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class ClubTableViewCell: GenericIconLabelTableViewCell {

    
    var selectedLabel: UILabel!
    let tickImageView = UIImageView(image: UIImage(named: "tick"))
    let optionLabel = UILabel()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        label.textColor = UIColor.whiteColor()
        iconImageView.image = UIImage(named: "detailarrow")
        
    }
    
    func sectionStyle() {
        
        label.font = UIFont(name: "\(kDefaultFont)-Roman", size: 14.0)
        
        iconImageView.autoPinEdgeToSuperviewEdge(ALEdge.Left, withInset: 25.0)
        iconImageView.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        
        label.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        label.autoPinEdge(ALEdge.Left, toEdge: ALEdge.Right, ofView: iconImageView, withOffset: 15.0)
        
        
        selectedLabel = UILabel()
        selectedLabel.textColor = UIColor.lightGrayColor()
        selectedLabel.text = "none selected"
        selectedLabel.font = UIFont(name: "\(kDefaultFont)-Light", size: 12.0)
        self.contentView.addSubview(selectedLabel)
        
        selectedLabel.autoPinEdgeToSuperviewEdge(ALEdge.Right, withInset: 15.0)
        selectedLabel.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
    }
    
    //GameSetUp
    func gameStyle(string: String) {
        
        label.font = UIFont(name: "\(kDefaultFont)-Light", size: 17.0)
        
        iconImageView.autoPinEdgeToSuperviewEdge(ALEdge.Left, withInset: 25.0)
        iconImageView.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        
        label.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        label.autoPinEdge(ALEdge.Left, toEdge: ALEdge.Right, ofView: iconImageView, withOffset: 15.0)
        
        let chevron = UIImageView(image: UIImage(named: "chevron")!)
        blackView.addSubview(chevron)
        
        chevron.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        chevron.autoPinEdgeToSuperviewEdge(ALEdge.Right, withInset: 15.0)
        
        optionLabel.text = string
        optionLabel.font = UIFont(name: "\(kDefaultFont)-Medium", size: 13.0)
        optionLabel.textColor = UIColor.whiteColor()
        self.contentView.addSubview(optionLabel)
        
        optionLabel.autoPinEdge(ALEdge.Right, toEdge: ALEdge.Left, ofView: chevron, withOffset: -10.0)
        optionLabel.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
    }
    
    func gameOption() {
    
        label.font = UIFont(name: "\(kDefaultFont)-Roman", size: 13.0)
        
        label.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        label.autoPinEdgeToSuperviewEdge(ALEdge.Right, withInset: 15.0)
    }
    
    func rowStyle() {
        
        label.font = UIFont(name: "\(kDefaultFont)-Roman", size: 13.0)
        
        label.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        label.autoPinEdgeToSuperviewEdge(ALEdge.Left, withInset: 65.0)
    }
    
    func addTick() {
    
        if tickImageView.superview == nil {
            self.contentView.addSubview(tickImageView)
            
            tickImageView.autoPinEdgeToSuperviewEdge(ALEdge.Right, withInset: 15.0)
            tickImageView.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        }
    }
    
    func removeTick() {
        if tickImageView.superview != nil {
            tickImageView.removeFromSuperview()
        }
    }

    func rotateArrow() {
        
            UIView.animateWithDuration(0.33, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
                var transform = CGAffineTransformMakeTranslation(0, 0)
                transform = CGAffineTransformRotate(transform, CGFloat(M_PI_2))
                transform = CGAffineTransformTranslate(transform, 0, 0)
            
                self.iconImageView.transform = transform
                }, completion: nil) 
    }
    
    func restoreArrow() {
        
            UIView.animateWithDuration(0.33, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
                var transform = CGAffineTransformMakeTranslation(0, 0)
                transform = CGAffineTransformRotate(transform, -CGFloat(M_PI_4)/25)
                transform = CGAffineTransformTranslate(transform, 0, 0)
            
                self.iconImageView.transform = transform
                }, completion: nil)
    }
}
