//
//  ViewController.swift
//  StrokeMaster-iOS
//
//  Created by TR Staake on 12/7/14.
//  Copyright (c) 2014 StrokeMaster. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController, WelcomeViewDelegate {

    @IBAction func signUpByEmailPressed(sender: AnyObject) {
        print("Signup with Email Pressed", terminator: "")
    }
    
    @IBAction func signUpWithFacebookPressed(sender: AnyObject) {
        print("Signup with Facebook Pressed", terminator: "")
        //let vc : AnyObject! = self.storyboard?.instantiateViewControllerWithIdentifier("newRoundStoryboard")
        //self.showViewController(vc as UIViewController, sender: vc)
        //http://stackoverflow.com/questions/24336581/programmatically-switching-views-swift
    }
    
    //Replace the default view a ViewController has with our WelcomeView
    override func loadView() {
        super.loadView()
        let welcomeView = WelcomeView()
        welcomeView.delegate = self
        self.view = welcomeView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Create Account View did load", terminator: "")
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func buttonPressed(index: NSInteger) {
        
        //self.navigationController?.customPush(index == 0 ? LoginTableViewController() : RegisterFirstStepViewController())
        self.navigationController?.customPush(index == 0 ? LoginTableViewController() : RegisterFormViewController())
        //uncomment to test vector view and comment out the above statement.  Register button will trigger it
        //self.navigationController?.customPush(index == 0 ? LoginTableViewController() : VectorViewController())
    }
}

