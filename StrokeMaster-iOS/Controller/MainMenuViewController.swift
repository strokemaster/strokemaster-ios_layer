//
//  MainMenuViewController.swift
//  StrokeMaster-iOS
//
//  Created by TR Staake on 12/7/14.
//  Copyright (c) 2014 StrokeMaster. All rights reserved.
//

import UIKit

class MainMenuViewController: UIViewController {

    //Takes you to the new round setup screen
    @IBAction func newRoundButtonPressed(sender: AnyObject) {
        println("New round button pressed") //Debug
    }
    //Takes you to the statistics screen
    @IBAction func statisticsButtonPressed(sender: AnyObject) {
        println("Statistics button pressed from main menu") //Debug
    }
    //Takes you to the settings screen
    @IBAction func settingsButtonPressed(sender: AnyObject) {
        println("Settings button pressed from main menu") //Debug
    }
    //Takes you to the user profile page
    @IBAction func profileButtonPressed(sender: AnyObject) {
        println("Profile button pressed from main menu") //Debug
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        println("Main Menu Did Load")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

