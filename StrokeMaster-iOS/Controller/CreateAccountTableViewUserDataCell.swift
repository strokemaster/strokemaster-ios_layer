//
//  CreateAccountTableViewCell.swift
//  StrokeMaster-iOS
//
//  Created by TR Staake on 12/22/14.
//  Copyright (c) 2014 StrokeMaster. All rights reserved.
//

import UIKit

class CreateAccountTableViewUserDataCell: UITableViewCell {

    @IBOutlet var userDataLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
