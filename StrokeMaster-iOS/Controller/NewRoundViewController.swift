//
//  NewRoundViewController.swift
//  StrokeMaster-iOS
//
//  Created by TR Staake on 12/7/14.
//  Copyright (c) 2014 StrokeMaster. All rights reserved.
//

import UIKit


class NewRoundViewController: UIViewController, UISearchBarDelegate {
    //Starts the new round with selected parameters
    @IBOutlet var courseSearchBar: UISearchBar!
    
    @IBAction func startRoundButtonPressed(sender: AnyObject) {
        println("Start round button pressed") //Debug
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        println("New Round Did Load")
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
