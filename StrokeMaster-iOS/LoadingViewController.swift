//
//  LoadingViewController.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/14/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class LoadingViewController: UIViewController {
    
    var course: CourseListCourse!
    var startHole: Int!
    var numHoles: Int!
    var teeColor: String!
    
    override func loadView() {
        super.loadView()
        let loadingView = LoadingView(frame: self.view.bounds)
        self.view = loadingView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Game Loading"
        self.navigationItem.setHidesBackButton(true, animated:true)
        getCourseTeeInfo()
        getCourseRoundInfo()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        //Timer triggers the selector after the interval send (3.0)
//        let timer = NSTimer.scheduledTimerWithTimeInterval(3.0, target: self, selector:  Selector("endLoading"), userInfo: nil, repeats: false)
    }
    
    func endLoading() {
//        self.navigationController?.customPush(CategoriesContainerViewController())
    }
    
    func getCourseRoundInfo() {
        DataManager.getVectorDataForCourseWithID(self.course.courseId, success: { (iGolfData) -> Void in
            
            //Main Queue
            dispatch_async(dispatch_get_main_queue(),{
                let categoriesViewController: CategoriesContainerViewController = CategoriesContainerViewController(nibName: nil, bundle: nil, json: JSON(data:iGolfData), course: self.course, numHoles: self.numHoles)
                categoriesViewController.startHole = self.startHole
                self.navigationController?.customPush(categoriesViewController)
            })
        })
    }
    
    //get course tee info
    func getCourseTeeInfo() {
        let semaphore = dispatch_semaphore_create(0)
        DataManager.getCourseTeeDetails(self.course.courseId, success: { (iGolfData) -> Void in
            let result = iGolfData
            let json = JSON(data: result)
    
            //self.course.teeDetails = [CourseTeeDetails]()
            for(var i = 0; i < json.count; i++){
                
                //Add course Card to each courseObject
                let courseTee: CourseTeeDetails = CourseTeeDetails(json: json[i])
                self.course.teeDetails = courseTee
                if(courseTee.teeColorName == self.teeColor){
                    break
                }
            }
            
            dispatch_semaphore_signal(semaphore)
        })
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)
    }
}
