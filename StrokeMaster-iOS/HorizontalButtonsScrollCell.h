//
//  HorizontalButtonsScrollCell.h
//  Vorterix
//

#import <UIKit/UIKit.h>

@interface HorizontalButtonsScrollCell : UICollectionViewCell

//@property (nonatomic, retain) UIView *view;

@property (nonatomic, strong) UILabel *title;
@property (nonatomic, strong) UILabel *subtitle;
@property (nonatomic, strong) UIColor *color;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIView *fakeColorView;
@property (nonatomic, strong) UIView *colorView;

/**
 *  Returns the expected size for a cell given a title and subtitle.
 *
 *  @param title    Title.
 *  @param subtitle Subtitle.
 *
 *  @return Expected cell size.
 */
+ (CGSize)cellSizeForTitle:(NSString *)title subtitle:(NSString *)subtitle;

- (void)setTitle:(NSString *)title subtitle:(NSString *)subtitle;
- (void)deselect;
- (void)select;

@end
