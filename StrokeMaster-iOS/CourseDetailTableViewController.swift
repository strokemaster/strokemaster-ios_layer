//
//  CourseDetailTableViewController.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/8/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class CourseDetailTableViewController: UITableViewController {
    
    var itemArray: NSArray!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let path = NSBundle.mainBundle().pathForResource("CourseDetail", ofType: "plist") {
            itemArray = NSArray(contentsOfFile: path)!
        }
        

        
        self.view.backgroundColor = UIColor.clearColor()
        self.tableView.separatorColor = UIColor.clearColor()
        self.tableView.tableFooterView = UIView()
        
        let startButton = UIButton(frame: CGRectMake(0, self.view.bounds.size.height - 153.0, SCREEN_WIDTH, 60.0))
        startButton.setTitle("START COURSE", forState: UIControlState.Normal)
        startButton.backgroundColor = kBlackTransparentColor
        startButton.addTarget(self, action: "startPressed", forControlEvents: UIControlEvents.TouchUpInside)
        
        self.tableView.addSubview(startButton)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.evo_drawerController?.openDrawerGestureModeMask = Optional.None!
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 4
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch (section) {
        case 1:
            return 3
        case 3:
            return 2
        default:
            return 1
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch (indexPath.section) {
        case 0:
            return 100.0
        case 2:
            return 60.0
        default:
            return 44.0
        }
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
       
        if indexPath.section == 0 {
            var cell = tableView.dequeueReusableCellWithIdentifier("cell") as? CourseTableViewCell
            if cell == nil {
                cell = CourseTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
            }
        
            let course = CourseListCourse(id:"123abc", name: "test course", address: "123 xyz", city: "city z", state: "ZZ", courseId: "123abc", numHole: 18)
            
            cell?.nameLabel.text = course.name
            cell?.locationLabel.text = course.address + " " + course.city + " " + course.state
            
            return cell!
            
        } else if indexPath.section == 1 {
            
            var cell = tableView.dequeueReusableCellWithIdentifier("cell") as? CourseDetailTableViewCell
            if cell == nil {
                cell = CourseDetailTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
            }
            
            cell?.iconImageView.image = UIImage(named: itemArray[indexPath.row]["image"] as! String)
            cell?.label.text = itemArray[indexPath.row]["title"] as? String
            
            return cell!
            
        } else if indexPath.section == 2 {
        
            var cell = tableView.dequeueReusableCellWithIdentifier("cell") as? GenericDualTableViewCell
            if cell == nil {
                cell = GenericDualTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
            }
            return cell!
        } else {
            
            var cell = tableView.dequeueReusableCellWithIdentifier("cell") as? CourseDetailTableViewCell
            if cell == nil {
                cell = CourseDetailTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
                cell?.addChevron()
            }
            
            cell?.iconImageView.image = UIImage(named: itemArray[indexPath.row + 3]["image"] as! String)
            cell?.label.text = itemArray[indexPath.row + 3]["title"] as? String
            
            return cell!
        }
        
    }
    
    func startPressed() {
        self.navigationController?.customPush(GameSetupTableViewController())
    }
}