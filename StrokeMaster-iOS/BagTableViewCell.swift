//
//  BagTableViewCell.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 6/22/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class BagTableViewCell: UITableViewCell {
    
    let label = UILabel()
    let tickImageView = UIImageView(image: UIImage(named: "tick"))
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    
        self.backgroundColor = UIColor.clearColor()
        self.selectionStyle = .None
        
        label.font = UIFont(name: "\(kDefaultFont)-Light", size: 16.0)
        label.textColor = UIColor.whiteColor()
        
        self.contentView.addSubview(label)
        label.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsMake(0, 30.0, 0, 0))
        
    }
    
    //Add or remove from bag
    func addTick() {
        if tickImageView.superview == nil {
            self.contentView.addSubview(tickImageView)
            tickImageView.autoPinEdgeToSuperviewEdge(ALEdge.Right, withInset: 30.0)
            tickImageView.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        }
    }
    
    func removeTick() {
        if tickImageView.superview != nil {
            tickImageView.removeFromSuperview()
        }
    }
    
    func hasTick() -> Bool {
        return tickImageView.superview != nil
    }
}
