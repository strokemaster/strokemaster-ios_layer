//
//  ScoreIndexView.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 6/2/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class ScoreIndexView: UIView {
    
    let holesTag = UILabel()
    let teesTag = UILabel()
    let handicapTag = UILabel()
    let parTag = UILabel()
    let robertTag = UILabel()
    let approachTag = UILabel()
    let shortGameTag = UILabel()
    let puttsTag = UILabel()

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    
        holesTag.scoreTag()
        holesTag.text = "H"
        self.addSubview(holesTag)
        
        holesTag.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsMake(1, 0, 0, 0), excludingEdge: ALEdge.Bottom)
        holesTag.autoSetDimension(ALDimension.Height, toSize: 48.0)
        
        teesTag.scoreTag()
        teesTag.backgroundColor = kSolidBlue
        teesTag.text = "BT"
        self.addSubview(teesTag)
        
        teesTag.autoPinEdge(ALEdge.Top, toEdge: ALEdge.Bottom, ofView: holesTag, withOffset: 2.0)
        teesTag.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        teesTag.autoMatchDimension(ALDimension.Height, toDimension: ALDimension.Height, ofView: holesTag)
        teesTag.autoMatchDimension(ALDimension.Width, toDimension: ALDimension.Width, ofView: holesTag)
        
        handicapTag.scoreTag()
        handicapTag.text = "HP"
        self.addSubview(handicapTag)
        
        handicapTag.autoPinEdge(ALEdge.Top, toEdge: ALEdge.Bottom, ofView: teesTag, withOffset: 2.0)
        handicapTag.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        handicapTag.autoMatchDimension(ALDimension.Height, toDimension: ALDimension.Height, ofView: holesTag)
        handicapTag.autoMatchDimension(ALDimension.Width, toDimension: ALDimension.Width, ofView: holesTag)
        
        parTag.scoreTag()
        parTag.text = "P"
        self.addSubview(parTag)
        
        parTag.autoPinEdge(ALEdge.Top, toEdge: ALEdge.Bottom, ofView: handicapTag, withOffset: 2.0)
        parTag.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        parTag.autoMatchDimension(ALDimension.Height, toDimension: ALDimension.Height, ofView: holesTag)
        parTag.autoMatchDimension(ALDimension.Width, toDimension: ALDimension.Width, ofView: holesTag)
        
        robertTag.scoreTag()
        robertTag.text = "RDJ"
        self.addSubview(robertTag)
        
        robertTag.autoPinEdge(ALEdge.Top, toEdge: ALEdge.Bottom, ofView: parTag, withOffset: 2.0)
        robertTag.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        robertTag.autoMatchDimension(ALDimension.Height, toDimension: ALDimension.Height, ofView: holesTag)
        robertTag.autoMatchDimension(ALDimension.Width, toDimension: ALDimension.Width, ofView: holesTag)
        
        approachTag.scoreTag()
        approachTag.text = "A"
        self.addSubview(approachTag)
        
        approachTag.autoPinEdge(ALEdge.Top, toEdge: ALEdge.Bottom, ofView: robertTag, withOffset: 52.0)
        approachTag.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        approachTag.autoMatchDimension(ALDimension.Height, toDimension: ALDimension.Height, ofView: holesTag)
        approachTag.autoMatchDimension(ALDimension.Width, toDimension: ALDimension.Width, ofView: holesTag)
        
        shortGameTag.scoreTag()
        shortGameTag.text = "SG"
        self.addSubview(shortGameTag)
        
        shortGameTag.autoPinEdge(ALEdge.Top, toEdge: ALEdge.Bottom, ofView: approachTag, withOffset: 2.0)
        shortGameTag.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        shortGameTag.autoMatchDimension(ALDimension.Height, toDimension: ALDimension.Height, ofView: holesTag)
        shortGameTag.autoMatchDimension(ALDimension.Width, toDimension: ALDimension.Width, ofView: holesTag)
        
        puttsTag.scoreTag()
        puttsTag.text = "PU"
        self.addSubview(puttsTag)
        
        puttsTag.autoPinEdge(ALEdge.Top, toEdge: ALEdge.Bottom, ofView: shortGameTag, withOffset: 2.0)
        puttsTag.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        puttsTag.autoMatchDimension(ALDimension.Height, toDimension: ALDimension.Height, ofView: holesTag)
        puttsTag.autoMatchDimension(ALDimension.Width, toDimension: ALDimension.Width, ofView: holesTag)
    }

}
