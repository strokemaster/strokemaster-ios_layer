//
//  GenericIconLabelTableViewCell.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/8/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class GenericIconLabelTableViewCell: UITableViewCell {

    let iconImageView = UIImageView()
    let label = UILabel()
    let blackView = UIView()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.clearColor()
        self.selectionStyle = .None

        blackView.transparentBlackView()
        self.contentView.addSubview(blackView)
        
        blackView.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsMake(2, 0, 0, 0))
        blackView.addSubview(iconImageView)
        
        label.textColor = UIColor.lightGrayColor()
        label.font = UIFont(name: "\(kDefaultFont)-Medium", size: 15.0)
        blackView.addSubview(label)
    }
    
    func addChevron() {
        
        let chevron = UIImageView(image: UIImage(named: "chevron")!)
        blackView.addSubview(chevron)
        
        chevron.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        chevron.autoPinEdgeToSuperviewEdge(ALEdge.Right, withInset: 15.0)
    }
    
    func addChevronWithString(string: String) {
        
        self.addChevron()
        //ADD LABEL
        
    }
}
