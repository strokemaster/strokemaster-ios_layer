//
//  CourseTableHeaderView.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/8/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class CourseTableHeaderView: UIView {

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.blackColor()
    
        //Search bar not being used for alpha version
        /*let searchBar = UISearchBar(frame: CGRectMake(0, 0, SCREEN_WIDTH - 30, 44))
        searchBar.placeholder = "Find your course"
        searchBar.tintColor = UIColor.whiteColor()
        searchBar.barTintColor = UIColor.blackColor()
        searchBar.autocorrectionType = UITextAutocorrectionType.No;
        searchBar.autocapitalizationType = UITextAutocapitalizationType.None;
        self.addSubview(searchBar)*/
    
    
    }

}
