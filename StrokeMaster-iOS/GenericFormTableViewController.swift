//
//  GenericFormTableViewController.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/4/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class GenericFormTableViewController: UITableViewController, FormTableFooterViewDelegate {

    let cellIdentifier = "loginCell"
    var formDataArray: NSArray!
    var footerView: FormTableFooterView!
    var headerView: FormTableHeaderView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.fillDataArray()
        self.setUpTableViewUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return formDataArray.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? FormTableViewCell
        if cell == nil {
            cell = FormTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
        }
        
        cell?.iconImageView.image = UIImage(named: formDataArray[indexPath.row]["image"] as! String)
        cell?.textField.setLightGrayPlaceholder(formDataArray[indexPath.row]["placeholder"] as! String)
        self.setTextFieldType(indexPath.row, cell: cell!)
        
        
        return cell!
    }
    
    func setTextFieldType(index: NSInteger, cell: FormTableViewCell) {
        index == 0 ? cell.textField.mailTypeTextField() : cell.textField.passwordTypeTextField()
    }
    
    func setUpTableViewUI() {
        
        //Footer
        footerView = FormTableFooterView(frame: CGRectMake(0, 0, SCREEN_WIDTH, 120))
        footerView.delegate = self
        self.tableView.tableFooterView = footerView
        
        //Header
        
        headerView = FormTableHeaderView(frame: CGRectMake(0, 0, SCREEN_WIDTH, 120))
        self.tableView.tableHeaderView = headerView
        
        self.tableView.backgroundColor = UIColor.clearColor()
        self.tableView.separatorColor = UIColor.clearColor()
        self.tableView.bounces = false
        self.tableView.rowHeight = 44.0
    }
    
    func fillDataArray() {
        if let path = NSBundle.mainBundle().pathForResource("LoginFormList", ofType: "plist") {
            formDataArray = NSArray(contentsOfFile: path)!
        }
    }
    
    //MARK: FormTableFooterViewDelegate
    //button press to login.  Validates correct data entered.  If validated tries to login to app.
    func loginPressed() {
        
        //I'll push just to access the app without credentials. Delete later
//        self.pushViewController()
        
        //Call login service with data
        var email: String = ""
        var pass: String = ""
        
        //get email and password entered
        for index in 0...self.tableView.visibleCells.count - 1 {
            let cell =  tableView.cellForRowAtIndexPath(NSIndexPath(forRow: index, inSection: 0)) as! FormTableViewCell
            let text = cell.textField.text
            if(index == 0){
                email = text!
            }
            else{
                pass = text!
            }
            //println("text: \(text)")
        }
        
        //var result: String
        //uncomment to implement logging in
        var datastring: NSString = ""
        
        //validate correct password and email format before even sending web service.  Why waste the call?
        if(self.validateLoginInfo(email, pass: pass)){
            let semaphore = dispatch_semaphore_create(0)
            DataManager.tryLogin(email, password: pass, success:{(loginResult) -> Void in
                let result = loginResult
                datastring = NSString(data: result, encoding: NSUTF8StringEncoding)!
                dispatch_semaphore_signal(semaphore)
            })
            dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)
        }
        //validation fails set the result to failure
        else{
           datastring = "Failure"
        }
        
        //REMOVE WHEN USING ABOVE LOGIN CODE
        //var datastring: NSString = "Success"
    
        //If success
        //Push
        if(datastring == "Success"){
            
            let userdefaults = NSUserDefaults.standardUserDefaults()
            userdefaults.setObject(email, forKey: "Email")
            userdefaults.synchronize()
            

            let semaphore = dispatch_semaphore_create(0)
            var datastring: String = ""
                
            DataManager.getClubsList(email, success:{(clubsResult) -> Void in
                let result = clubsResult
                let datansstring: NSString = NSString(data: result, encoding: NSUTF8StringEncoding)!
                datastring = datansstring as String
                dispatch_semaphore_signal(semaphore)
            })
                
            dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)
                
                
            //fix this shit tomorrow
            let myClubs : [String] = datastring.componentsSeparatedByString(", ")
                
            userdefaults.setObject(myClubs, forKey: "Clubs")
            userdefaults.synchronize()
            
            
            self.pushViewController()
            //Show intro
            let userDefaults = NSUserDefaults.standardUserDefaults()
            userDefaults.setBool(true, forKey: "userLogged")
            userDefaults.setValue("userID", forKey: "currentUserID") //ADD USER ID AFTER LOGIN
        }
        //failed login
        else{
           let cell =  tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 1, inSection: 0)) as! FormTableViewCell
            cell.textField.text = ""
            self.headerView.setErrorMsg("Username/Password Combination Not Found")
        }
    }
    
    func pushViewController() {
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let tabBar = appDelegate.createDrawerController()
        
        //Animation Fix
        appDelegate.window?.rootViewController = tabBar
        appDelegate.window?.rootViewController = self.navigationController
        
        
        //Send to main thread
        dispatch_async(dispatch_get_main_queue(),{
            UIView.transitionWithView(appDelegate.window!,
                duration: 0.5,
                options: UIViewAnimationOptions.TransitionCrossDissolve,
                animations: {
                    appDelegate.window!.rootViewController = tabBar
                },
                completion: nil)
        })
    }
    
    func backPressed() {
        self.navigationController?.customPop()
    }
    
    //validate email entered
    func isValidEmail(testStr:String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
    
    //Validate login info before even sending to db
    func validateLoginInfo(email: String, pass: String) -> Bool {
        if(!isValidEmail(email)){
            self.headerView.setErrorMsg("Invalid Email Address")
            return false
        }
        if(pass.characters.count < 8){
            self.headerView.setErrorMsg("Username/Password Combination Not Found")
            return false
        }
        
        return true
    }
}
