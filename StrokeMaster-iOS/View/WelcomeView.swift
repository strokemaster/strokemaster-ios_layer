//
//  WelcomeView.swift
//  StrokeMaster-iOS
//
//  Created by TR Staake on 12/22/14.
//  Copyright (c) 2014 StrokeMaster. All rights reserved.
//

import UIKit

protocol WelcomeViewDelegate {
    func buttonPressed(index: NSInteger)
}

class WelcomeView: UIView {
    
    let logoImageView = UIImageView(image: UIImage(named: "logo"))
    let title = UILabel()
    let subtitle = UILabel()
    let loginButton = UIButton()
    let registerButton = UIButton()
    var delegate: WelcomeViewDelegate!
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(logoImageView)
        logoImageView.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        logoImageView.autoAlignAxis(ALAxis.Horizontal, toSameAxisOfView: self, withOffset: -(logoImageView.bounds.size.height/2))
        
        title.text = "Strokemaster"
        title.textColor = UIColor.whiteColor()
        title.font = UIFont(name: "\(kDefaultFont)-UltraLight", size: 50.0)
        self.addSubview(title)
        
        title.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        title.autoPinEdge(ALEdge.Top, toEdge: ALEdge.Bottom, ofView: logoImageView, withOffset: 15.0)
        
        subtitle.text = "The Smart Way To Keep Score™"
        subtitle.textColor = UIColor.whiteColor()
        subtitle.textAlignment = NSTextAlignment.Center
        subtitle.font = UIFont(name: "\(kDefaultFont)-ThinItalic", size: 15.0)
        self.addSubview(subtitle)
        
        subtitle.autoPinEdge(ALEdge.Top, toEdge: ALEdge.Bottom, ofView: title, withOffset: 5.0)
        subtitle.autoAlignAxis(ALAxis.Vertical, toSameAxisOfView: title)
        
        loginButton.transparentButtonWithTitle("LOG IN")
        loginButton.tag = 0
        loginButton.addTarget(self, action: "buttonPressed:", forControlEvents: UIControlEvents.TouchUpInside)
        self.addSubview(loginButton)
        
        loginButton.autoPinEdgeToSuperviewEdge(ALEdge.Bottom, withInset: SCREEN_HEIGHT/10)
        loginButton.autoPinEdgeToSuperviewEdge(ALEdge.Left)
        loginButton.autoSetDimension(ALDimension.Width, toSize: SCREEN_WIDTH/2 - 0.5)
        
        registerButton.transparentButtonWithTitle("REGISTER")
        registerButton.tag = 1
        registerButton.addTarget(self, action: "buttonPressed:", forControlEvents: UIControlEvents.TouchUpInside)
        self.addSubview(registerButton)
        
        registerButton.autoAlignAxis(ALAxis.Horizontal, toSameAxisOfView: loginButton)
        registerButton.autoPinEdgeToSuperviewEdge(ALEdge.Right)
        registerButton.autoMatchDimension(ALDimension.Width, toDimension: ALDimension.Width, ofView: loginButton)
    }
    
    func buttonPressed(sender: UIButton) {
        self.delegate.buttonPressed(sender.tag)
    }
}
