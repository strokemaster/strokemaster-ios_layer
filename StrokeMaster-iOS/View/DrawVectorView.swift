//
//  DrawVectorView.swift
//  StrokeMaster-iOS
//
//  Created by Greg Morano on 2/27/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//
import Foundation
import UIKit
import CoreLocation

protocol DrawVectorDelegate {
    func continuePressed()
}

class DrawVectorView: UIView {

    var greenPoints = [Line]()
    var fairwayPoints = [Line]()
    var perimeterPoints = [Line]()
    var greenCenter = CGPoint()
    var water = Dictionary<Int, [Line]>()
    var teeBoxes = Dictionary<Int, [Line]>()
    var bunkers = Dictionary<Int, [Line]>()
    var cartpath = Dictionary<Int, [Line]>()
    var trees  = [CGPoint]()
    var flag = [CGPoint]()
    var flagJson: JSON!
    var drawVectordelegate: DrawVectorDelegate!
    
    var json: JSON!
    
    var userLocation: CLLocation!
    var shouldFlip = false
    
    //Values
    var minx: CGFloat!
    var miny: CGFloat!
    var maxx: CGFloat!
    var maxy: CGFloat!
    
    var xratio: CGFloat!
    var yratio: CGFloat!
    
    //User pin
    var imageView = UIImageView(image: UIImage(named: "userPin"))
    
    override init(frame: CGRect) {
        super.init(frame: frame)
//        self.backgroundColor = UIColor(red: 0 , green: 40/255, blue: 0, alpha: 1)
        self.backgroundColor = .clearColor()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        
//        var semaphore = dispatch_semaphore_create(0)
//        DataManager.getTopAppsDataFromIGolfWithSuccess { (iGolfData) -> Void in
//            let json = JSON(data: iGolfData)
            self.greenPoints =  DataManager.pointsToVectorLines("green", json: self.json)
            self.fairwayPoints =  DataManager.pointsToVectorLines("fairway", json: self.json)
            self.perimeterPoints =  DataManager.pointsToVectorLines("perimeter", json: self.json)
            self.greenCenter = CGPoint(x: (json["greenCenter"][0]["x"].doubleValue) , y: (self.json["greenCenter"][0]["y"].doubleValue))
            self.water = DataManager.pointsToMultiVectorLines("water", json: self.json)
            self.teeBoxes = DataManager.pointsToMultiVectorLines("teeboxes", json: self.json)
            self.bunkers = DataManager.pointsToMultiVectorLines("bunkers", json: self.json)
            self.cartpath = DataManager.pointsToMultiVectorLines("cartpath", json: self.json)
            self.trees = DataManager.pointsToMultiPoints("trees", json: self.json)
            self.flag = DataManager.pointsToMultiPoints("greenCenter", json: self.json)
        flagJson = json["greenCenter"]
        
//            dispatch_semaphore_signal(semaphore)
//        }
        
//        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)

        minx = perimeterPoints[0].start.x
        miny = perimeterPoints[0].start.y
        maxx = perimeterPoints[0].start.x
        maxy = perimeterPoints[0].start.y
        
        for line in perimeterPoints{
        
            if(minx > line.start.x){
                minx = line.start.x
            }
            else if(maxx < line.start.x){
                maxx = line.start.x
            }
            if(miny > line.start.y){
                miny = line.start.y
            }
            else if(maxy < line.start.y){
                maxy = line.start.y
            }
            
            
        }
        
        //add a 5% buffer to the sides
        maxx = maxx + ((maxx-minx) * 0.05)
        minx = minx - ((maxx-minx) * 0.05)
        maxy = maxy + ((maxy-miny) * 0.05)
        miny = miny - ((maxy-miny) * 0.05)
        
        let context = UIGraphicsGetCurrentContext()
        xratio = rect.width/(maxx - minx)
        yratio = rect.height/(maxy - miny)
        var flip90 = false
        
        if((maxx-minx) > (maxy-miny)){
            //CGContextTranslateCTM(context, ( (maxx - minx) * xratio), ((maxy-miny ) * yratio))
            //CGContextRotateCTM(context, CGFloat(270 * Float(M_PI)/180))
            var temp = maxx
            maxx = maxy
            maxy = temp
            temp = minx
            minx = miny
            miny = temp
            xratio = rect.width/(maxx - minx)
            yratio = rect.height/(maxy - miny)
            flip90 = true
        }
        
        shouldFlip = flip90
        var flip180 = false
        
        //course rotations so tee box is always at the bottom of the screen
        if( ((maxy+miny)/2.0) < abs(greenCenter.y) && !flip90){
            CGContextTranslateCTM(context, ( (maxx - minx) * xratio), ((maxy-miny ) * yratio))
            CGContextRotateCTM(context, CGFloat(180 * Float(M_PI)/180))
            flip180 = true
        }
        else if( ((maxy+miny)/2.0) < abs(greenCenter.x) && flip90){
            CGContextTranslateCTM(context, ( (maxx - minx) * xratio), ((maxy-miny ) * yratio))
            CGContextRotateCTM(context, CGFloat(180 * Float(M_PI)/180))
            flip180 = true
        }
    
        //initialize perimeter path
        if(flip90){
            CGContextMoveToPoint(context,  ((maxx - perimeterPoints[0].start.y) * xratio ), ((perimeterPoints[0].start.x - miny) * yratio))
        }
        else{
            CGContextMoveToPoint(context,  ((perimeterPoints[0].start.x - minx) * xratio ), ((perimeterPoints[0].start.y - miny) * yratio))
        }
        //iterate through perimeter points and connect
        for line in perimeterPoints{
            if(flip90){
                CGContextAddLineToPoint(context, ((maxx - line.end.y) * xratio ), ((line.end.x - miny) * yratio))
            }
            else{
                CGContextAddLineToPoint(context, ((line.end.x - minx) * xratio ), ((line.end.y - miny) * yratio))
            }
        }
        
        let rough: UIImage = UIImage(named: "roughTexture3")!
        let roughColor: UIColor = UIColor(patternImage: rough)
        CGContextSetFillColorWithColor(context, roughColor.CGColor)
        
        CGContextFillPath(context)
        
        //move to green path statrt
        if(flip90){
            CGContextMoveToPoint(context,  ((maxx - greenPoints[0].start.y) * xratio ), ((greenPoints[0].start.x - miny) * yratio))
        }
        else{
            CGContextMoveToPoint(context,  ((greenPoints[0].start.x - minx) * xratio ), ((greenPoints[0].start.y - miny) * yratio))
        }
        //iterate through green points and connect
        for line in greenPoints{
            if(flip90){
               CGContextAddLineToPoint(context, ((maxx - line.end.y) * xratio), ((line.end.x - miny) * yratio))
            }
            else{
                CGContextAddLineToPoint(context, ((line.end.x - minx) * xratio), ((line.end.y - miny) * yratio))
            }
        }
        
        let green: UIImage = UIImage(named: "greentext")!
        let greenColor: UIColor = UIColor(patternImage: green)
        CGContextSetFillColorWithColor(context, greenColor.CGColor)
        
        CGContextFillPath(context)
        
        //move to fairway start point
        if(self.fairwayPoints.count > 0){
            if(flip90){
                CGContextMoveToPoint(context,  ((maxx - fairwayPoints[0].start.y) * xratio ), ((fairwayPoints[0].start.x - miny) * yratio))
            }
            else{
                CGContextMoveToPoint(context,  ((fairwayPoints[0].start.x - minx) * xratio ), ((fairwayPoints[0].start.y - miny) * yratio))
            }
            //iterate through fairway points and connect
            for line in fairwayPoints{
                if(flip90){
                    CGContextAddLineToPoint(context, ((maxx - line.end.y) * xratio), ((line.end.x - miny) * yratio))
                }
                else{
                    CGContextAddLineToPoint(context, ((line.end.x - minx) * xratio), ((line.end.y - miny) *   yratio))
                }
            }
            let grass = UIImage(named: "fairway")  //(imageNamed: "fairway.jpeg")!
            let grassColor = UIColor(patternImage: grass!)
            CGContextSetFillColorWithColor(context, grassColor.CGColor)
        
            CGContextFillPath(context)
        }
        //iterate through all the water points and draw them
        for lines in water{
            //move to water start point
            if(flip90){
                CGContextMoveToPoint(context,  ((maxx - lines.1[0].start.y) * xratio ), ((lines.1[0].start.x - miny) * yratio))
            }
            else{
                CGContextMoveToPoint(context,  ((lines.1[0].start.x - minx) * xratio ), ((lines.1[0].start.y - miny) * yratio))
            }
            
            //iterate through water points
            for line in lines.1{
                if(flip90){
                    CGContextAddLineToPoint(context, ((maxx - line.end.y) * xratio), ((line.end.x - miny) * yratio))
                }
                else{
                    CGContextAddLineToPoint(context, ((line.end.x - minx) * xratio), ((line.end.y - miny) * yratio))
                }
            }
    
            let water: UIImage = UIImage(named: "water")!
            let waterColor: UIColor = UIColor(patternImage: water)
            CGContextSetFillColorWithColor(context, waterColor.CGColor)
            
            
            CGContextFillPath(context)
        }
        
        //iterate through all the teebox points and draw them
        for lines in teeBoxes{
            //move to teebox start point
            if(flip90){
                CGContextMoveToPoint(context,  ((maxx - lines.1[0].start.y) * xratio ), ((lines.1[0].start.x - miny) * yratio))
            }
            else{
                CGContextMoveToPoint(context,  ((lines.1[0].start.x - minx) * xratio ), ((lines.1[0].start.y - miny) * yratio))
            }
            //iterate through teebox points
            for line in lines.1{
                if(flip90){
                    CGContextAddLineToPoint(context, ((maxx - line.end.y) * xratio), ((line.end.x - miny) * yratio))
                }
                else{
                    CGContextAddLineToPoint(context, ((line.end.x - minx) * xratio), ((line.end.y - miny) * yratio))
                }
            }
            
            CGContextSetFillColorWithColor(context, greenColor.CGColor)
            
            CGContextFillPath(context)
        }
        
        
        //iterate through all the bunker points and draw them
        for lines in bunkers{
            //move to bunker start point
            if(flip90){
                CGContextMoveToPoint(context,  ((maxx - lines.1[0].start.y) * xratio ), ((lines.1[0].start.x - miny) * yratio))
            }
            else{
                CGContextMoveToPoint(context,  ((lines.1[0].start.x - minx) * xratio ), ((lines.1[0].start.y - miny) * yratio))
            }
            //iterate through bunker points
            for line in lines.1{
                if(flip90){
                    CGContextAddLineToPoint(context, ((maxx - line.end.y) * xratio), ((line.end.x - miny) * yratio))
                }
                else{
                    CGContextAddLineToPoint(context, ((line.end.x - minx) * xratio), ((line.end.y - miny) * yratio))
                }
            }
            
            let bunker: UIImage = UIImage(named: "sand")!
            let bunkerColor: UIColor = UIColor(patternImage: bunker)
            CGContextSetFillColorWithColor(context, bunkerColor.CGColor)
            

            CGContextFillPath(context)
        }
        
        //iterate through all the cartpath items and draw them
        for lines in cartpath{
            //move to cartpath start point
            if(flip90){
                CGContextMoveToPoint(context,  ((maxx - lines.1[0].start.y) * xratio ), ((lines.1[0].start.x - miny) * yratio))
            }
            else{
                CGContextMoveToPoint(context,  ((lines.1[0].start.x - minx) * xratio ), ((lines.1[0].start.y - miny) * yratio))
            }

            //iterate through cartpath points
            for line in lines.1{
                if(flip90){
                    CGContextAddLineToPoint(context, ((maxx  - line.end.y) * xratio), ((line.end.x - miny) * yratio))
                }
                else{
                    CGContextAddLineToPoint(context, ((line.end.x - minx) * xratio), ((line.end.y - miny) * yratio))
                }
            }
            //increase line width for more accurate path width
            CGContextSetLineWidth(context, 4)
            CGContextSetRGBStrokeColor(context, 150/255, 150/255, 150/255, 1)
            CGContextStrokePath(context)
        }
        
        //iterate through all the tree and place them on the map
        for point in trees{
            
            var myPoint: CGPoint
            let treeHeight: CGFloat = 25.0
            let treeWidth: CGFloat = 15.0
            
            var tree = UIImage(named: "tree6")!
            
            if(flip90){
                if(flip180){
                    myPoint = CGPoint(x: ((maxx - point.y) * xratio ), y: ((point.x - miny) * yratio))
                }
                else{
                    myPoint = CGPoint(x: ((maxx - point.y) * xratio ) - treeWidth, y: ((point.x - miny) * yratio) - treeHeight )
                    tree = UIImage(named: "invertedTree")!
                }
            }
            else{
                if flip180{
                    myPoint = CGPoint(x: ((point.x - minx) * xratio ), y: ((point.y - miny) * yratio))
                }
                else{
                    myPoint = CGPoint(x: ((point.x - minx) * xratio ) - treeWidth, y: ((point.y - miny) * yratio) - treeHeight)
                    tree = UIImage(named: "invertedTree")!
                }
            }
            
            //Different sizes
            //let sizeIncrement = CGFloat(arc4random_uniform(3))
            let treeBox: CGRect = CGRect(origin: myPoint, size: CGSizeMake(treeWidth, treeHeight))
            CGContextDrawImage(context, treeBox, tree.CGImage)
        }
        
        //Place flag at the center of the green
        
        let array = json["greenCenter"].arrayValue
        let dictionary = array[0].dictionaryValue
            
        let xValue = dictionary["x"]!.doubleValue
        let yValue = dictionary["y"]!.doubleValue

        var myPoint = CGPointMake(CGFloat(xValue) * (-1), CGFloat(yValue))
        let flagSize = CGSizeMake(15.0, 40.0)
        var flagImage = UIImage(named: "flag")!
        
        if flip90 {
            if(flip180){
              myPoint = CGPoint(x: ((maxx - myPoint.y) * xratio ) , y: ((myPoint.x - miny) * yratio) )
            }
            else{
                myPoint = CGPoint(x: ((maxx - myPoint.y) * xratio ) - flagSize.width, y: ((myPoint.x - miny) * yratio) - flagSize.height)
                flagImage = UIImage(named: "invertedFlag")!
            }
        }
        else {
            if(flip180){
                myPoint = CGPoint(x: ((myPoint.x - minx) * xratio ), y: ((myPoint.y - miny) * yratio) )
            }
            else{
               myPoint = CGPoint(x: ((myPoint.x - minx) * xratio ) - flagSize.width, y: ((myPoint.y - miny) * yratio) - flagSize.height )
                flagImage = UIImage(named: "invertedFlag")!
            }
            
        }
        
        let flagRect: CGRect = CGRect(origin: myPoint, size: CGSizeMake(flagSize.width, flagSize.height))
        CGContextDrawImage(context, flagRect, flagImage.CGImage)
    }

    func buttonPressed(sender: UIButton) {
        self.drawVectordelegate.continuePressed()
    }

    //User location
    func drawUserLocation(location: CLLocation) {

        print(location.coordinate.latitude)
    
        var point = CGPointMake(CGFloat(location.coordinate.longitude) * (-1), CGFloat(location.coordinate.latitude))
            
        if shouldFlip == true {
            point = CGPoint(x: ((maxx - point.y) * xratio ), y: ((point.x - miny) * yratio))
        }
        else {
            point = CGPoint(x: ((point.x - minx) * xratio ), y: ((point.y - miny) * yratio))
        }
        
        let pinCircle: CGRect = CGRect(origin: point, size: CGSizeMake(20, 20))
        
        //User location is correct. Draw icon in position
        
        if imageView.superview == nil {
            imageView.frame = pinCircle
            self.addSubview(imageView)
        } else {
            //You can animate the new location like this
            UIView.animateWithDuration(0.33, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
                
                self.imageView.frame = pinCircle
                
                }, completion: nil)
        
        }
    }
}
