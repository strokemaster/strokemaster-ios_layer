//
//  CoursesTableViewController.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/5/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit
import CoreLocation

class CoursesTableViewController: UITableViewController, SideMenuDelegate, CLLocationManagerDelegate {
    
    let cellIdentifier = "courseCell"
    var courses = [CourseListCourse]()
    var coursesScorecard = [CourseScorecard]()
    var locationManager: CLLocationManager!
    let numberOfNearbyCourses = 5
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //initalize our location manager
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        
        self.navigationItem.title = "Strokemaster"
        
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = backButton
        
        self.view.backgroundColor = UIColor.clearColor()
        
        //Table UI
        self.tableView.rowHeight = 100.0
        self.tableView.separatorColor = UIColor.clearColor()
        
        //Start Transparent and show when courses appear
        self.tableView.alpha = 0.0
        
        //section headers

        //Create MenuButton
        let leftDrawerButton = UIBarButtonItem(image: UIImage(named: "menuButton"), style: UIBarButtonItemStyle.Plain, target: self, action: "menuPressed")
        self.navigationItem.setLeftBarButtonItem(leftDrawerButton, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.evo_drawerController?.openDrawerGestureModeMask = .All
        self.evo_drawerController?.closeDrawerGestureModeMask = .All
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        //Show intro
        let hasSeen = NSUserDefaults.standardUserDefaults().objectForKey("hasSeenIntro") as? Bool
        if hasSeen != true {
            self.presentViewController(WalkthroughViewController(), animated: true, completion: nil)
        }
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView {
        
        let label = UILabel()
        label.textColor = UIColor.whiteColor()
        label.backgroundColor = kLightGreenColor
        label.textAlignment = NSTextAlignment.Center
        label.text = section == 0 ? "Quick Play" : "Explore Nearby"
        return label
    }
    
    //Height needed when using custom header
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if courses.count > 0 {
            //let bah: String = String( String(section) + " " + String(courses.count-1))
            //print(bah)
            return section == 0 ? 1 : self.numberOfNearbyCourses
            
        }

        return 0
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? CourseTableViewCell
        
        if cell == nil {
            cell = CourseTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
            cell?.addArrow()
            cell?.courseImageView.image = UIImage(named: "course\(indexPath.row + indexPath.section)")
        }
        
        //FIX-ME checks
        
        let course = courses[indexPath.row + indexPath.section]
        if course.name != nil {
            cell?.nameLabel.text = course.name
        }
        
        if course.address != nil && course.city != nil && course.state != nil {
            cell?.locationLabel.text = course.address + " " + course.city + " " + course.state
        }
        
        return cell!
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let control = GameSetupTableViewController()
        
        //Quick sintax, no need for itemAtIndex(N)
        control.course = courses[indexPath.row + indexPath.section]
        self.navigationController?.customPush(control)
    }
    

    func menuPressed() {
        self.evo_drawerController?.toggleDrawerSide(.Left, animated: true, completion: nil)
    }
    
    //MARK: SideMenuDelegate
    func bagPressed() {
        self.menuPressed()
        self.navigationController?.customPush(BagTableViewController())
    }
    
    //MARK: - CLLocationManagerDelegate
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        manager.stopUpdatingLocation()
        getCourseList()
    }
    
    //Get courses nearby
    func getCourseList() {
        
        let semaphore = dispatch_semaphore_create(0)
        
        DataManager.getCourseList(locationManager.location!.coordinate.longitude, lat: locationManager.location!.coordinate.latitude, success:{(listResult) -> Void in
            let result = listResult
            let json = JSON(data: result)
            //datastring = NSString(data: result, encoding: NSUTF8StringEncoding)!
            
            //iterate through courses and put them into an object and add to gloabal list
            for(var i = 0; i < json["courses"].count; i++){
                let myCourse: CourseListCourse = CourseListCourse(id: json["courses"][i]["id_course"].string, name: json["courses"][i]["courseName"].string, address: json["courses"][i]["address1"].string, city: json["courses"][i]["city"].string, state: json["courses"][i]["stateShort"].string, courseId: json["courses"][i]["id_course"].string, numHole: json["courses"][i]["layoutHoles"].int)
                self.courses.append(myCourse)
            }
            
            self.getCourseScorecardList()
        
            
            dispatch_semaphore_signal(semaphore)
        })
        
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)
            //Main Queue
            dispatch_async(dispatch_get_main_queue(),{
                self.tableView.reloadData()
                UIView.animateWithDuration(0.33,
                    delay: 0.0,
                    options: UIViewAnimationOptions.CurveEaseInOut,
                    animations: {
                        self.tableView.alpha = 1.0
                    },completion: nil)
            })
        //})

    }
    
    //Get Scorecard Information for all courses in the table
    func getCourseScorecardList(){
        
        var idList = [String]()
        for(var i = 0; i < self.courses.count; i++){
            if courses[i].courseId != nil {
                idList.append(self.courses[i].courseId)   
            }
        }

        DataManager.getCourseScorecardList(idList, success:{(listResult) -> Void in
            let result = listResult
            let json = JSON(data: result)
            
            for(var i = 0; i < json.count; i++){
                
                //Add course Card to each courseObject
                let courseCard: CourseScorecard = CourseScorecard(json: json[i])
                self.courses[i].scoreCardInfo = courseCard
            }
        })
    }
}
