//
//  HorizontalButtonsScrollItem.h
//  Vorterix
//

#import <Foundation/Foundation.h>
@class HorizontalButtonView;

@interface HorizontalButtonsScrollItem : NSObject

@property (nonatomic, assign) int itemID;
@property (nonatomic, strong, readonly) NSString *title;
@property (nonatomic, strong, readonly) NSString *subtitle;

- (id)initWithID:(int)itemID title:(NSString *)title subtitle:(NSString *)subtitle;

@end
