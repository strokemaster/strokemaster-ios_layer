//
//  GpsGameView.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/16/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit
import CoreLocation

class GpsGameView: GenericGameView {
    
//    var courseImage = DrawVectorView(frame: CGRectMake(40, 30, 260, 400))
    var courseImage = DrawVectorView()
    var greenStart: InGameTopBox!
    var greenCenter: InGameTopBox!
    var greenEnd: InGameTopBox!

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addCourseImage()
        
        //Bottom Boxes
        greenStart = InGameTopBox()
        greenStart.titleLabel.text = "FRONT"
        self.addSubview(greenStart)
        
        greenStart.autoPinEdgeToSuperviewEdge(ALEdge.Left)
        greenStart.autoSetDimension(ALDimension.Width, toSize: SCREEN_WIDTH/3 - 1)
        greenStart.autoPinEdgeToSuperviewEdge(ALEdge.Bottom, withInset: 60.0)
        
        greenCenter = InGameTopBox()
        greenCenter.titleLabel.text = "CENTER"
        self.addSubview(greenCenter)
        
        greenCenter.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        greenCenter.autoSetDimension(ALDimension.Width, toSize: SCREEN_WIDTH/3 - 1)
        greenCenter.autoPinEdge(ALEdge.Bottom, toEdge: ALEdge.Bottom, ofView: greenStart)
        
        greenEnd = InGameTopBox()
        greenEnd.titleLabel.text = "BACK"
        self.addSubview(greenEnd)
        
        greenEnd.autoPinEdgeToSuperviewEdge(ALEdge.Right)
        greenEnd.autoSetDimension(ALDimension.Width, toSize: SCREEN_WIDTH/3 - 1)
        greenEnd.autoPinEdge(ALEdge.Bottom, toEdge: ALEdge.Bottom, ofView: greenStart)
    }
    
    func addCourseImage() {
        self.addSubview(courseImage)
        courseImage.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        courseImage.autoPinEdge(ALEdge.Top, toEdge: ALEdge.Bottom, ofView: yardsView, withOffset: 10.0)
        courseImage.autoSetDimensionsToSize(CGSizeMake(250, 380))
    }
    
    //Get the distance to Start/Center/End of green and fill labels
    func calculateGreenDistances(golferPosition: CLLocation) {

        let array = courseImage.json["greenCenter"].arrayValue
        let dictionary = array[0].dictionaryValue
        
        let xValue = dictionary["x"]!.doubleValue
        let yValue = dictionary["y"]!.doubleValue
        
        let centerLocation = CLLocation(latitude: xValue, longitude: yValue)
        
        var locationArray = courseImage.json["green"].arrayValue
        var locationDict = locationArray[0].dictionaryValue
        
        var xPos = locationDict["x"]!.doubleValue
        var yPos = locationDict["y"]!.doubleValue
        
        let startLocation = CLLocation(latitude: xPos, longitude: yPos)
        
        locationDict = locationArray[locationArray.count/2].dictionaryValue
        
        xPos = locationDict["x"]!.doubleValue
        yPos = locationDict["y"]!.doubleValue

        let endLocation = CLLocation(latitude: xPos, longitude: yPos)
        
        let startDistance = round(startLocation.distanceFromLocation(golferPosition) * 1.09)
        let centerDistance = round(centerLocation.distanceFromLocation(golferPosition) * 1.09)
        let endDistance = round(endLocation.distanceFromLocation(golferPosition) * 1.09)
        
        
        if startDistance < 600 {
            greenStart.informationLabel.text = "\(Int(startDistance)) yards"
            greenCenter.informationLabel.text = "\(Int(centerDistance)) yards"
            greenEnd.informationLabel.text = "\(Int(endDistance)) yards"
        }
    }
}
