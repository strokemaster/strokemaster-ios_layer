//
//  RegisterFirstStepViewController.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/4/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

//View Controller for custimze bag start page
class RegisterFirstStepViewController: UIViewController, RegisterStepsDelegate {
    
    override func loadView() {
        super.loadView()
        let registerView = RegisterStepView()
        registerView.delegate = self
        self.view = registerView
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func continuePressed() {
        self.navigationController?.customPush(BagSetupViewController())
        //self.pushViewController()
    }
    
    func pushViewController() {
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let tabBar = self.createDrawerController()
        
        //Animation Fix
        appDelegate.window?.rootViewController = tabBar
        appDelegate.window?.rootViewController = self.navigationController
        
        UIView.transitionWithView(appDelegate.window!,
            duration: 0.5,
            options: UIViewAnimationOptions.TransitionCrossDissolve,
            animations: {
                appDelegate.window!.rootViewController = tabBar
            },
            completion: nil)
        
    }
    
    //MARK: Create DrawerController
    
    func createDrawerController() -> DrawerController {
        
        let tabBarController = UITabBarController()
        let viewControllerArray: NSArray = NSArray(objects: UINavigationController(viewController:CoursesTableViewController()))
        tabBarController.viewControllers = viewControllerArray as? [UIViewController]
        
        tabBarController.view.backgroundColor = UIColor(patternImage: UIImage(named: "backgroundblack")!)
        
        tabBarController.tabBar.barTintColor = UIColor.blackColor()
        tabBarController.tabBar.translucent = false
        
        let drawerController = DrawerController(centerViewController: tabBarController, leftDrawerViewController: SideMenuTableViewController())
        
        drawerController.showsShadows = true
        drawerController.restorationIdentifier = "Drawer"
        drawerController.maximumLeftDrawerWidth = 240.0
        drawerController.openDrawerGestureModeMask = .All
        drawerController.closeDrawerGestureModeMask = .All
        drawerController.drawerVisualStateBlock = DrawerVisualState.slideAndScaleVisualStateBlock
        
        //Create icons for tab
        let tabItems = tabBarController.tabBar.items as [UITabBarItem]!
        tabItems[0].title = "Courses"
        tabItems[0].image = UIImage(named: "courses")
        
        
        return drawerController
    }
}
