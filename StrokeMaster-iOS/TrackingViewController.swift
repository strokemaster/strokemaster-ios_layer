//
//  TrackingViewController.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/14/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit
import CoreLocation

class TrackingViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, TrackingViewDelegate, ClubsTableViewControllerDelegate, PuttsTableViewControllerDelegate {
    
    var tableView: UITableView!
    var strokeArray: Array<SMStroke>!
    var strokeCount: Int?
    var userLocated: Bool?
    let yardValue = 1.09361
    
    var hole: SMHole!
    var trackingView: TrackingView!
    
    var previousPoint: CLLocation!
    
    //Parent 
    var containerViewController: CategoriesContainerViewController!
    
    override func loadView() {
        super.loadView()
        trackingView = TrackingView(frame: self.view.bounds)
        trackingView.delegate = self
        self.view = trackingView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        /*
        Moved location manager to CategoriesContainerViewController so it can send data to both
        tracking and gps controllers
        */
        
//        locationManager = CLLocationManager()
//        locationManager.delegate = self
//        locationManager.desiredAccuracy = kCLLocationAccuracyBest
//        locationManager.activityType = CLActivityType.Fitness
//        locationManager.startUpdatingLocation()
        
        containerViewController = self.parentViewController!.parentViewController!.parentViewController as! CategoriesContainerViewController
        
        
        strokeArray = Array()
        
        tableView = UITableView(frame: CGRectMake(0, SCREEN_HEIGHT - (SCREEN_HEIGHT/3 + 120), SCREEN_WIDTH, 176.0), style: UITableViewStyle.Plain)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = UIColor.clearColor()
        tableView.tableFooterView = UIView()
        tableView.rowHeight = 44.0
        
        //Disable Scrolling
        tableView.scrollEnabled = false
        
        self.view.addSubview(tableView)
        
        //SetUp Top boxes info with hole
        trackingView.fillInformationWithHole(hole)
        
        //PreviousPoint (Delete when the initial hole position is given from API)
        //previousPoint = containerViewController.getLocation()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        let string = ""
        let newString = string.suffixForNumber(hole.number)
        containerViewController.changeNavigationTitle("\(newString) Hole")
    }
    
    //TableView Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return strokeArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("trackCell") as? TrackingTableViewCell
        if cell == nil {
            cell = TrackingTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "trackCell")
        }
        
        let stroke = strokeArray[indexPath.row]
        cell?.strokeLabel.text = "Stroke " + "\(stroke.index + 1)"
        
        if let _ = stroke.club {
            let string = stroke.club!.name
            cell?.clubLabel.text = string
        } else {
            cell?.clubLabel.text = "-"
        }

       
        if let yards = stroke.yards {
            cell?.yardsLabel.text = "\(yards) yards"
        //} else if let feet = stroke.feet {
          //  cell?.yardsLabel.text = checkPuttValue(feet)
        } else {
            cell?.yardsLabel.text = "-"
        }
    
        return cell!
    }
    
    //Deleting cell style
//    func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
//        return UITableViewCellEditingStyle.Delete
//    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            // handle delete (by removing the data from your array and updating the tableview)
            strokeArray.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
        }
    }
    
//    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
//        if (editingStyle == UITableViewCellEditingStyle.Delete) {
//            strokeArray.removeAtIndex(indexPath.row)
//            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
//        }
//    }
    
    //Check feet for correct label
    func checkPuttValue(feetValue: Int) -> String {
        
        //TO-DO Fix Code Structure 
        if feetValue == 1 {
            return "< 1 feet"
        } else if feetValue == 50 {
            return "40+ feet"
        } else {
            return "\(feetValue) feet"
        }
    }
    
    //MARK: TrackingViewDelegate
    
    func gpsButtonPressed() {
        
        let clubsMenu = ClubsTableViewController()
//        let clubsMenu = BagTableViewController()
        clubsMenu.clubsDelegate = self
        self.evo_drawerController?.rightDrawerViewController = clubsMenu
        
        self.evo_drawerController?.toggleDrawerSide(.Right, animated: true, completion: nil)
    }
    
    func penaltyButtonPressed() {

        let penaltyStroke = SMStroke(type: "Penalty", club: nil, yards: nil, startPoint: "0,0", endPoint: "0,0", startDist: 0 ,endDist: 0 , index: strokeArray.count)
        strokeArray.insert(penaltyStroke, atIndex: 0)
        
        self.tableView.beginUpdates()
        self.tableView.insertRowsAtIndexPaths([NSIndexPath(forRow: 0, inSection: 0)], withRowAnimation: UITableViewRowAnimation.Fade)
        self.tableView.endUpdates()
    }
    
    func puttButtonPressed() {
    
        let puttMenu = PuttsTableViewController()
        puttMenu.puttDelegate = self
        self.evo_drawerController?.rightDrawerViewController = puttMenu
        
        self.evo_drawerController?.toggleDrawerSide(.Right, animated: true, completion: nil)
    }
    
    //MARK: ClubsTableDelegate
    
    func clubSelected(club: SMClub) {

        let distance = getStrokeDistance()
        
        let stroke = SMStroke(type: "Stroke", club: club, yards: distance, startPoint: "0,0",endPoint: "0,0",startDist: 0,endDist: 0, index: strokeArray.count, clubNumber: club.clubNumber)
        strokeArray.insert(stroke, atIndex: 0)
        addStroke()
    }
    
    //MARK: - PutterTableDelegate
    func puttSelected(distance: NSInteger) {
        
        let stroke = SMStroke(type: "Putt", club: SMClub(type: Club.Putter, number: 0, name: "Putter"), yards: distance, startPoint: "0,0",endPoint: "0,0",startDist: 0,endDist: 0, index: strokeArray.count, clubNumber: 0)
        strokeArray.insert(stroke, atIndex: 0)
        addStroke()         
    }
    
    //Add Stroke to Table
    func addStroke() {
        
        self.tableView.beginUpdates()
        self.tableView.insertRowsAtIndexPaths([NSIndexPath(forRow: 0, inSection: 0)], withRowAnimation: UITableViewRowAnimation.Fade)
        self.tableView.endUpdates()
        
        self.evo_drawerController?.toggleDrawerSide(.Right, animated: true, completion: nil)
    }
    
    //Get Distance between the two latest strokes
    func getStrokeDistance() -> Int {
        
        //Get actual point (after ball hit) from parentViewController
        print("Getting location")
        let location = containerViewController.getLocation()
        let latitude = location.coordinate.latitude
        let longitude = location.coordinate.longitude

        let ballPosition = CLLocation(latitude: latitude, longitude: longitude)

        print(ballPosition)
        //Calculate distance taking into account previous position
        let distanceCovered = ballPosition.distanceFromLocation(previousPoint)
        print("Previous Location")
        print(previousPoint)
        let yards = round(distanceCovered * yardValue)
        print("Shot")
        print(self.strokeArray.count)
        print("Yards")
        print(yards)
        previousPoint = ballPosition
        print("\n")
        return Int(yards)
    }
    
    //Update new hole
    func updateHole(hole: SMHole) {
        trackingView.fillInformationWithHole(hole)
        strokeArray.removeAll(keepCapacity: false)
        tableView.reloadData()
        self.hole = hole
        self.trackingView.refreshTrackingView()
    }
    
    //MARK: StartHoleDelegate 
    func updateStartLocation(){
        print("Initializing previous Location")
        print("Hole " +  String(self.hole.number))
        print("\n")
        previousPoint = containerViewController.getLocation()
    }
}
        

