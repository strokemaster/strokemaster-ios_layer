//
//  CourseTableViewCell.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/5/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class CourseTableViewCell: UITableViewCell {
    
    let blackView = UIView()
    let courseImageView = UIImageView()
    let nameLabel = UILabel()
    let locationLabel = UILabel()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    
        self.selectionStyle = UITableViewCellSelectionStyle.None
        self.backgroundColor = UIColor.clearColor()
        
        blackView.transparentBlackView()
        blackView.drawBordersWithColorAndWidth(UIColor.blackColor(), width: 1.0)
        self.contentView.addSubview(blackView)
        
        blackView.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsMake(0, -1, 5, -1))
    
        
        //FIX-ME Create circular image
        courseImageView.image = UIImage(named: "course")
        courseImageView.circularImageViewWithWidth(60)
        blackView.addSubview(courseImageView)
        
        courseImageView.autoPinEdgeToSuperviewEdge(ALEdge.Left, withInset: 15.0                                 )
        courseImageView.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        courseImageView.autoSetDimensionsToSize(CGSizeMake(60, 60))

        nameLabel.textColor = UIColor.whiteColor()
        nameLabel.font = UIFont(name: "\(kDefaultFont)", size: 18.0)

        blackView.addSubview(nameLabel)
        
        nameLabel.autoPinEdge(ALEdge.Left, toEdge: ALEdge.Right, ofView: courseImageView, withOffset: 15.0)
        nameLabel.autoAlignAxis(ALAxis.Horizontal, toSameAxisOfView: blackView, withOffset: -12.0)
        nameLabel.autoPinEdgeToSuperviewEdge(ALEdge.Right, withInset: 30.0)
        
        locationLabel.textColor = UIColor.whiteColor()
        locationLabel.font = UIFont(name: "\(kDefaultFont)-LightItalic", size: 14.0)
        blackView.addSubview(locationLabel)
        
        locationLabel.autoPinEdge(ALEdge.Left, toEdge: ALEdge.Left, ofView: nameLabel)
        locationLabel.autoAlignAxis(ALAxis.Horizontal, toSameAxisOfView: blackView, withOffset: 12.0)
    }

    func addArrow() {
        let arrowImage = UIImageView(image: UIImage(named: "arrow")!)
        blackView.addSubview(arrowImage)
        arrowImage.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        arrowImage.autoPinEdgeToSuperviewEdge(ALEdge.Right, withInset: 10.0)
    }

}
