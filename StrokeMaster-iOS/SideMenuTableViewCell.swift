//
//  SideMenuTableViewCell.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/6/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: GenericIconLabelTableViewCell {
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.blackColor()
    
        iconImageView.autoPinEdgeToSuperviewEdge(ALEdge.Left, withInset: 15.0)
        iconImageView.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        
        label.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        label.autoPinEdge(ALEdge.Left, toEdge: ALEdge.Right, ofView: iconImageView, withOffset: 10.0)
        
        self.addChevron()
    }
    
    func setWhiteStyle() {
        self.label.font = UIFont(name: "\(kDefaultFont)-Light", size: 15.0)
        self.label.textColor = UIColor.whiteColor()
        self.backgroundColor = UIColor.clearColor()
    }
    
    override func addChevron() {
            
        let chevron = UIImageView(image: UIImage(named: "chevron")!)
        blackView.addSubview(chevron)
            
        chevron.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        chevron.autoPinEdgeToSuperviewEdge(ALEdge.Right, withInset: 15.0)
    }
}
