//
//  HorizontalButtonsScroll.m
//  Vorterix
//

#import "HorizontalButtonsScroll.h"
#import "HorizontalButtonsScrollCell.h"
#import "CWDLeftAlignedCollectionViewFlowLayout.h"

@interface HorizontalButtonsScroll () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate>
{
    CWDLeftAlignedCollectionViewFlowLayout *_collectionViewLayout;
    UICollectionView *_collectionView;
    NSInteger _selectedIndex;
}
@end

@implementation HorizontalButtonsScroll

- (id)initWithFrame:(CGRect)frame
{
    self = [self initWithFrame:frame fixedCellSize:CGSizeZero];
    if (self) {

    }
    return self;
}

- (id)initWithFrame:(CGRect)frame fixedCellSize:(CGSize)size
{
    self = [super initWithFrame:frame];
    if (self) {
        _fixedCellSize = size;
        
        _collectionViewLayout = [[CWDLeftAlignedCollectionViewFlowLayout alloc] init];
        [_collectionViewLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        _collectionViewLayout.minimumInteritemSpacing = 0;
        _collectionViewLayout.minimumLineSpacing = 0;
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:_collectionViewLayout];
        _collectionView.translatesAutoresizingMaskIntoConstraints = NO;
        [_collectionView registerClass:[HorizontalButtonsScrollCell class] forCellWithReuseIdentifier:@"Cell"];
        [_collectionView setBackgroundColor: [UIColor blackColor]];
        [_collectionView setShowsHorizontalScrollIndicator:NO];
        _collectionView.bounces = false;
        [self addSubview:_collectionView];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_collectionView]|"
                                                                     options:0
                                                                     metrics:nil
                                                                       views:NSDictionaryOfVariableBindings(_collectionView)]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_collectionView]|"
                                                                     options:0
                                                                     metrics:nil
                                                                       views:NSDictionaryOfVariableBindings(_collectionView)]];
        
        self.iconArray = [NSMutableArray new];
        
    }
    
    return self;
}

- (void)selectButtonWithIndex:(NSInteger)buttonIndex
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:buttonIndex inSection:0];
    [_collectionView selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionCenteredHorizontally];
    HorizontalButtonsScrollCell *cell = (HorizontalButtonsScrollCell *)[_collectionView cellForItemAtIndexPath:indexPath];
    cell.selected = YES;
//    [_collectionView.delegate collectionView:_collectionView didSelectItemAtIndexPath:indexPath];
}

- (void)reloadData
{
    [_collectionView reloadData];
    [_collectionViewLayout invalidateLayout];
}

- (void)setDataSource:(id<HorizontalButtonsScrollDataSource>)dataSource
{
    _dataSource = dataSource;
    
    _collectionView.dataSource = self;
}

- (void)setDelegate:(id<HorizontalButtonsScrollDelegate>)delegate
{
    _delegate = delegate;
    
    _collectionView.delegate = self;
}

- (void)setFixedCellSize:(CGSize)fixedCellSize
{
    _fixedCellSize = fixedCellSize;
    
    [self reloadData];
}

- (CGSize)intrinsicContentSize
{
    return CGSizeMake(320, 48);
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.dataSource numberOfButtonsInHorizontalButtonsScroll:self];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    HorizontalButtonsScrollCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    [cell setTitle:[self.dataSource horizontalButtonsScroll:self titleForButtonAtIndex:indexPath.row]
          subtitle:[self.dataSource horizontalButtonsScroll:self subtitleForButtonAtIndex:indexPath.row]];

    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (CGSizeEqualToSize(self.fixedCellSize, CGSizeZero)) {
        CGSize size = [HorizontalButtonsScrollCell cellSizeForTitle:[self.dataSource horizontalButtonsScroll:self titleForButtonAtIndex:indexPath.row]
                                                           subtitle:[self.dataSource horizontalButtonsScroll:self subtitleForButtonAtIndex:indexPath.row]];
        return size;
    }
    
    return self.fixedCellSize;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    
    _selectedIndex = indexPath.row;

    [self.delegate horizontalButtonsScroll:self didSelectButtonAtIndex:indexPath.row];
}

- (void) scrollToIndex:(NSInteger) index {
    [_collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0] atScrollPosition:UICollectionViewScrollPositionLeft animated:false];
}

@end
