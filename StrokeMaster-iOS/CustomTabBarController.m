//
//  CustomTabBarController.m
//  AA2000
//


#import "CustomTabBarController.h"
#import "PureLayout.h"

@interface CustomTabBarController () <HorizontalButtonsScrollDataSource, HorizontalButtonsScrollDelegate>

@end

@implementation CustomTabBarController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
        
        _selectedIndex = 0;
    }
    return self;
}

- (void)loadView
{
    UIView *view = [[UIView alloc] init];
    self.view = view;
    
    // Si no agregamos esta vista, por algún motivo no anda el HorizontalButtonsScroll
    UIView *tabButtonsContainerView = [[UIView alloc] init];
	[view addSubview:tabButtonsContainerView];
    
    _horizontalButtonsScroll = [[HorizontalButtonsScroll alloc] initForAutoLayout];
    self.horizontalButtonsScroll.dataSource = self;
    self.horizontalButtonsScroll.delegate = self;
    self.horizontalButtonsScroll.colorRGB = self.colorRGB;
    
    self.horizontalButtonsScroll.layer.shadowOpacity = 1.0;
    self.horizontalButtonsScroll.layer.shadowRadius = 1.0;
    self.horizontalButtonsScroll.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.horizontalButtonsScroll.layer.shadowOffset = CGSizeMake(0.0, 1.0);
    
    [view addSubview:self.horizontalButtonsScroll];
    [self.horizontalButtonsScroll autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:0];
    [self.horizontalButtonsScroll autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:0];
    [self.horizontalButtonsScroll autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
    [self.horizontalButtonsScroll autoSetDimension:ALDimensionHeight toSize:49];

    self.pageViewController.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundblack"]];
    
    
    [self addChildViewController:self.pageViewController];
    self.pageViewController.view.translatesAutoresizingMaskIntoConstraints = NO;
    [view addSubview:self.pageViewController.view];
    [self.pageViewController.view autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self.horizontalButtonsScroll withOffset:0];
    [self.pageViewController.view autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:0];
    [self.pageViewController.view autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
    [self.pageViewController.view autoPinToTopLayoutGuideOfViewController:self withInset:0];
    [_pageViewController didMoveToParentViewController:self];
    
    [self.view bringSubviewToFront:self.horizontalButtonsScroll];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor clearColor];
    
    if (self.viewControllers) {
        [self updateHorizontalButtonsScroll];
        self.selectedIndex = 0;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.viewControllers) {
        [self updateHorizontalButtonsScroll];
//        self.selectedIndex = 0;
        
        if (!self.selectedIndex){
            self.selectedIndex = 0;
        }
        [self setSelectedIndex:self.selectedIndex animated:YES];
    }
}

- (void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.horizontalButtonsScroll reloadData];
}

- (void)setViewControllers:(NSArray *)viewControllers
{
    _viewControllers = viewControllers;
    
//    self.selectedIndex = 0;
    
    self.buttonsWidth = _buttonsWidth;
    
    if (self.horizontalButtonsScroll) {
        [self updateHorizontalButtonsScroll];
    }
}

- (void)updateHorizontalButtonsScroll
{
    self.horizontalButtonsScroll.dataSource = self;
    self.horizontalButtonsScroll.delegate = self;
    
    if (self.buttonsWidth == 0) {
        self.horizontalButtonsScroll.fixedCellSize = CGSizeZero;
    } else {
        self.horizontalButtonsScroll.fixedCellSize = CGSizeMake(self.buttonsWidth, self.horizontalButtonsScroll.intrinsicContentSize.height);
    }
    
    [self.horizontalButtonsScroll reloadData];
}

- (void)setSelectedIndex:(NSInteger)selectedIndex
{
    [self setSelectedIndex:selectedIndex animated:NO];
}

- (void)setSelectedIndex:(NSInteger)selectedIndex animated:(BOOL)animated
{
    [self.horizontalButtonsScroll selectButtonWithIndex:selectedIndex];
    
    if (selectedIndex == 3) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                 delegate:(id)self.parentViewController
                                                        cancelButtonTitle:@"Cancel"
                                                   destructiveButtonTitle:@"Delete Round"
                                                        otherButtonTitles:nil];
        
        [actionSheet addButtonWithTitle:@"Pause Round"];
        [actionSheet addButtonWithTitle:@"End Round"];
        [actionSheet showInView:self.view];
        return;
    }
    
    UIPageViewControllerNavigationDirection direction = _selectedIndex < selectedIndex ? UIPageViewControllerNavigationDirectionForward : UIPageViewControllerNavigationDirectionReverse;
    
    self.horizontalButtonsScroll.userInteractionEnabled = NO;
    __typeof__(self) __weak weakSelf = self;
    [self.pageViewController setViewControllers:@[self.viewControllers[selectedIndex]] direction:direction animated:_selectedIndex == selectedIndex ? NO : animated completion:^(BOOL finished){
        weakSelf.horizontalButtonsScroll.userInteractionEnabled = YES;
    }];
    
    _selectedIndex = selectedIndex;
}

- (void)setButtonsWidth:(CGFloat)buttonsWidth
{
    _buttonsWidth = buttonsWidth;
    
    [self updateHorizontalButtonsScroll];
}

#pragma mark - Horizontal buttons scroll data source

- (NSInteger)numberOfButtonsInHorizontalButtonsScroll:(HorizontalButtonsScroll *)horizontalButtonsScroll
{
    return self.viewControllers.count;
}

- (NSString *)horizontalButtonsScroll:(HorizontalButtonsScroll *)horizontalButtonsScroll titleForButtonAtIndex:(NSInteger)index
{
    NSString *string = [self.viewControllers[index] title];
    return string;
}

- (NSString *)horizontalButtonsScroll:(HorizontalButtonsScroll *)horizontalButtonsScroll subtitleForButtonAtIndex:(NSInteger)index
{
    return nil;
}

- (UIColor *)horizontalButtonsScroll:(HorizontalButtonsScroll *)horizontalButtonsScroll colorForButtonAtIndex:(NSInteger)index
{
    return [UIColor lightGrayColor];
}

#pragma mark - Horizontal buttons scroll delegate

- (void)horizontalButtonsScroll:(HorizontalButtonsScroll *)horizontalButtonsScroll didSelectButtonAtIndex:(NSInteger)index
{
    [self setSelectedIndex:index animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
