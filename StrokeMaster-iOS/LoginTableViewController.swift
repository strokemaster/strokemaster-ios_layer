//
//  LoginTableViewController.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/4/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class LoginTableViewController: GenericFormTableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.fillDataArray()
        self.setUpTableViewUI()
        
        footerView.leftGoBackButton()
        footerView.addForgotPasswordButton()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
