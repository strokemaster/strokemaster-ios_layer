//
//  FormTableViewCell.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/4/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class FormTableViewCell: UITableViewCell {
    
    let iconImageView = UIImageView()
    let separator = UIImageView(image: UIImage(named: "separator")!)
    let textField = UITextField()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = UITableViewCellSelectionStyle.None
        self.backgroundColor = kBlackTransparentColor
        
        iconImageView.contentMode = UIViewContentMode.ScaleAspectFit
        self.contentView.addSubview(iconImageView)
        
        iconImageView.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        iconImageView.autoPinEdgeToSuperviewEdge(ALEdge.Left, withInset: 25.0)
        iconImageView.autoSetDimension(ALDimension.Width, toSize: 21)
        
        self.contentView.addSubview(separator)
        
        separator.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        separator.autoPinEdge(ALEdge.Left, toEdge: ALEdge.Right, ofView: iconImageView, withOffset: 10.0)
        separator.autoSetDimension(ALDimension.Width, toSize: 1.0)
        
        textField.transparentTextField()
        self.contentView.addSubview(textField)
        
        textField.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsMake(0, 0, 0, 0), excludingEdge: ALEdge.Left)
        textField.autoPinEdge(ALEdge.Left, toEdge: ALEdge.Right, ofView: separator, withOffset: 10.0)
    }
}
