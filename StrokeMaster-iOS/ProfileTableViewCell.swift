//
//  ProfileTableViewCell.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/12/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class ProfileTableViewCell: GenericIconLabelTableViewCell {
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    
        iconImageView.circularImageViewWithWidth(80.0)
        iconImageView.drawBordersWithColorAndWidth(UIColor.whiteColor(), width: 3.0)
        iconImageView.image = UIImage(named: "golfer")
        
        iconImageView.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        iconImageView.autoPinEdgeToSuperviewEdge(ALEdge.Left, withInset: 30.0)
        
        //label.text = "Robert Downey Jr."
        let userdefaults = NSUserDefaults.standardUserDefaults()
        label.text = userdefaults.objectForKey("Email") as? String
        label.textColor = UIColor.whiteColor()
        label.font = UIFont(name: "\(kDefaultFont)-Light", size: 19.0)
        
        label.autoAlignAxis(ALAxis.Horizontal, toSameAxisOfView: self.contentView, withOffset: -10.0)
        label.autoPinEdge(ALEdge.Left, toEdge: ALEdge.Right, ofView: iconImageView, withOffset: 15.0)
        
        let mailLabel = UILabel()
        mailLabel.text = "robert.dj@gmail.com"
        mailLabel.textColor = UIColor.whiteColor()
        mailLabel.font = UIFont(name: "\(kDefaultFont)-MediumItalic", size: 14.0)
        self.contentView.addSubview(mailLabel)
        
        mailLabel.autoAlignAxis(ALAxis.Horizontal, toSameAxisOfView: self.contentView, withOffset: 10.0)
        mailLabel.autoPinEdge(ALEdge.Left, toEdge: ALEdge.Left, ofView: label)
    
    }
}
