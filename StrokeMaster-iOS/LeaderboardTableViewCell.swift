//
//  LeaderboardTableViewCell.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 6/17/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class LeaderboardTableViewCell: UITableViewCell {
    
    let rankLabel = UILabel()
    let playerLabel = UILabel()
    let scoreLabel = UILabel()
    let roundsLabel = UILabel()
    
    var labelsArray: Array<UILabel>!

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.clearColor()
        self.selectionStyle = .None
        
        labelsArray = [rankLabel, playerLabel, scoreLabel, roundsLabel]
        
        for label in labelsArray {
            label.textColor = UIColor.whiteColor()
            label.font = UIFont(name: "\(kDefaultFont)-Roman", size: 16.0)
            self.contentView.addSubview(label)
            label.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        }
        
        rankLabel.autoAlignAxis(ALAxis.Vertical, toSameAxisOfView: self.contentView, withOffset: -SCREEN_WIDTH/2 + 40.0)
        playerLabel.autoAlignAxis(ALAxis.Vertical, toSameAxisOfView: self.contentView, withOffset: -SCREEN_WIDTH/7)
        scoreLabel.autoAlignAxis(ALAxis.Vertical, toSameAxisOfView: self.contentView, withOffset: SCREEN_WIDTH/7)
        roundsLabel.autoAlignAxis(ALAxis.Vertical, toSameAxisOfView: self.contentView, withOffset: SCREEN_WIDTH/2 - 40.0)
        
    }
    
    //Use the same style of cell as a header
    func setUpAsHeader() {
        for label in labelsArray {
            label.font = UIFont(name: "\(kDefaultFont)-Bold", size: 17.0)
            label.textColor = kBrightGreenColor
        }
        rankLabel.text = "Rank"
        playerLabel.text = "Player"
        scoreLabel.text = "Avg. Score"
        roundsLabel.text = "Rounds"
        
        let separationView = UIView()
        separationView.backgroundColor = UIColor.whiteColor()
        
        self.addSubview(separationView)
        
        separationView.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsZero, excludingEdge: ALEdge.Top)
        separationView.autoSetDimension(ALDimension.Height, toSize: 1.0)
    }

}
