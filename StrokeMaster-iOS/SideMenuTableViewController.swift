//
//  SideMenuTableViewController.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/5/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

protocol SideMenuDelegate {
    func bagPressed()
}

class SideMenuTableViewController: UITableViewController, LogoutViewDelegate {
    
    var itemArray: NSArray!
    var delegate: SideMenuDelegate!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let path = NSBundle.mainBundle().pathForResource("SideMenuList", ofType: "plist") {
            itemArray = NSArray(contentsOfFile: path)!
        }
        
        self.view.backgroundColor = UIColor.blackColor()
        self.tableView.tableFooterView = UIView()
        self.tableView.tableHeaderView = SideMenuTableHeaderView(frame: CGRectMake(0, 0, SCREEN_WIDTH, 150))
        self.tableView.bounces = false
        self.tableView.rowHeight = 44.0
        
        self.addLogoutView()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemArray.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("cell") as? SideMenuTableViewCell
        if cell == nil {
            cell = SideMenuTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        }

        cell?.label.text = itemArray[indexPath.row]["title"] as? String
        cell?.iconImageView.image = UIImage(named: itemArray[indexPath.row]["image"] as! String)
        
        return cell!
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 1 {
            self.delegate.bagPressed()
        }
        
        if indexPath.row == 3 {
            self.evo_drawerController?.closeDrawerAnimated(true, completion: nil)
            self.evo_drawerController?.centerViewController?.presentViewController(WalkthroughViewController(), animated: true, completion: nil)
        }
    }

    func addLogoutView() {
        
        let logoutView = SideMenuTableFooterView(frame: CGRectMake(0, SCREEN_HEIGHT - 60.0, SCREEN_WIDTH, 60.0))
        logoutView.delegate = self
        self.view.addSubview(logoutView)
        
    }
    
    //MARK: Logout Delegate
    func logoutPresed() {
        
        //Remove current user \
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setBool(false, forKey: "userLogged")
        userDefaults.setValue(nil, forKey: "currentUserID")
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        //Animation Fix
        appDelegate.window?.rootViewController = appDelegate.navigationController
        appDelegate.window?.rootViewController = self.evo_drawerController
        
        UIView.transitionWithView(appDelegate.window!,
            duration: 0.5,
            options: UIViewAnimationOptions.TransitionCrossDissolve,
            animations: {
                appDelegate.window!.rootViewController = appDelegate.navigationController
            },
            completion: nil)
    }
}
