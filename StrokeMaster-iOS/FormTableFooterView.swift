//
//  FormTableFooterView.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/4/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

protocol FormTableFooterViewDelegate {
    func loginPressed()
    func backPressed()
}

class FormTableFooterView: UIView {
    
    let button = UIButton()
    let backButton = UIButton()
    let forgotPasswordButton = UIButton()
    var delegate: FormTableFooterViewDelegate!

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        button.transparentButtonWithTitle("LOG IN")
        button.drawBordersWithColorAndWidth(kGreenColor, width: 1.0)
        button.addTarget(self, action: "loginPressed", forControlEvents: UIControlEvents.TouchUpInside)
        self.addSubview(button)
        
        button.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        button.autoSetDimension(ALDimension.Width, toSize: SCREEN_WIDTH + 2.0)
        button.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        
        backButton.setTitle("Go back", forState: UIControlState.Normal)
        backButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        backButton.titleLabel?.font = UIFont(name: "\(kDefaultFont)-Light", size: 14.0)
        backButton.addTarget(self, action: "backPressed", forControlEvents: UIControlEvents.TouchUpInside)
        self.addSubview(backButton)
        
    }
    
    //Create different layouts for footers
    func leftGoBackButton() {
        backButton.autoPinEdgeToSuperviewEdge(ALEdge.Left, withInset: 15.0)
        backButton.autoPinEdge(ALEdge.Top, toEdge: ALEdge.Bottom, ofView: button, withOffset: 15.0)
    }
    
    func centeredGoBackButton() {
        backButton.autoPinEdge(ALEdge.Top, toEdge: ALEdge.Bottom, ofView: button, withOffset: 15.0)
        backButton.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
    }
    
    func addForgotPasswordButton() {
        forgotPasswordButton.setTitle("Forgot password?", forState: UIControlState.Normal)
        forgotPasswordButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        forgotPasswordButton.titleLabel?.font = UIFont(name: "\(kDefaultFont)-Light", size: 14.0)
        self.addSubview(forgotPasswordButton)
        
        forgotPasswordButton.autoPinEdgeToSuperviewEdge(ALEdge.Right, withInset: 15.0)
        forgotPasswordButton.autoAlignAxis(ALAxis.Horizontal, toSameAxisOfView: backButton)
    }
    
    func addMailLabel() {
        let mailLabel = UILabel()
        mailLabel.text = "email is not visible to anyone"
        mailLabel.font = UIFont(name: "\(kDefaultFont)-LightItalic", size: 12.0)
        mailLabel.textColor = UIColor.whiteColor()
        
        self.addSubview(mailLabel)
        
        mailLabel.autoPinEdgeToSuperviewEdge(ALEdge.Top, withInset: 5.0)
        mailLabel.autoPinEdgeToSuperviewEdge(ALEdge.Left, withInset: 25.0)
    }
    
    func loginPressed() {
        self.delegate.loginPressed()
    }
    
    func backPressed() {
        self.delegate.backPressed()
    }
}
