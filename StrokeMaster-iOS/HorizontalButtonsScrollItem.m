//
//  HorizontalButtonsScrollItem.m
//  Vorterix
//

#import "HorizontalButtonsScrollItem.h"

@implementation HorizontalButtonsScrollItem

- (id)initWithID:(int)itemID title:(NSString *)title subtitle:(NSString *)subtitle
{
    self = [super init];
    if (self) {
        _itemID = itemID;
        _title = title;
        _subtitle = subtitle;
    }
    return self;
}

@end
