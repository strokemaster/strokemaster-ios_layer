//
//  CourseSectionHeaderView.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/8/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class CourseSectionHeaderView: UIView {

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.clearColor()
    
        //FIX-ME
        //COURSES CLOSE, MAP ICON, ARROW
    
    
    }
}
