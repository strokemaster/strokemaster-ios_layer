//
//  WalkthroughViewController.swift
//  Hangify
//
//  Created by Marcos Griselli on 6/20/15.
//  Copyright (c) 2015 Hangify. All rights reserved.
//

import UIKit

class WalkthroughViewController: UIViewController, WalkthroughDelegate {

    override func loadView() {
        super.loadView()
        let walkthroughtView = WalkthroughView()
        walkthroughtView.delegate = self
        self.view = walkthroughtView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func donePressed() {
        let userDefault = NSUserDefaults.standardUserDefaults()
        userDefault.setBool(true, forKey: "hasSeenIntro")
        userDefault.synchronize()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
