//
//  HoleData.swift
//  StrokeMaster-iOS
//
//  Created by Greg Morano on 8/20/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class HoleData: NSObject {
    
    var startPoint: [String]!
    var endPoint: [String]!
    var startDist: [Int]!
    var endDist: [Int]!
    var yardage: [Int]!
    var terrain: [String]!
    var clubs: [String]!
    var par: Int!
    

    
    init(let startPoint: [String], endPoint: [String], startDist: [Int], endDist: [Int], yardage: [Int], terrain: [String], clubs: [String], par: Int){
        self.startPoint = startPoint
        self.endDist = endDist
        self.endPoint = endPoint
        self.startDist = startDist
        self.yardage = yardage
        self.terrain = terrain
        self.clubs = clubs
        self.par = par
    }
    
    override init(){
        self.startPoint = [String]()
        self.endPoint = [String]()
        self.startDist = [Int]()
        self.endDist = [Int]()
        self.yardage = [Int]()
        self.terrain = [String]()
        self.clubs = [String]()
        self.par = 4
        super.init()
    }
    
    
    //Getters and Setters
    

    
}
