//
//  Line.swift
//  StrokeMaster-iOS
//
//  Created by Greg Morano on 2/27/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class Line{
    var start: CGPoint
    var end: CGPoint
    
    init(start _start: CGPoint, end _end: CGPoint){
        start  = _start
        end = _end
    }
}