//
//  SMClub.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/21/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

enum Club: Int {
    
    case Wood = 0,
    Hybrid,
    Iron,
    Wedge,
    Putter
    
    func description () -> String {
        switch self {
        case .Wood:
            return "Wood"
        case .Iron:
            return "Iron"
        case .Hybrid:
            return "Hybrid"
        case .Wedge:
            return "Wedge"
        case .Putter:
            return "Putter"
        }
    }
}

class SMClub: NSObject {
    
    let type: Club!
    let number: NSInteger?
    let name: String?
    var clubNumber: Int?
   
    override var description: String {
        return "Type: \(type), Number: \(number)"
    }
    
    var shortDescription: String {
        if let clubNumber = number {
            return "\(clubNumber) \(type.description())"
        } else {
            return "\(type.description())"
        }

    }
    
    init(type: Club!, number: NSInteger?, name: String?) {
        self.type = type
        self.number = number
        self.name = name
        self.clubNumber = nil
    }
    
    init(type: Club!, number: NSInteger?, name: String?, clubNumber: Int?) {
        self.type = type
        self.number = number
        self.name = name
        self.clubNumber = clubNumber
        super.init()
    }
}
