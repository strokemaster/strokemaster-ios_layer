//
//  FormTableHeaderView.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/4/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class FormTableHeaderView: UIView {
    
    let title = UILabel()
    let errorMsg = UILabel()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        title.text = "Login to Strokemaster"
        title.textColor = UIColor.whiteColor()
        title.font = UIFont(name: "\(kDefaultFont)-Light", size: 22.0)
        
        self.addSubview(title)
        
        title.autoAlignAxis(ALAxis.Horizontal, toSameAxisOfView: self)
        title.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        
        errorMsg.text = ""
        errorMsg.textColor = UIColor.redColor()
        errorMsg.font = UIFont(name: "\(kDefaultFont)-Light", size: 18.0)
        
        self.addSubview(errorMsg)
        
        errorMsg.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        errorMsg.autoPinEdge(ALEdge.Top, toEdge: ALEdge.Bottom, ofView: title, withOffset: 10)
    }

    func addError(text: String) {
        let step = UILabel()
        step.text = text
        step.textColor = UIColor.redColor()
        
        self.addSubview(step)
        
        step.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        step.autoPinEdge(ALEdge.Top, toEdge: ALEdge.Bottom, ofView: title, withOffset: 10)
    }
    
    func setErrorMsg(error: String) {
        errorMsg.text = error
    }
    
    func clearErrorMsg(){
        errorMsg.text = ""
    }
}
