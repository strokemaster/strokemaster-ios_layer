//
//  CourseScorecard.swift
//  StrokeMaster-iOS
//
//  Created by Greg Morano on 6/13/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class CourseScorecard: NSObject {
    
    let courseId: String!
    let courseName: String!
    let mensHandicap: [Int]!
    let mensPar: [Int]!
    let mensParIn: Int!
    let mensParOut: Int!
    let mensParTotal: Int!
    let ladiesHandicap: [Int]!
    let ladiesPar: [Int]!
    let ladiesParIn: Int!
    let ladiesParOut: Int!
    let ladiesParTotal: Int!
    
    
    init(json :JSON) {
        self.courseId = json["courseId"].string
        self.courseName = json["courseName"].string
        self.mensHandicap = DataManager.toIntArray(json, str: "mensHandicap")
        self.mensPar = DataManager.toIntArray( json, str: "mensPar" )
        self.mensParIn = json["mensParIn"].int
        self.mensParOut = json["mensParOut"].int
        self.mensParTotal = json["mensParTotal"].int
        self.ladiesHandicap = DataManager.toIntArray( json, str: "ladiesHandicap")
        self.ladiesPar = DataManager.toIntArray( json, str: "ladiesPar")
        self.ladiesParIn = json["ladiesParIn"].int
        self.ladiesParOut = json["ladiesParOut"].int
        self.ladiesParTotal = json["ladiesParTotal"].int
        
        
        super.init()
    }
    
    
    
    
}