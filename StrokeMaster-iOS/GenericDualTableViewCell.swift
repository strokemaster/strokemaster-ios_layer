//
//  GenericDualTableViewCell.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/8/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class GenericDualTableViewCell: UITableViewCell {
    
    let firstButton = UIButton()
    let secondButton = UIButton()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.clearColor()
        self.selectionStyle = .None
        
        firstButton.transparentButton("Slope\n118")
        firstButton.tag = 0
        firstButton.addTarget(self, action: "buttonPressed:", forControlEvents: UIControlEvents.TouchUpInside)
        self.addSubview(firstButton)
        
        firstButton.autoPinEdgeToSuperviewEdge(ALEdge.Bottom, withInset: 1.0)
        firstButton.autoPinEdgeToSuperviewEdge(ALEdge.Left)
        firstButton.autoPinEdgeToSuperviewEdge(ALEdge.Top, withInset: 1.0)
        firstButton.autoSetDimension(ALDimension.Width, toSize: SCREEN_WIDTH/2 - 1)
        
        secondButton.transparentButton("Holes\n18")
        secondButton.tag = 0
        secondButton.addTarget(self, action: "buttonPressed:", forControlEvents: UIControlEvents.TouchUpInside)
        self.addSubview(secondButton)
        
        secondButton.autoPinEdgeToSuperviewEdge(ALEdge.Bottom, withInset: 1.0)
        secondButton.autoPinEdgeToSuperviewEdge(ALEdge.Right)
        secondButton.autoPinEdgeToSuperviewEdge(ALEdge.Top, withInset: 1.0)
        secondButton.autoSetDimension(ALDimension.Width, toSize: SCREEN_WIDTH/2 - 1)
        
    }
    
    

}
