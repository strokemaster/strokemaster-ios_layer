//
//  VectorViewController.swift
//  StrokeMaster-iOS
//
//  Created by Greg Morano on 3/12/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class VectorViewController: UIViewController, DrawVectorDelegate{
    
//    let f = DrawVectorView(frame: CGRectMake(0, 0, 50, 50))
    
    override func loadView() {
        super.loadView()
        let vectorView = DrawVectorView()
        vectorView.drawVectordelegate = self
        self.view = vectorView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
  
    func getPoints() -> [Line]{
        
        var points = [Line]()
        DataManager.getTopAppsDataFromIGolfWithSuccess { (iGolfData) -> Void in
            let json = JSON(data: iGolfData)
            let green = "green"
            points =  DataManager.pointsToVectorLines(green, json: json)
            //println(json["green"][0])  //prints the first x,y coordinate for the green
            if let appName = json["green"].stringValue as String? {
                //if let appName = json["feed"]["entry"][0]["im:name"]["label"].stringValue as String? {
                print("NSURLSession: \(appName)")
            }
        }
        return points
    }
    
    func continuePressed() {
        self.navigationController?.customPush(RegisterFormTableViewController())
    }
}