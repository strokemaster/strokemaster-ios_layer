//
//  SMCourse.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 6/6/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class SMCourse: NSObject {
    
    let name: String!
    let holesCount: NSInteger!
    let par: NSInteger!
    let slope: NSInteger!
    
    
    init(name: String!, holesCount: NSInteger!, par: NSInteger!, slope: NSInteger!) {
        self.name = name
        self.holesCount = holesCount
        self.par = par
        self.slope = slope
        super.init()
    }
}
