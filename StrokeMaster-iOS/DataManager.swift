//
//  DataManager.swift
//  StrokeMaster-iOS
//
//  Created by Greg Morano on 3/12/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

let serverURL = "http://ec2-52-24-220-119.us-west-2.compute.amazonaws.com:9000/"
let testURL = "http://localhost:9000/"
//let TopAppURL = "https://itunes.apple.com/us/rss/topgrossingipadapplications/limit=25/json"
//let iGolfUrl = "https://localhost:9443/vector-data"


class DataManager {
    
    //webservice call to get vector data for specific course
    class func getTopAppsDataFromIGolfWithSuccess(success: ((iGolfData: NSData!) -> Void)) {
        let iGolfUrl: String = "http://localhost:9000/vector-data"
        //var iGolfUrl: String  = "http://ec2-52-24-220-119.us-west-2.compute.amazonaws.com:9000/vector-data"
        loadDataFromURL(NSURL(string: iGolfUrl)!, completion:{(data, error) -> Void in
            //If data was retrieved successfully
            if let urlData = data {
                success(iGolfData: urlData)
            }
        })
    }
    
    class func getVectorDataForCourseWithID(courseId: String, success: ((iGolfData: NSData!) -> Void)) {
        //var iGolfUrl: String = "http://localhost:9000/vector-data"
        
        //Replace courseID
        let iGolfUrl = "\(serverURL)vector-data?id=\(courseId)"
        loadDataFromURL(NSURL(string: iGolfUrl)!, completion:{(data, error) -> Void in
            //If data was retrieved successfully
            if let urlData = data {
                success(iGolfData: urlData)
            }
        })
    }
    
    //tries to login with user name and password
    class func tryLogin(username: String, password: String, success: ((loginResult: NSData!) -> Void)) {
        
        //var loginUrl: String = "https://localhost:9443/login?"
        //var loginUrl: String = "http://localhost:9000/login?"
        var loginUrl: String = "http://ec2-52-24-220-119.us-west-2.compute.amazonaws.com:9000/login?"
        loginUrl += "email=" + username + "&pass=" + password
            
        loadDataFromURL(NSURL(string: loginUrl)!, completion:{(data, error) -> Void in
            //If data was retrieved successfully
            if let urlData = data {
                success(loginResult: urlData)
            }
        })
        
    }
    
    //tries to create user with username and password
    class func createUser(username: String, password: String, success: ((loginResult: NSData!) -> Void)) {
        
        //var loginUrl: String = "https://localhost:9443/create-user?"
        //var loginUrl: String = "http://localhost:9000/create-user?"
        var loginUrl: String = "http://ec2-52-24-220-119.us-west-2.compute.amazonaws.com:9000/create-user?"
        loginUrl += "email=" + username + "&pass=" + password
        
        loadDataFromURL(NSURL(string: loginUrl)!, completion:{(data, error) -> Void in
            //If data was retrieved successfully
            if let urlData = data {
                success(loginResult: urlData)
            }
        })
    }
    
    //gets the closest courses
    class func getCourseList(lon: Double, lat: Double, success: ((loginResult: NSData!) -> Void)) {
        
        //var loginUrl: String = "https://localhost:9443/course-list?"
        //var loginUrl: String = "http://localhost:9000/course-list?"
        var loginUrl: String = "http://ec2-52-24-220-119.us-west-2.compute.amazonaws.com:9000/course-list?"
        loginUrl += "lon=" + String(format:"%f", lon) + "&lat=" + String(format:"%f", lat)
        
        loadDataFromURL(NSURL(string: loginUrl)!, completion:{(data, error) -> Void in
            //If data was retrieved successfully
            if let urlData = data {
                success(loginResult: urlData)
            }
        })
    }
    
    //gets the closest courses scorecards
    class func getCourseScorecardList(courseIds: [String], success: ((loginResult: NSData!) -> Void)) {
        
        //var loginUrl: String = "https://localhost:9443/course-scorecard-list?"
        //var loginUrl: String = "http://localhost:9000/course-scorecard-list?"
        var loginUrl: String = "http://ec2-52-24-220-119.us-west-2.compute.amazonaws.com:9000/course-scorecard-list?"
        loginUrl += "courses=["
        for str in courseIds{
            loginUrl += str + ","
        }
        loginUrl = loginUrl.substringToIndex(loginUrl.endIndex.predecessor())
        loginUrl += "]"
        loadDataFromURL(NSURL(string: loginUrl)!, completion:{(data, error) -> Void in
            //If data was retrieved successfully
            if let urlData = data {
                success(loginResult: urlData)
            }
        })
    }
    
    
    //gets the given courses tee details
    class func getCourseTeeDetails(courseId: String, success: ((loginResult: NSData!) -> Void)) {
        
        //var loginUrl: String = "https://localhost:9443/course-tee?"
        //var loginUrl: String = "http://localhost:9000/course-tee?"
        var loginUrl: String = "http://ec2-52-24-220-119.us-west-2.compute.amazonaws.com:9000/course-tee?"
        loginUrl += "id=" + courseId
        
        loadDataFromURL(NSURL(string: loginUrl)!, completion:{(data, error) -> Void in
            //If data was retrieved successfully
            if let urlData = data {
                success(loginResult: urlData)
            }
        })
    }
    
    //test function for getting json data from a file.  Not used
    class func getTopAppsDataFromFileWithSuccess(success: ((data: NSData) -> Void)) {
        //1
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            //2
            let filePath = NSBundle.mainBundle().pathForResource("TopApps",ofType:"json")
            
            var readError:NSError?
            do {
                let data = try NSData(contentsOfFile:filePath!,
                    options: NSDataReadingOptions.DataReadingUncached)
                    success(data: data)
            } catch let error as NSError {
                readError = error
                print(readError?.description)
            } catch {
                fatalError()
            }
        })
    }
    
    //create an http request and call the given url web service
    class func loadDataFromURL(url: NSURL, completion:(data: NSData?, error: NSError?) -> Void) {
        let session = NSURLSession.sharedSession()
        
        // Use NSURLSession to get data from an NSURL
        let loadDataTask = session.dataTaskWithURL(url, completionHandler: { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            if let responseError = error {
                completion(data: nil, error: responseError)
            } else if let httpResponse = response as? NSHTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    let statusError = NSError(domain:"com.strokemaster", code:httpResponse.statusCode, userInfo:[NSLocalizedDescriptionKey : "HTTP status code has unexpected value."])
                    completion(data: nil, error: statusError)
                } else {
                    completion(data: data, error: nil)
                }
            }
        })
        
        loadDataTask.resume()
    }
    
    //converts json of point data to a vector of lines to draw
    class func pointsToVectorLines(courseObject: String, json: JSON) -> [Line] {
        var count = 0
        var points = [CGPoint]()
        var lines  = [Line]()
        
        //parse the json into an array of CGPOints
        while let _ = json[courseObject][count]["x"].int {
            //println("Point " + String(count))
            //println(json[courseObject][count])
            let point: CGPoint = CGPoint(x: abs(json[courseObject][count]["x"].doubleValue) , y: abs(json[courseObject][count]["y"].doubleValue))
            points.append(point)
            count++
        }
        
        let size  = points.count
        for( var i = 0; i < (size-1); i++){
            let line: Line = Line(start: points[i], end: points[i+1])
            lines.append(line)
        }
        if(size > 0){
            lines.append(Line(start: points[size-1], end: points[0]))
        }
        
        return lines
    }
    
    /*
    *Iterates through multiple shapes for a specific course object.  Then adds them to a dictionary to be later drawn.
    */
    class func pointsToMultiVectorLines(courseObject: String, json: JSON) -> Dictionary<Int, [Line]> {
        var objects = Dictionary<Int, [Line]>()
        
        //iterate through each shape
        for(var i = 0; i < json[courseObject].count; i++){
            var points = [CGPoint]()
            var lines  = [Line]()
            var count = 0
            
            //iterate through each point for this shape then creates lines from them
            while let _ = json[courseObject][i.description][count]["x"].int {

                let point: CGPoint = CGPoint(x: abs(json[courseObject][i.description][count]["x"].doubleValue) , y: abs(json[courseObject][i.description][count]["y"].doubleValue))
                points.append(point)
                count++
            }
            
            let size  = points.count
            for( var i = 0; i < (size-1); i++){
                let line: Line = Line(start: points[i], end: points[i+1])
                lines.append(line)
            }
            if(courseObject != "cartpath"){
                lines.append(Line(start: points[size-1], end: points[0]))
            }
            objects[i] = lines
        }
        
        return objects
    }
    
    
    /*
    *Iterates through multiple shapes for a specific course object.  Then adds them to a dictionary to be later drawn.
    */
    class func pointsToMultiPoints(courseObject: String, json: JSON) -> [CGPoint] {
        var points = [CGPoint]()
        
        //iterate through each shape
        for(var i = 0; i < json[courseObject].count; i++){
            let point: CGPoint = CGPoint(x: abs(json[courseObject][i.description]["x"].doubleValue) , y: abs(json[courseObject][i.description]["y"].doubleValue))
            points.append(point)
        }
        
        return points
    }
    
    
    
    /*
    *Converts an array of json to an array of int
    */
    class func toIntArray(arr: JSON, str: String) -> [Int] {
        
        var res = [Int]()
        for(var i = 0; i < arr[str].count; i++){
            res.append(arr[str][i].int!)
        }
        return res
    }
    
    
    /*
    *Converts a string to an array of int
    */
    class func stringToIntArray(var str: String) -> [Int] {
        
        //remove array brackets
        str = String(str.substringToIndex(str.endIndex.predecessor()))
        str = String(str.substringFromIndex(str.startIndex.successor()))
        
        var res = [Int]()
        let intArr = str.componentsSeparatedByString(",")
        
        for(var i = 0; i < intArr.count; i++){
            res.append(Int(intArr[i])!)
        }
        return res
    }
    
    
    
    /*
    *Converts a string to an array of int
    */
    class func stringToDoubleArray(let str: String) -> [Double] {
        
        
        var res = [Double]()
        let intArr = str.componentsSeparatedByString(",")
        
        for(var i = 0; i < intArr.count; i++){
            if(intArr[i] != ""){
                res.append(Double(intArr[i])!)
            }
        }
        return res
    }
    
    /*
    Sending data to backend
    */
    class func sendGolfBag(json:JSON, success: ((result: Int) -> Void)) {
    
        //declare parameter as a dictionary which contains string as key and value combination.
        //        var parameters = ["object": jsonArray] as Dictionary<String, JSON>
        
        //create the url with NSURL
        let url = NSURL(string: serverURL + "clubs") //Set the post URL as soon as Greg has it.
        var res:Int = -1
        //create the session object
        let session = NSURLSession.sharedSession()
        
        //now create the NSMutableRequest object using the url object
        let request = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "POST" //set http method as POST
        
        var err: NSError?
        
        var xyz: NSData?
        do {
            xyz = try json[0].rawData(options: [])
        } catch let error as NSError {
            err = error
            print(err?.description)
            xyz = nil
        }
        //var xyz = NSJSONSerialization.dataWithJSONObject(dictionary, options: nil, error: &err) // pass dictionary to nsdata object and set it as request body
        request.HTTPBody = xyz
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        //create dataTask using the session object to send data to the server
        let semaphore = dispatch_semaphore_create(0)
        print(json)
        let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            print("Response: \(response)")
            let strData = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("Body: \(strData)")

            do{
                let json = try NSJSONSerialization.JSONObjectWithData(data!, options: .MutableLeaves) as! NSDictionary
                
                // The JSONObjectWithData constructor didn't return an error. But, we should still
                // check and make sure that json has a value using optional binding.
                res = json["Success"] as! Int
                print("Succes: \(res)")
                
                
            } catch let myJSONError {
                print(myJSONError)
                let jsonStr = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("Error could not parse JSON: '\(jsonStr)'")
            }
            

            dispatch_semaphore_signal(semaphore)
        })
        task.resume()
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)
        success(result: res)
    }
    
    
    
    class func sendCreateUser(json:JSON, success: ((result: Int) -> Void)) {
        
        //declare parameter as a dictionary which contains string as key and value combination.
        //        var parameters = ["object": jsonArray] as Dictionary<String, JSON>
        
        //create the url with NSURL
        let url = NSURL(string: serverURL + "create-user") //Set the post URL as soon as Greg has it.
        var res:Int = -1
        //create the session object
        let session = NSURLSession.sharedSession()
        
        //now create the NSMutableRequest object using the url object
        let request = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "POST" //set http method as POST
        
        var err: NSError?
        
        var xyz: NSData?
        do {
            xyz = try json[0].rawData(options: [])
        } catch let error as NSError {
            err = error
            print(err?.description)
            xyz = nil
        }
        //var xyz = NSJSONSerialization.dataWithJSONObject(dictionary, options: nil, error: &err) // pass dictionary to nsdata object and set it as request body
        request.HTTPBody = xyz
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        //create dataTask using the session object to send data to the server
        let semaphore = dispatch_semaphore_create(0)
        print(json)
        let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            print("Response: \(response)")
            let strData = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("Body: \(strData)")
            do{
                let json = try NSJSONSerialization.JSONObjectWithData(data!, options: .MutableLeaves) as! NSDictionary
                
                // The JSONObjectWithData constructor didn't return an error. But, we should still
                // check and make sure that json has a value using optional binding.
                res = json["Success"] as! Int
                print("Succes: \(res)")
                
                
            } catch let myJSONError {
                print(myJSONError)
                let jsonStr = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("Error could not parse JSON: '\(jsonStr)'")
            }
            dispatch_semaphore_signal(semaphore)
        })
        task.resume()
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)
        success(result: res)
        
        
    }
    
    
    
    
    
    class func sendEndRound(json:String, success: ((result: Int) -> Void)) {
        
        //create the url with NSURL
        let url = NSURL(string: serverURL + "end-round") //Set the post URL as soon as Greg has it.
        var res:Int = -1
        //create the session object
        let session = NSURLSession.sharedSession()
        
        //now create the NSMutableRequest object using the url object
        let request = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "POST" //set http method as POST
        
        //var err: NSError?
        
        //var xyz = json[0].rawData(options: nil, error: &err)
        let xyz: NSData = json.dataUsingEncoding(NSUTF8StringEncoding)!
        //var xyz = NSJSONSerialization.dataWithJSONObject(data, options: NSJSONReadingOptions(0), error: &err) // pass dictionary to nsdata object and set it as request body
        request.HTTPBody = xyz
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        //create dataTask using the session object to send data to the server
        let semaphore = dispatch_semaphore_create(0)
        print(json)
        let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            print("Response: \(response)")
            let strData = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("Body: \(strData)")
            
            do{
                let json = try NSJSONSerialization.JSONObjectWithData(data!, options: .MutableLeaves) as! NSDictionary
                
                // The JSONObjectWithData constructor didn't return an error. But, we should still
                // check and make sure that json has a value using optional binding.
                res = json["Success"] as! Int
                print("Succes: \(res)")
                
                
            } catch let myJSONError {
                print(myJSONError)
                let jsonStr = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("Error could not parse JSON: '\(jsonStr)'")
            }
            dispatch_semaphore_signal(semaphore)
        })
        task.resume()
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)
        success(result: res)
        
        
    }
    
    
    //gets clubs list
    class func getClubsList(type: String, success: ((loginResult: NSData!) -> Void)) {
        
        //create the url with NSURL
        let url = NSURL(string: serverURL + "clubs?list=" + type)
        
        loadDataFromURL(url!, completion:{(data, error) -> Void in
            //If data was retrieved successfully
            if let urlData = data {
                success(loginResult: urlData)
            }
        })
    }
    
    //gets game summary
    class func getGameSummary(user: String, success: ((results: NSData!) -> Void)) {
        
        //create the url with NSURL
        let url = NSURL(string: serverURL + "gameSummary?userID=" + user)
        
        loadDataFromURL(url!, completion:{(data, error) -> Void in
            //If data was retrieved successfully
            if let urlData = data {
                success(results: urlData)
            }
        })
    }
    
    
    //gets user diagnostics
    class func getDiagnostics(user: String, success: ((results: NSData!) -> Void)) {
        
        //create the url with NSURL
        let url = NSURL(string: serverURL + "diagnosticStats?userID=" + user)
        
        loadDataFromURL(url!, completion:{(data, error) -> Void in
            //If data was retrieved successfully
            if let urlData = data {
                success(results: urlData)
            }
        })
    }
    
    
    //gets Leaderboard
    class func getLeaderboard(success: ((results: NSData!) -> Void)) {
        
        //create the url with NSURL
        let url = NSURL(string: serverURL + "leaderboard")
        
        loadDataFromURL(url!, completion:{(data, error) -> Void in
            //If data was retrieved successfully
            if let urlData = data {
                success(results: urlData)
            }
        })
    }
    
    
    //gets Users Club Stats
    class func getClubStats(user: String, success: ((results: NSData!) -> Void)) {
        
        //create the url with NSURL
        let url = NSURL(string: serverURL + "clubStats?userID=" + user)
        
        loadDataFromURL(url!, completion:{(data, error) -> Void in
            //If data was retrieved successfully
            if let urlData = data {
                success(results: urlData)
            }
        })
    }
    
    
}