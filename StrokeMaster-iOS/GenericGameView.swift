//
//  GenericGameView.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/16/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class InGameTopBox: UIView {
    
    //Top Boxes on Tracking View and GPS View
    let blackView = UIView()
    let titleLabel = UILabel()
    let informationLabel = UILabel()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        blackView.transparentBlackView()
        self.addSubview(blackView)
        blackView.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsZero)
        
        titleLabel.text = "title"
        titleLabel.textColor = kBrightGreenColor
        titleLabel.font = UIFont(name: "\(kDefaultFont)-Medium", size: 12.0)
        blackView.addSubview(titleLabel)
        
        titleLabel.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        titleLabel.autoPinEdgeToSuperviewEdge(ALEdge.Top, withInset: 2.0)
        
        informationLabel.textColor = UIColor.whiteColor()
        informationLabel.font = UIFont(name: "\(kDefaultFont)-Medium", size: 16.0)
        blackView.addSubview(informationLabel)
        
        informationLabel.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        informationLabel.autoPinEdgeToSuperviewEdge(ALEdge.Bottom, withInset: 5.0)
        informationLabel.autoPinEdge(ALEdge.Top, toEdge: ALEdge.Bottom, ofView: titleLabel, withOffset: 5.0)

    }
}

class GenericGameView: UIView {
    
    var yardsView: InGameTopBox!
    var parView: InGameTopBox!
    var handicapView: InGameTopBox!

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.clearColor()
        
        yardsView = InGameTopBox()
        yardsView.titleLabel.text = "YARDS"
        self.addSubview(yardsView)
        
        yardsView.autoPinEdgeToSuperviewEdge(ALEdge.Left)
        yardsView.autoPinEdgeToSuperviewEdge(ALEdge.Top, withInset: 5.0)
        yardsView.autoSetDimension(ALDimension.Width, toSize: SCREEN_WIDTH/3 - 1)
        
        parView = InGameTopBox()
        parView.titleLabel.text = "PAR"
        
        self.addSubview(parView)
        
        parView.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        parView.autoPinEdgeToSuperviewEdge(ALEdge.Top, withInset: 5.0)
        parView.autoSetDimension(ALDimension.Width, toSize: SCREEN_WIDTH/3 - 1)
        
        handicapView = InGameTopBox()
        handicapView.titleLabel.text = "HANDICAP"
        
        self.addSubview(handicapView)
        
        handicapView.autoPinEdgeToSuperviewEdge(ALEdge.Right)
        handicapView.autoPinEdgeToSuperviewEdge(ALEdge.Top, withInset: 5.0)
        handicapView.autoSetDimension(ALDimension.Width, toSize: SCREEN_WIDTH/3 - 1)
    }
    
    func fillInformationWithHole(hole: SMHole) {
        yardsView.informationLabel.text = "\(hole.yards)"
        parView.informationLabel.text = "\(hole.par)"
        handicapView.informationLabel.text = "\(hole.handicap)"
    }
}
