//
//  TrackingTableViewCell.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/16/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class TrackingTableViewCell: UITableViewCell {
    
    let strokeLabel = UILabel()
    let clubLabel = UILabel()
    let yardsLabel = UILabel()

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.clearColor()
        self.selectionStyle = .None
        
        strokeLabel.textColor = UIColor.whiteColor()
        self.contentView.addSubview(strokeLabel)
        
        strokeLabel.autoPinEdgeToSuperviewEdge(ALEdge.Left, withInset: 15.0)
        strokeLabel.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        
        clubLabel.textColor = UIColor.whiteColor()
        self.contentView.addSubview(clubLabel)
        
        clubLabel.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        clubLabel.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        
        yardsLabel.textColor = UIColor.whiteColor()
        self.contentView.addSubview(yardsLabel)
        
        yardsLabel.autoPinEdgeToSuperviewEdge(ALEdge.Right, withInset: 15.0)
        yardsLabel.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        
    
    }

}
