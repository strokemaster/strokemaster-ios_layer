//
//  ClubStatsTableViewController.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 6/26/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class ClubStatsTableViewController: UITableViewController {
    
    //let clubList = ["Driver", "3 Wood", "5 Wood", "3 Hybrid", "6 Hybrid ", "4 Iron", "8 Iron", "9 Iron", "Wedges", "Putter"]
    //let percentages = [76, 45, 87, 94, 65, 55, 76, 65, 89]
    //var framesInfo: NSArray!
    var json: JSON = nil
    
    init() {
        super.init(style: .Plain)
        let userdefaults = NSUserDefaults.standardUserDefaults()
        
        let semaphore = dispatch_semaphore_create(0)
        
        let email = userdefaults.objectForKey("Email") as! String
        
        DataManager.getClubStats(email, success:{(results) -> Void in
            let result = results
            self.json = JSON(data: result)
            
            dispatch_semaphore_signal(semaphore)
        })
        
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Strokemaster"
        
        
        //Load everyClub
        /*if let path = NSBundle.mainBundle().pathForResource("ClubStatsList", ofType: "plist") {
            framesInfo = NSArray(contentsOfFile: path)!
        }*/
        
        //TableUI
        self.view.backgroundColor = UIColor.clearColor()
        self.tableView.tableFooterView = UIView()
        self.tableView.tableHeaderView = createHeader()
        
        let backButton = UIBarButtonItem(image: UIImage(named: "backarrow"), style: UIBarButtonItemStyle.Plain, target: self, action: "backPressed")
        self.navigationItem.leftBarButtonItem = backButton

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return json.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("statsCell") as? ClubStatsTableViewCell
        if cell == nil {
            cell = ClubStatsTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "statsCell")
        }
        
        var club = json[indexPath.row]
        
        cell?.clubLabel.text = club["clubName"].string
        cell?.clubLabel.font = UIFont(name: "\(kDefaultFont)-Medium", size: 14.0)
        
        let first = club["min"].int!
        let second = club["avgDist"].double!
        let third = club["max"].int!
        
        cell?.rangeView.firstValue.text = "\(first)"
        cell?.rangeView.midValue.text = "\(second)"
        cell?.rangeView.finalValue.text = "\(third)"
        
//        cell?.percentajeLabel.textColor = setColorForScore(percentages[indexPath.row])
//        cell?.percentajeLabel.text = "\(percentages[indexPath.row])%"

        return cell!
    }
    
    func createHeader() -> UIView {
    
        let headerView = UIView(frame: CGRectMake(0, 0, SCREEN_WIDTH, 60))
        headerView.backgroundColor = kBlackTransparentColor
        
        let label = UILabel()
        label.textColor = .whiteColor()
        label.text = "Scores for Selected Club" //Add selected course
        headerView.addSubview(label)
        label.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        label.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        
        return headerView
    
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRectMake(0, 0, SCREEN_WIDTH, 40.0))
        
        let clubLabel = UILabel()
        clubLabel.textColor = .whiteColor()
        clubLabel.text = "Club"
        headerView.addSubview(clubLabel)
        clubLabel.autoPinEdgeToSuperviewEdge(ALEdge.Left, withInset: 15.0)
        clubLabel.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        
        let rangeLabel = UILabel()
        rangeLabel.textColor = .whiteColor()
        rangeLabel.text = "Range"
        headerView.addSubview(rangeLabel)
        rangeLabel.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        rangeLabel.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
     
//        let effectivityLabel = UILabel()
//        effectivityLabel.textColor = .whiteColor()
//        effectivityLabel.text = "Effectivity"
//        headerView.addSubview(effectivityLabel)
//        effectivityLabel.autoPinEdgeToSuperviewEdge(ALEdge.Right, withInset: 15.0)
//        effectivityLabel.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        
        let separatorView = UIView()
        separatorView.backgroundColor = UIColor.lightGrayColor()
        headerView.addSubview(separatorView)
        separatorView.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsZero, excludingEdge: ALEdge.Top)
        separatorView.autoSetDimension(ALDimension.Height, toSize: 0.5)
        
        return headerView
    }
    
    //Color check
    func setColorForScore(score: Int) -> UIColor {
        
        if score < 60 {
            return UIColor.redColor()
        } else if score < 70 {
            return UIColor.orangeColor()
        } else if score < 80 {
            return UIColor.yellowColor()
        }
        return score < 90 ? kscoreGreenColor : kscoreBlueColor
    }
    
    func backPressed() {
        self.navigationController?.customPop()
    }
}
