//
//  RegisterStepView.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/4/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

protocol RegisterStepsDelegate {
    func continuePressed()
}

class RegisterStepView: UIView {
    
    let title = UILabel()
    let text = UILabel()
    let bagButton = UIButton()
    //let continueButton = UIButton()
    var delegate: RegisterStepsDelegate!

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        title.text = "Welcome to Strokemaster!"
        title.textColor = UIColor.whiteColor()
        title.font = UIFont(name: "\(kDefaultFont)-Light", size: 22.0)
        self.addSubview(title)
        
        title.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        title.autoPinEdgeToSuperviewEdge(ALEdge.Top, withInset: 100.0)

        text.text = "Free GPS data thousands of courses tracking of all rounds calculated score information club distances accuracy and advanced stats"
        text.numberOfLines = 0
        text.textColor = UIColor.whiteColor()
        text.font = UIFont(name: "\(kDefaultFont)-LightItalic", size: 18.0)
        self.addSubview(text)
        
        text.autoAlignAxis(ALAxis.Horizontal, toSameAxisOfView: self, withOffset: -50.0)
        text.autoPinEdgeToSuperviewEdge(ALEdge.Left, withInset: 15.0)
        text.autoPinEdgeToSuperviewEdge(ALEdge.Right, withInset: 15.0)
        
        //Removing Continue Button
        /*continueButton.transparentButtonWithTitle("OK. Let's go!")
        continueButton.tag = 1
        continueButton.addTarget(self, action: "buttonPressed:", forControlEvents: UIControlEvents.TouchUpInside)
        continueButton.drawBordersWithColorAndWidth(kGreenColor, width: 1.0)
        self.addSubview(continueButton)
        
        continueButton.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        continueButton.autoPinEdgeToSuperviewEdge(ALEdge.Bottom, withInset: 100)
        continueButton.autoSetDimension(ALDimension.Width, toSize: SCREEN_WIDTH + 1)*/
        
        bagButton.transparentButtonWithTitle("Customize my bag")
        bagButton.tag = 0
        bagButton.addTarget(self, action: "buttonPressed:", forControlEvents: UIControlEvents.TouchUpInside)
        bagButton.drawBordersWithColorAndWidth(kGreenColor, width: 1.0)
        self.addSubview(bagButton)
        
        bagButton.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        //bagButton.autoPinEdge(ALEdge.Bottom, toEdge: ALEdge.Top, ofView: bagButton, withOffset: -10.0)
        bagButton.autoPinEdgeToSuperviewEdge(ALEdge.Bottom, withInset: 100)
        bagButton.autoSetDimension(ALDimension.Width, toSize: SCREEN_WIDTH + 1)
        
    }
    
    func buttonPressed(sender: UIButton) {
        self.delegate.continuePressed()
    }
}
