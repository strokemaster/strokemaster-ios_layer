//
//  CategoriesContainerViewController.swift
//  IndyLouisiana
//
//  Created by Marcos Griselli on 3/12/15.
//  Copyright (c) 2015 Marcos Griselli. All rights reserved.
//

import UIKit
import CoreLocation

class CategoriesContainerViewController: UIViewController, UIActionSheetDelegate, UIAlertViewDelegate, CLLocationManagerDelegate{
    
    var customTabBarController: CustomTabBarController!
    
    //Location
    let locationManager = CLLocationManager()
    
    var course: CourseListCourse!
    
    var holesArray = Array<SMHole>()
    let trackingViewController = TrackingViewController()
    let gpsViewController = GpsViewController()
    let scoreCardViewController = ScoreCardViewController()
    
    var holeCount = 0
    var startHole:Int
    var scoresArray = Array<NSInteger>()
    
    //Json value
    var holesJson: JSON!
    var holesJsonCount = 0
    var roundData: InRoundData = InRoundData(userId: "1", name: "Test Course")
    
    //Navigation Button
    var nextHole: UIBarButtonItem!
    
    //Best Location 
    var bestLocation: CLLocation!
    
    //Distances array
    var distancesArray = Array<Array<Int>>()
    
    //Clubs array
    var clubsArray = Array<Array<Int>>()
    
    //round info
    var totalscore = 0
    var totalPar = 0
    
    var userdefaults = NSUserDefaults.standardUserDefaults()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?, json: JSON, course: CourseListCourse, numHoles: Int) {
        
        scoreCardViewController.initTeeDetails(course)
        self.startHole = 0
        super.init(nibName: nil, bundle: nil)
        
        //Location Manager
        locationManager.delegate = self
        //locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.startUpdatingLocation()
        
        customTabBarController = CustomTabBarController(nibName: nil, bundle: nil)
        customTabBarController.view.backgroundColor = .whiteColor()
    
        holesJsonCount = numHoles
        
        //Holes, missing yards!  FIX ME
        if let scoreCard = course.scoreCardInfo {
            for (index, value) in scoreCard.mensHandicap.enumerate() {
                let hole = SMHole(number: index + 1, yards: course.teeDetails!.ydsHole[index], par: scoreCard.mensPar[index], handicap: value)
                holesArray.append(hole)
            }
        }

        //Different ViewControllers our In Round section will have.
        trackingViewController.hole = holesArray[holeCount]
        trackingViewController.title = "TRACKING"
        
        holesJson = json
        let firstHole = json.arrayValue[0]
        gpsViewController.gpsView.courseImage.json = firstHole
        
        
        gpsViewController.hole = holesArray[holeCount]
        gpsViewController.title = "GPS"
        
        scoreCardViewController.title = "SCORECARD"
        scoreCardViewController.headerLabel.text = "\(course.name)"
        scoreCardViewController.scoreCard = course.scoreCardInfo
        
        let menuViewController = UIViewController()
        menuViewController.title = "MENU"
        
        let viewControllers = [trackingViewController, gpsViewController, scoreCardViewController, menuViewController]
        customTabBarController.viewControllers = viewControllers
        customTabBarController.buttonsWidth = UIScreen.mainScreen().bounds.size.width / CGFloat(viewControllers.count)
        
        
        //Set course
        self.course = course
    }
    
    override func loadView() {
        super.loadView()
        self.view = UIView()
        
        self.addChildViewController(customTabBarController)
        self.view.addSubview(customTabBarController.view)
        customTabBarController.view.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsZero, excludingEdge: ALEdge.Top)
        customTabBarController.view.autoPinToTopLayoutGuideOfViewController(self, withInset: 0)
        customTabBarController.didMoveToParentViewController(self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "1st Hole"
        self.navigationItem.hidesBackButton = true
        
        nextHole = UIBarButtonItem(title: "2nd Hole >", style: UIBarButtonItemStyle.Plain, target: self, action: "switchHole")
        self.navigationItem.rightBarButtonItem = nextHole
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.hidden = true
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //change navigation title
    func changeNavigationTitle(string: String) {
        self.title = string
    }
    
    //MARK: - LocationManagerDelegate
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let newLocation = locations.last as CLLocation!
        
        //Check for first location
        if bestLocation == nil {
            bestLocation = newLocation
        }
        
        let locationAge = -(newLocation.timestamp.timeIntervalSinceNow)

        //Avoid those 20 mts jumps
        if locationAge < 3.0 && newLocation.distanceFromLocation(bestLocation) > 15 {
            return
        } else {
            bestLocation = newLocation
        }
        
        gpsViewController.updateLocation(bestLocation)
    }
    
    func getLocation() -> CLLocation {
        return CLLocation(latitude: locationManager.location!.coordinate.latitude, longitude: locationManager.location!.coordinate.longitude)
    }
    
    //Change holes while in round
    func switchHole() {

        if (holeCount + 1) != holesJsonCount {
            
            
            
            let refreshAlert = UIAlertController(title: "Next Hole", message: "Moving on to the next hole will close the current hole score", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) in
                
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                self.addHoleData()
                self.alertAction(1)
                
                
            }))
            
            presentViewController(refreshAlert, animated: true, completion: nil)
            
            /*let alertView = UIAlertView(title: "Next Hole", message: "Moving on to the next hole will close the current hole score", delegate: self, cancelButtonTitle: "Cancel", otherButtonTitles: "Move On!")
            alertView.tag = 1
            alertView.show()
            addHoleData()*/
        } else {
            
            /*let alertView = UIAlertView(title: "Finished", message: "Game is finished", delegate: self, cancelButtonTitle: "Ok")
            alertView.tag = 3
            alertView.show()
            addHoleData()*/
            
            let refreshAlert = UIAlertController(title: "Finished", message: "Game is finished", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) in
                
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Finish", style: .Default, handler: { (action: UIAlertAction!) in
                self.addHoleData()
                self.alertAction(3)
                self.navigationController?.popToRootViewControllerAnimated(true)
                
            }))
            
            presentViewController(refreshAlert, animated: true, completion: nil)
        }
    }
    
    
    func addHoleData(){
        
        
        //populate hole info
        var startPoints = [String]()
        var endPoints = [String]()
        var startDists = [Int]()
        var endDists = [Int]()
        var yardages = [Int]()
        var terrains = [String]()
        var clubs = [String]()
        //fix me account for mens/womens par
        let par: Int = self.course.scoreCardInfo!.mensPar[self.roundData.holes.count]
        
        for var index = trackingViewController.strokeArray.count-1; index >= 0 ; --index {
            let stroke:SMStroke = trackingViewController.strokeArray[index]
            startPoints.append("\"" + stroke.startPoint! + "\"")
            endPoints.append("\"" + stroke.endPoint! + "\"")
            startDists.append(stroke.startDist!)
            endDists.append(stroke.endDist!)
            yardages.append(stroke.yards!)
            terrains.append("\"" + "Grass" + "\"")
            clubs.append("\"" + stroke.club!.name! + "\"")
        }
        
        let thisHole : HoleData = HoleData(startPoint: startPoints, endPoint: endPoints, startDist: startDists, endDist: endDists, yardage: yardages, terrain: terrains, clubs: clubs, par:par)
        
        roundData.holes.append(thisHole)
        self.totalscore = self.totalscore + yardages.count
        roundData.totalScore = self.totalscore
        roundData.score = roundData.score + String(yardages.count) + ","
        self.totalPar = self.totalPar + par
        roundData.totalPar = self.totalPar
    }
    
    //MARK: ActionSheetDelegate
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        
        //buttonIndex 1 is cancel
        if buttonIndex != 1 {
            /*let alertView = UIAlertView(title: "Delete Round", message: "Are you sure you want to delete this round?", delegate: self, cancelButtonTitle: "Cancel", otherButtonTitles: "Yes")
            alertView.tag = 0
            alertView.show()*/
            let refreshAlert = UIAlertController(title: "End Round", message: "Are you sure you want to end this round?", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) in
                
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "End Round", style: .Default, handler: { (action: UIAlertAction!) in
                self.alertAction(0)
                
                
            }))
            
            presentViewController(refreshAlert, animated: true, completion: nil)
        }
    }
    

    func alertAction(type:Int) {

        if type == 0 {
            self.navigationController?.popToRootViewControllerAnimated(true)
        }
        if type == 1 {
            
            //Fill up Scorecard
            let score = trackingViewController.strokeArray.count
            scoresArray.append(score)
            scoreCardViewController.user.append(score)
            
            strokeTypeScore(trackingViewController.strokeArray)
            
            var strokesDistanceForRound = [Int]()
            var clubsForRound = [Int]()
            
            for stroke in trackingViewController.strokeArray {
                if let yards = stroke.yards {
                    strokesDistanceForRound.append(yards)
                } else {
                    strokesDistanceForRound.append(0)
                }
                
                if let number = stroke.club?.clubNumber {
                    clubsForRound.append(number)
                } else {
                    clubsForRound.append(0)
                }
            }
            
            distancesArray.append(strokesDistanceForRound)
            clubsArray.append(clubsForRound)
            
            holeCount++
            
            
            
            //Fix Code
            let string = ""
            let newString = string.suffixForNumber(holeCount + 1)
            self.title = "\(newString) Hole"
            nextHole.title = "\(newString.suffixForNumber(holeCount + 2)) Hole >"
            
            if (holeCount + 1) == holesJsonCount {
                nextHole.title = "Finish!"
            }
            
            trackingViewController.updateHole(holesArray[holeCount])
            gpsViewController.updateHole(holesArray[holeCount], json: holesJson[holeCount])
        }
        
        if type == 3 {
            
            //Fill up Scorecard
            let score = trackingViewController.strokeArray.count
            scoresArray.append(score)
            scoreCardViewController.user.append(score)
            
            strokeTypeScore(trackingViewController.strokeArray)
            
            var strokesDistanceForRound = [Int]()
            var clubsForRound = [Int]()
            
            for stroke in trackingViewController.strokeArray {
                if let yards = stroke.yards {
                    strokesDistanceForRound.append(yards)
                } else {
                    strokesDistanceForRound.append(0)
                }
                
                if let number = stroke.club?.clubNumber {
                    clubsForRound.append(number)
                } else {
                    clubsForRound.append(99) //Putter
                }
            }
            
            distancesArray.append(strokesDistanceForRound)
            clubsArray.append(clubsForRound)
            
            holeCount++
            
            var scoreCount = 0
            
            for value in scoresArray {
                scoreCount = scoreCount + value
            }
            
            //update round info
            roundData.userId = userdefaults.objectForKey("Email") as! String
            roundData.courseName = course.name
            //fix me account for mens/womens par
            roundData.par = arrayToString(self.course.scoreCardInfo!.mensPar)
            roundData.startHole = self.startHole
            //remove the trailing comma
            roundData.score = String(roundData.score.substringToIndex(roundData.score.endIndex.predecessor()))

            DataManager.sendEndRound(roundData.toJSONString(), success:{(res) -> Void in

            })

        }
    }
    
    //Fill in scorecard (Approach, short and putts) 
    func strokeTypeScore(strokeArray: Array<SMStroke>) {
        
        var approach = 0
        var short = 0
        var putts = 0
        
        for stroke in strokeArray {
            
            if stroke.club?.type.rawValue == 4 {
                putts++
            } else if stroke.club?.type.rawValue != 0 {
                stroke.yards <= 40 ? short++ : approach++
            }
        }
        
        scoreCardViewController.arrays[5].append(approach)
        scoreCardViewController.arrays[6].append(short)
        scoreCardViewController.arrays[7].append(putts)
        
        if scoreCardViewController.tableView != nil {
            self.scoreCardViewController.tableView.reloadData()
        }
    }
    
    func arrayToString(arr: [Int]) -> String {
        var res : String = ""
        for var i = 0; i < arr.count; i++ {
            res = res + String(arr[i]) + ","
        }
        if(res.characters.count > 0 ){
            res = String(res.substringToIndex(res.endIndex.predecessor()))
        }
        return res
    }
}
    