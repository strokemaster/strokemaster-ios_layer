//
//  ClubStatsTableViewCell.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 6/26/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class ClubStatsTableViewCell: UITableViewCell {
    
    let clubLabel = UILabel()
    var rangeView = StatsRangesView()
    let percentajeLabel = UILabel()

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.clearColor()
    
        clubLabel.textColor = UIColor.whiteColor()
        self.addSubview(clubLabel)
        clubLabel.autoPinEdgeToSuperviewEdge(ALEdge.Left, withInset: 15.0)
        clubLabel.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        
        self.addSubview(rangeView)
        rangeView.autoSetDimensionsToSize(CGSizeMake(SCREEN_WIDTH * 0.6, 44.0))
//        rangeView.autoAlignAxisToSuperviewAxis(ALAxis.Vertical)
        rangeView.autoPinEdgeToSuperviewEdge(ALEdge.Right, withInset: 15.0)
        
//        percentajeLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 20.0)
//        percentajeLabel.textColor = UIColor.whiteColor()
//        self.addSubview(percentajeLabel)
//        percentajeLabel.autoPinEdgeToSuperviewEdge(ALEdge.Right, withInset: 25.0)
//        percentajeLabel.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
    }
}
