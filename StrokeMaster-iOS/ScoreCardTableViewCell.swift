//
//  ScoreCardTableViewCell.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 6/1/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

class ScoreCardTableViewCell: UITableViewCell {
    
    var cellLabel: UILabel?
    var firstLabel = UILabel()
    let blackView = UIView()

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .None
        self.backgroundColor = UIColor.clearColor()
        
        blackView.backgroundColor = kBlackTransparentColor
        self.contentView.addSubview(blackView)
    
        blackView.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsMake(1, 0, 0, 0), excludingEdge: ALEdge.Right)
        blackView.autoSetDimension(ALDimension.Width, toSize: SCREEN_WIDTH/2 + 25)
        
        cellLabel = UILabel()
        cellLabel?.textColor = UIColor.whiteColor()
        cellLabel?.text = "Hole"
        cellLabel?.font = UIFont(name: "\(kDefaultFont)-Medium", size: 16.0)
        self.contentView.addSubview(cellLabel!)
        
        cellLabel?.autoPinEdgeToSuperviewEdge(ALEdge.Left, withInset: 15.0)
        cellLabel?.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        
        firstLabel.textColor = UIColor.whiteColor()
        firstLabel.text = ""
        firstLabel.font = UIFont(name: "\(kDefaultFont)-Medium", size: 15.0)
        self.contentView.addSubview(firstLabel)
        
        firstLabel.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        firstLabel.autoPinEdgeToSuperviewEdge(ALEdge.Left, withInset: SCREEN_WIDTH/2)
    }
    
    //Fill the cell sideways
    func fillCellWithValuesAndBackgroundColor(array: Array<NSInteger>, color: UIColor?) {
       
        var previousLabel = firstLabel
        
        for value in array {
            let label = UILabel()
            label.textColor = UIColor.whiteColor()
            label.text = "\(value)"
            label.font = UIFont(name: "\(kDefaultFont)-Medium", size: 16.0)
            label.textAlignment = NSTextAlignment.Center
            if let _ = color {
                label.backgroundColor = color
            } else {
                label.transparentViewWithAlpha(0.5)
            }
            self.contentView.addSubview(label)
            
            label.autoAlignAxis(ALAxis.Vertical, toSameAxisOfView: previousLabel, withOffset: 50.0)
            label.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
            //TO-DO Fix sizes
            label.autoSetDimensionsToSize(CGSizeMake(48, 48))
            
            previousLabel = label
        }
    }

    
    //User Score for diferent colors
    func fillUserScoreValues(scoreArray: Array<NSInteger>, parArray: Array<NSInteger>) {
        var previousLabel = firstLabel
            
        for (index, value) in scoreArray.enumerate() {
            let label = UILabel()
            label.textColor = colorForScore(value, par: parArray[index])
            label.text = "\(value)"
            label.font = UIFont(name: "\(kDefaultFont)-Bold", size: 17.0)
            label.textAlignment = NSTextAlignment.Center
            label.transparentViewWithAlpha(0.5)
            self.contentView.addSubview(label)
                
            label.autoAlignAxis(ALAxis.Vertical, toSameAxisOfView: previousLabel, withOffset: 50.0)
            label.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
            label.autoSetDimensionsToSize(CGSizeMake(48, 48))
                
            previousLabel = label
        }
    }
    
    //Check for corresponding color
    func colorForScore(score: NSInteger, par: NSInteger) -> UIColor {
        
        if score == par - 2 {
            return .blueColor()
        } else if score == par - 1 {
            return .greenColor()
        } else if score == par {
            return .yellowColor()
        } else {
            return score == par + 1 ? .orangeColor() : .redColor()
        }
    }
    
    //Add player
    func addImage() {
        let profileImageView = UIImageView()
        profileImageView.circularImageViewWithWidth(30.0)
        profileImageView.image = UIImage(named: "golfer")
        self.addSubview(profileImageView)
        
        profileImageView.autoPinEdgeToSuperviewEdge(ALEdge.Left, withInset: 15.0)
        profileImageView.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        
        //TO-DO Fix
        cellLabel?.removeFromSuperview()
        cellLabel = nil
        
        cellLabel = UILabel()
        cellLabel?.textColor = UIColor.whiteColor()
        let userdefaults = NSUserDefaults.standardUserDefaults()
        cellLabel?.text = userdefaults.objectForKey("Email") as? String
        //cellLabel?.text = "Robert D."
        cellLabel?.font = UIFont(name: "\(kDefaultFont)-Light", size: 16.0)
        self.contentView.addSubview(cellLabel!)
        
        cellLabel?.autoPinEdge(ALEdge.Left, toEdge: ALEdge.Right, ofView: profileImageView, withOffset: 10.0)
        cellLabel?.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        
        
    }
}
