//
//  SideMenuTableFooterView.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 5/6/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

protocol LogoutViewDelegate {
    func logoutPresed()
}

class SideMenuTableFooterView: UIView {
    
    let logoutImageView = UIImageView(image: UIImage(named: "logout")!)
    let logoutLabel = UILabel()
    var delegate: LogoutViewDelegate!

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.blackColor()
        
        self.addSubview(logoutImageView)
        
        logoutImageView.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        logoutImageView.autoPinEdgeToSuperviewEdge(ALEdge.Left, withInset: 15.0)
        
        logoutLabel.textColor = UIColor.whiteColor()
        logoutLabel.font = UIFont(name: "\(kDefaultFont)-Light", size: 17.0)
        logoutLabel.text = "LOG OUT"
        self.addSubview(logoutLabel)
        
        logoutLabel.autoAlignAxisToSuperviewAxis(ALAxis.Horizontal)
        logoutLabel.autoPinEdge(ALEdge.Left, toEdge: ALEdge.Right, ofView: logoutImageView, withOffset: 10.0)
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: "tap:")
        self.addGestureRecognizer(gestureRecognizer)

    }
    
    func tap(sender: UITapGestureRecognizer) {
        self.delegate.logoutPresed()
    }
}
