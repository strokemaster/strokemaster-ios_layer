//
//  PuttsTableViewController.swift
//  StrokeMaster-iOS
//
//  Created by Marcos Griselli on 6/1/15.
//  Copyright (c) 2015 StrokeMaster. All rights reserved.
//

import UIKit

protocol PuttsTableViewControllerDelegate {
    func puttSelected(distance: NSInteger)
}

class PuttsTableViewController: ClubsTableViewController {
    
    var puttsArray = Array<String>()
    let feetArray = [1, 5, 10, 20, 40, 50]
    var puttDelegate: PuttsTableViewControllerDelegate!
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nil, bundle: nil)
        headerText = "SELECT PUTT"
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        puttsArray = ["Tap-In", "5 Feet to Hole", "10 Feet to Hole", "20 Feet to Hole", "40 Feet to Hole", "40+ Feet to Hole"]
    }
    
    //Override ClubsTableView methods
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return puttsArray.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("puttCell") as UITableViewCell!
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "puttCell")
        }
        
        let string = puttsArray[indexPath.row]
        cell?.textLabel?.text = string.uppercaseString
        customizeCell(cell!)
        
        return cell!
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.puttDelegate.puttSelected(feetArray[indexPath.row])
    }
}
